# FreeNAS Media Server Installer

## Introduction
I feel that there are so many "how-tos" out there, with very few that "just work", that we would all benefit from this being "open source" so others can add to it, improve on it, or just read the script itself as a way to see how to install a "fully" working system. 

Over the years, I've experimented with various types of home media servers and this is my favorite build and build process, as of today.   

One of the great things about this particular process is that you are left with each of the application's configuration being completely isolated from its installation jail. The configuration/cache/database/logs/etc are stored on the ZPool Volume, so therefore, once you've configured an application, or run it for a while, you can still blow away the jail anytime, and every time, you want to re-install an application (or even FreeNAS itself) and it'll pick up where you left it.

I've used packages where decent ones existed, but a few of the applications require installation from a GIT clone, or even the download of a specific binary.

Note that, being a person who is addicted to the "Update Now" button, I've made it so the creation scripts add "auto-update" cron jobs, which are set to run every 24 hours; Welcome to the "leading edge".

Lastly, one thing I've often noticed is the seeming "omnipresence" of Joshua Ruehlig and Clinton Hall in this arena.  Big shout out to them and all the work they've done.

## Recommended approach
* Enable all the apps (except radarr)
* Enable installation process
* Run installer
    * Successful application installation windows will disappear as they are completed, installations with critical errors will remain on display
    * If any application windows remain, disable each of the successful applications and rerun the installer to re-install failed applications
* Enable all the apps (except radarr)
* Disable installation process
* Enable setup process
* Run installer
    * Successful application setup windows will disappear as they are completed, setups with critical errors will remain on display
    * If any application windows remain, disable each of the successful applications and rerun the installer to re-setup failed applications

## Setup
1. SSH to the FreeNAS host using the `root` user:
   ```
   # ssh root@{$freenas_IP}
   ```
2. Download and extract the media application/jail creation scripts to root's `$HOME` folder on the FreeNAS host:
   ```
   # wget -O freenas-media-server-installer.zip -P $HOME https://gitlab.com/TNosredna/freenas-media-server-installer/-/archive/master/freenas-media-server-installer-master.zip
   # unzip -d $HOME $HOME/freenas-media-server-installer.zip
   ```
3. Copy the default configuration template file to `$HOME/`:   
   ```
   # cp $HOME/freenas-media-server-installer/common-config.sh $HOME/common-config.sh
   ```
    
## Configuration
    
##### Update the `$HOME/common-config.sh` configuration template file
1. Edit the configuration
    ```
    # nano $HOME/common-config.sh
    ```
    * Enable each application you wish installed and/or setup by setting it's value to "YES" (eg `deluge_enabled="YES"`)
    * Enable the installation of each enabled application by setting `install_jails_enabled="YES"`
    * Enable the setup of each enabled applications by setting `setup_apps_enabled="YES"`
    * Update the "Jail Installation Configuration (install_jails_enabled)" configuration section if you've enabled `install_jails_enabled` for each of the enabled applications
    * Update the "Jail Setup Configuration (setup_apps_enabled)" configuration section if you've enabled `setup_apps_enabled` for each of the enabled applications
* Note: if `setup_apps_enabled="YES"`, prior to setting up the applications, the installer will create a backup of the current `/mnt/{zpool_name}/apps` folder (including children) to `$HOME/app_configs_bak.zip` containing all current application configurations
* **Note: each iocage needs to have DHCP enabled, so you MUST determine ahead of time what MAC and IP addresses you are going to assign to each, and setup your local router so it will assign the expected IP to each iocage based on MAC hardware address.**

###### VPN Configuration (Optional)
1. If the default configuration doesn't work, you can download/create your own OpenVPN `openvpn.opvn` config file and save it to `$HOME/openvpn.ovpn`, where the installer will look for it during installation

###### Firewall Configuration (Optional)
1. Each application's firewall rules will be found at `/mnt/{zpool_name}/apps/{app_name}/ipfw.rules`.  Note that the default rules for Deluge and SabNZBD should prevent communication by the 'media' user (eg Deluge) when OpenVPN isn't running. 

## Installation
1. Run the installer script    
    ```
    # $HOME/freenas-media-server-installer/install.sh
    ```
* If you want to merely retrieve the latest version of this project, without running the scripts, run: 
    ```
    # $HOME/freenas-media-server-installer/install.sh updateonly
    ```
    Note: You can change the branch that you're downloading via the `checkoutURL` configuration option
    
### Post-Installation
###### Fine-tune the configuration of each installed application via their respective GUIs
 
 Application | URL (Default)| Special Note
 ---|---|---
 Couchpotato | <http://{jail_ip}:5050/> |
 Deluge | <http://{jail_ip}:8112/> | Default password is "deluge"
 Headphones | <http://{jail_ip}:8181/> |
 Lazy Librarian | <http://{jail_ip}:5299/> |
 Madsonic | <http://{jail_ip}:8080/> |
 Medusa | <http://{jail_ip}:4040/index.view> |
 Plex Media Server | <http://{jail_ip}:32400/web/index.html> |
 SabNZBD | <http://{jail_ip}:8080/sabnzbd/> |

* ##### Note that all the application configurations are located at:
    * SSH'd to FreeeNAS host: `/mnt/{zpool_name}/apps/{app_name}` 
    * SSH'd to a specific iocage jail: `/mnt/config`
* Ensure that all modifications are done as the "media" user
    * Note that the each application's NFS share will automatically "proxy" you as the 'media' user

## Configuration Details
#### Applications
###### Enable
Setting|Default Value|Description
:---|:---:|:---
deluge_enabled|"NO|"Deluge is a torrent downloader       
sabnzbd_enabled|"NO"|SabNZBD is a Usenet / NZB downloader
plex_enabled|"NO"|Plex Media Server manages and serves the media library         
couchpotato_enabled|"NO"|Couchpotato finds movies [Recommended because it supports public torrent sites]
radarr_enabled|"NO"|Radarr finds movies, like Couchpotato, but doesn't support public Torrent sites
medusa_enabled|"NO"|Medusa finds TV shows
calibre_enabled|"NO"|Calibre manages and serves the eBook library
lazylibrarian_enabled|"NO"|Lazy Librarian finds eBooks
madsonic_enabled|"NO"|Madsonic hosts and serves the music library
headphones_enabled|"NO"|Headphones finds music
#### IOCage and Application Installation
###### Enable 
Setting|Default Value|Description
:---|:---:|:---
install_jails_enabled|"NO"|Enable installation of enabled applications
###### General Settings (IOCage and Application Installation)
Setting|Default Value|Description
:---|:---:|:---
subnet|""|Network subnet IP address (###.###.###.0)
router_ip|""|Router IP address (###.###.###.1)
freenas_ip|""|FreeNAS host IP address (###.###.###.###)
zpool_name|"Barn"|FreeNAS zPool name
root_user_name|"root"|Superuser username
root_user_password|""|Superuser password
media_user_name|"media"|Username used for all media operations
media_user_password|""|Media user's password
media_group_name|"media"|Media group for all media operations
notify_gmail_address|""|Recipient address for jail notifications (jsmith@gmail.com)
openvpn_user_name|""|Username used with OpenVPN's auth-user-pass credentials (jsmith@gmail.com@newshosting)
openvpn_user_password|""|Password used with OpenVPN's auth-user-pass credentials
###### Deluge Settings (IOCage and Application Installation) - https://dev.deluge-torrent.org
Setting|Default Value|Description
:---|:---:|:---
deluge_jail_ip|""|IP address assigned to this jail (###.###.###.###)
deluge_jail_mac|""|MAC Addresses assigned to this jail (##:##:##:##:##:## ##:##:##:##:##:##)
###### SabNZBD Settings (IOCage and Application Installation) - https://sabnzbd.org
Setting|Default Value|Description
:---|:---:|:---
sabnzbd_jail_ip|""|IP address assigned to this jail (###.###.###.###)
sabnzbd_jail_mac|""|MAC Addresses assigned to this jail (##:##:##:##:##:## ##:##:##:##:##:##)
###### Plex Media Server Settings (IOCage and Application Installation) - https://www.plex.tv
Setting|Default Value|Description
:---|:---:|:---
plex_jail_ip|""|IP address assigned to this jail (###.###.###.###)
plex_jail_mac|""|MAC Addresses assigned to this jail (##:##:##:##:##:## ##:##:##:##:##:##)
###### Couchpotato Settings (IOCage and Application Installation) - https://couchpota.to
Setting|Default Value|Description
:---|:---:|:---
couchpotato_jail_ip|""|IP address assigned to this jail (###.###.###.###)
couchpotato_jail_mac|""|MAC Addresses assigned to this jail (##:##:##:##:##:## ##:##:##:##:##:##)
###### Radarr Settings (IOCage and Application Installation) - https://radarr.video
Setting|Default Value|Description
:---|:---:|:---
radarr_jail_ip|""|IP address assigned to this jail (###.###.###.###)
radarr_jail_mac|""|MAC Addresses assigned to this jail (##:##:##:##:##:## ##:##:##:##:##:##)
###### Medusa Settings (IOCage and Application Installation) - https://pymedusa.com
Setting|Default Value|Description
:---|:---:|:---
medusa_jail_ip|""|IP address assigned to this jail (###.###.###.###)
medusa_jail_mac|""|MAC Addresses assigned to this jail (##:##:##:##:##:## ##:##:##:##:##:##)
###### Calibre Settings (IOCage and Application Installation) - https://calibre-ebook.com
Setting|Default Value|Description
:---|:---:|:---
calibre_jail_ip|""|IP address assigned to this jail (###.###.###.###)
calibre_jail_mac|""|MAC Addresses assigned to this jail (##:##:##:##:##:## ##:##:##:##:##:##)
###### Lazy Librarian Settings (IOCage and Application Installation) - https://lazylibrarian.gitlab.io
Setting|Default Value|Description
:---|:---:|:---
lazylibrarian_jail_ip|""|IP address assigned to this jail (###.###.###.###)
lazylibrarian_jail_mac|""|MAC Addresses assigned to this jail (##:##:##:##:##:## ##:##:##:##:##:##)
###### Madsonic Settings (IOCage and Application Installation) - https://www.madsonic.org/pages/index.jsp
Setting|Default Value|Description
:---|:---:|:---
madsonic_jail_ip|""|IP address assigned to this jail (###.###.###.###)
madsonic_jail_mac|""|MAC Addresses assigned to this jail (##:##:##:##:##:## ##:##:##:##:##:##)
###### Headphones Settings (IOCage and Application Installation) - https://github.com/rembo10/headphones/wiki
Setting|Default Value|Description
:---|:---:|:---
headphones_jail_ip|""|IP address assigned to this jail (###.###.###.###)
headphones_jail_mac||MAC Addresses assigned to this jail (##:##:##:##:##:## ##:##:##:##:##:##)

#### Application Sample Setup
###### Enable
Setting|Default Value|Description
:---|:---:|:---
setup_apps_enabled|"NO"|Enable setup of enabled applications
backup_existing_setups|"YES"|Perform backup of entire Apps folder
###### General Settings (Application Sample Setup)
Setting|Default Value|Description
:---|:---:|:---
notify_gmail_password|""|Jail notification recipient's password
usenet_provider_user_name|""|Username used to connect to remote service
usenet_provider_user_password|""|Password used to connect to remote service
usenet_provider_retention|""|Number of days your usenet downloader retains NZBs
git_user_name|""|Git username for updating git based applications
git_user_password|""|Git password for updating git based applications
omdb_api_key|""|API key used to make calls to this remote service
tmdb_api_key|""|API key used to make calls to this remote service
newznab_api_key|""|API key used to make calls to this remote service
nzbgeek_api_key|""|API key used to make calls to this remote service
nzbsu_api_key|""|API key used to make calls to this remote service
rating_host=|""|Host/server to connect to for alternate rating info
rating_api_key|""|API key used to make calls to this remote service
goodreads_api_key|""|API key used to make calls to this remote service
googlebooks_api_key|""|API key used to make calls to this remote service
###### Plex Media Server Settings (Application Sample Setup)
Setting|Default Value|Description
:---|:---:|:---
plex_online_user_name|""|Username used to connect to remote service
plex_online_user_password|""|
plex_online_email|""|Password used to connect to remote service (jsmith@gmail.com)
plex_jail_name|"plex"|Name assigned to this iocage jail
plex_jail_port|"32400"|
plex_web_root|"web/index.html"|
plex_sections="movie,3&#124;tv,4"|
plex_server_friendly_name|"Media Appliance Plex Server"|
plex_openvpn_enabled|"NO"|Enable OpenVPN for this jail
plex_ipfw_enabled|"NO"|Enable IPFW (Firewall) for this jail
###### SabNZBD Settings (Application Sample Setup)
Setting|Default Value|Description
:---|:---:|:---
sabnzbd_jail_name|"sabnzbd"|Name assigned to this iocage jail
sabnzbd_jail_port|8080|
sabnzbd_web_root|"/sabnzbd"|
sabnzbd_openvpn_enabled|"NO"|Enable OpenVPN for this jail
sabnzbd_ipfw_enabled|"NO"|Enable IPFW (Firewall) for this jail
###### Deluge Settings (Application Sample Setup)
Setting|Default Value|Description
:---|:---:|:---
deluge_jail_name|"deluge"|Name assigned to this iocage jail
deluge_jail_port|"58846"|
deluge_jail_http_port|"8112"|
deluge_web_root|""|
deluge_openvpn_enabled|"NO"|Enable OpenVPN for this jail
deluge_ipfw_enabled|"NO"|Enable IPFW (Firewall) for this jail
###### Couchpotato Settings (Application Sample Setup)
Setting|Default Value|Description
:---|:---:|:---
couchpotato_jail_name|"couchpotato"|Name assigned to this iocage jail
couchpotato_jail_port|"5050"|
couchpotato_web_root|""|
couchpotato_openvpn_enabled|"NO"|Enable OpenVPN for this jail
couchpotato_ipfw_enabled|"NO"|Enable IPFW (Firewall) for this jail
###### Radarr Settings (Application Sample Setup)
Setting|Default Value|Description
:---|:---:|:---
radarr_jail_name|"radarr"|Name assigned to this iocage jail
radarr_jail_port|"7878"|
radarr_web_root|""|
radarr_openvpn_enabled|"NO"|Enable OpenVPN for this jail
radarr_ipfw_enabled|"NO"|Enable IPFW (Firewall) for this jail
###### Medusa Settings (Application Sample Setup)
Setting|Default Value|Description
:---|:---:|:---
medusa_jail_name|"medusa"|Name assigned to this iocage jail
medusa_jail_port|"8081"|
medusa_web_root|"/home"|
medusa_openvpn_enabled|"NO"|Enable OpenVPN for this jail
medusa_ipfw_enabled|"NO"|Enable IPFW (Firewall) for this jail
###### Calibre Settings (Application Sample Setup) 
Setting|Default Value|Description
:---|:---:|:---
calibre_jail_name|"calibre"|Name assigned to this iocage jail
calibre_jail_port|"8082"|
calibre_web_root|"/"|
calibre_openvpn_enabled|"NO"|Enable OpenVPN for this jail
calibre_ipfw_enabled|"NO"|Enable IPFW (Firewall) for this jail
###### Lazy Librarian Settings (Application Sample Setup)
Setting|Default Value|Description
:---|:---:|:---
lazylibrarian_jail_name|"lazylibrarian"|Name assigned to this iocage jail
lazylibrarian_jail_port|"5299"|
lazylibrarian_web_root|""|
lazylibrarian_openvpn_enabled|"NO"|Enable OpenVPN for this jail
lazylibrarian_ipfw_enabled|"NO"|Enable IPFW (Firewall) for this jail
###### Madsonic Settings (Application Sample Setup)
Setting|Default Value|Description
:---|:---:|:---
madsonic_jail_name|"madsonic"|Name assigned to this iocage jail
madsonic_jail_port|"4040"|
madsonic_web_root|"/index.view"|
madsonic_openvpn_enabled|"NO"|Enable OpenVPN for this jail
madsonic_ipfw_enabled|"NO"|Enable IPFW (Firewall) for this jail
###### Headphones Settings (Application Sample Setup)
Setting|Default Value|Description
:---|:---:|:---
headphones_jail_name|"headphones"|Name assigned to this iocage jail
headphones_jail_port|"8181"|
headphones_web_root|"/"|
headphones_openvpn_enabled|"NO"|Enable OpenVPN for this jail
headphones_ipfw_enabled|"NO"|Enable IPFW (Firewall) for this jail

### System Settings
    Don't change the following unless you know what you're doing and have a very good reason
Setting|Default Value|Description
:---|:---:|:---
nzbtomedia_enabled|"YES"|
log_enabled|"TRUE"|

#### Media Processing Folder Structure
Setting|Current Value|Description
:---|:---|:---
download|/mnt/download|All content is being downloaded underneath here (categorised and uncategorised )
download_incomplete_torrent|"/mnt/download/incomplete/torrent"|(deluge -> Download -> Download to)
download_incomplete_nzb|"/mnt/download/incomplete/nzb"|(sabnzbd -> incomplete)
download_incomplete_process|"/mnt/download/incomplete/process"|(autoProcessMedia -> [Torrent] -> outputDirectory)
download_incomplete_process_tv|"/mnt/download/incomplete/process/tv"|(medusa -> config -> postProcessing -> config/postProcessing)
download_incomplete_process_movies|"/mnt/download/incomplete/process/movies"|(couchpotato -> settings -> renamer -> from)
download_incomplete_process_ebooks|"/mnt/download/incomplete/process/ebooks"|(lazylibrarian -> settings -> renamer -> from)
download_incomplete_process_audiobooks|"/mnt/download/incomplete/process/audiobooks"|
download_incomplete_process_music|"/mnt/download/incomplete/process/music"|(headphones -> settings -> renamer -> from)
download_incomplete_manual|"/mnt/download/manualImport"|
jail_media_folder_tv|"/mnt/media/tv"|(medusa -> tv -> series -> destination)
jail_media_folder_movies|"/mnt/media/movies"|(couchpotato -> settings -> renamer -> To)
jail_media_folder_ebooks|"/mnt/media/ebooks"|(lazylibrarian -> settings -> renamer -> To)
jail_media_folder_music|"/mnt/media/music"|(headphones -> settings -> renamer -> To)
jail_media_folder_audiobooks|"/mnt/media/audiobooks"|
jail_media_folder_pictures|"/mnt/media/pictures"|
jail_media_folder_home_videos|"/mnt/media/homeVideos"|
jail_media_folder|"/mnt/media"|
jail_config|"/mnt/config"|Where application's configurations and logs are available from within jail
jail_nzbtomedia|"/mnt/nzbtomedia"|Where nzbToMedia is available from within jail

#### Local Application API Keys
Setting|Current Value|Description
:---|:---|:---
couchpotato_api_key|"944cb8ca9dc1f5ca4db9067e31881494"|a default API key used to make calls to this local application
radarr_api_key|"944cb8ca9dc1f5ca4db9067e31881494"|a default API key used to make calls to this local application
headphones_api_key|"944cb8ca9dc1f5ca4db9067e31881494"|a default API key used to make calls to this local application
lazylibrarian_api_key|"944cb8ca9dc1f5ca4db9067e31881494"|a default API key used to make calls to this local application
medusa_api_key|"944cb8ca9dc1f5ca4db9067e31881494"|a default API key used to make calls to this local application
plex_online_auth_token|""|Token used to make calls to this remote service
sabnzbd_api_key|"944cb8ca9dc1f5ca4db9067e31881494"|a default API key used to make calls to this local application

##### Freebsd Version Setting
Setting|Default Value|Description
:---|:---:|:---
jail_freebsd_version|""|used by installer to track freebsd version downloading

##### Checkout Setting
Setting|Default Value|Description
:---|:---:|:---
checkoutURL|"https://gitlab.com/TNosredna/freenas-media-server-installer/-/archive/master/freenas-media-server-installer-master.zip"|default download url for installer
#checkoutURL|"https://gitlab.com/TNosredna/freenas-media-server-installer/-/archive/develop/freenas-media-server-installer-develop.zip"

## Versions Tested On So Far

 Application       | Version        
 ----------------- | -------------- 
 FreeNAS, iocage jails | 11.2
 Calibre | 4.2.0 [514762]
 Couchpotato | 3.01 [75e576e]
 Deluge | 2.03
 Headphones | 5283b48736cda701416aad11467649224c6e7080
 Lazy Librarian | 3c367cd4b41443982256bd3f5a9cea23bcd9fc28
 Madsonic | 6.2.9084
 Medusa | 6.2.9084
 Plex Media Server | 4.8.3
 SabNZB | 2.3.9 [03c10dc]

## Troubleshooting
### $jail_ip or $jail_mac showing up blank/empty
1. Check the common-config.sh file and make sure that you have a variable configured that matches your current jail_name as configured in each jail's create file. 
    * eg "jail_name=couchpotato", therefore common-config.sh must have matching "couchpotato_jail_ip" and a "couchpotato_jail_mac" variables configured.

### Deluge downloads being denied
1. "Tracker Status: trackerfix.com: Error: Permission denied"  
2. Can't add torrent files

    * I changed out the vpn server I was hitting --> no change.
    * I disabled the firewall and openvpn --> it worked, but not a viable solution.

### Testing OpenVPN
1. Ensure a new 'tun' interface is present
    ```
    # ifconfig
    ```
2. Check IP sent to site:
    ```
    # iocage exec deluge service openvpn stop
    # iocage exec deluge "wget http://ipinfo.io/ip -qO -"
    ```
3. Note the IP address
    ```
    # iocage exec deluge service openvpn start
    # iocage exec deluge "wget http://ipinfo.io/ip -qO -"
    ```
4. Note the IP address

### Testing Firewall
1. Add the following torrent to Deluge: `http://torrent.unix-ag.uni-kl.de/torrents/KNOPPIX_V8.6-2019-08-08-EN.torrent`
2. While it is downloading, SSH into the FreeNAS host server and stop the Deluge jail's OpenVPN service:
    ```
    # iocage exec deluge service openvpn stop
    ```
    You should see the download rate of the torrent drop to zero, because the IPFW rules won't allow it to connect without the VPN service running.
2. While it is downloading, SSH into the FreeNAS host server and reestart the Deluge jail's OpenVPN service:
    ```
    # iocage exec deluge service openvpn start
    ```
    You should see the download rate of the torrent increase back to it's original speed.

### INTEL GPU OFFLOADING for Plex Media Server
If you have a supported Intel GPU, you can leverage hardware
accelerated encoding/decoding in Plex Media Server on FreeBSD 12.0+.
* Install multimedia/drm-kmod: 
    * e.g. pkg install drm-fbsd12.0-kmod
* Enable loading of kernel module on boot of FreeNAS host: 
    * sysrc kld_list+="i915kms"
        * You must load the module from within the FreeNAS host, not the jail!
* Load the kernel module now (although reboot is advised): 
    * kldload i915kms
* In jails, make a devfs ruleset to expose /dev/dri/* devices.
    * e.g., /dev/devfs.rules on the host:
        * [plex_drm=10]
            * add include $devfsrules_hide_all
            * add include $devfsrules_unhide_basic
            * add include $devfsrules_unhide_login
            * add include $devfsrules_jail
            * add path 'dri*' unhide
            * add path 'dri/*' unhide
            * add path 'drm*' unhide
            * add path 'drm/*' unhide
* Enable the devfs ruleset for your jail. 
    * e.g. iocage set devfs_ruleset="10"
* Make sure hardware transcoding is enabled in the server settings

### Process Flows (INCOMPLETE: Still fleshing out this section)
#### TV FLOW
    [Medusa]
        User requests a show
        searches "Torrent Providers" for torrents
	        Success: sends .torrent url to "Deluge"
        searches "Usenet Providers" for nzbs
	        Success: sends .nzb url to "SabNZBD"
    [Deluge]
        downloads from "Torrent Provider" to "/mnt/download/incomplete/torrent"
	    once seeding is complete, moves from "/mnt/download/incomplete/torrent" to "/mnt/download/incomplete/process/tv"
	    sends notification to "TorrentToMedia.py" with "/mnt/download/incomplete/process/tv" parameter
    [SabNZBD] 
        downloads from "Usenet Provider" to "/mnt/download/incomplete/nzb"
        sends notification to "nzbToSabnzbd.py" with "tv" category
    [nzbToSabnzbd.py] 
        moves download from "/mnt/download/incomplete/nzb" to "/media/download/incomplete/process/tv"
        sends notification to "Medusa"
    [TorrentToMedia.py] 
        ?moves download to "/media/download/incomplete/process/tv"
        sends notification to "Medusa"
    [Medusa] 
        renames from "/media/download/incomplete/process/tv/{old_showname}" to "/media/tv/{new_showname}"
        sends notification to "Plex"
    [Plex] 
        refreshes media library
	
#### Movie FLOW
    [Couchpotato]
        User requests a show
        searches "Torrent Providers" for torrents
	        Success: sends .torrent url to "Deluge"
        searches "Usenet Providers" for nzbs
	        Success: sends .nzb url to "SabNZBD"
    [Deluge]
        downloads show from "Torrent Provider" to "/mnt/download/incomplete/torrent"
		once complete, moves from "/mnt/download/incomplete/torrent" to "/mnt/download"
	    sends notification to "TorrentToMedia.py" with "movies" label
    [SabNZBD] 
        downloads show from "Usenet Provider" to "/mnt/download/incomplete/nzb"
		once complete, moves from "/mnt/download/incomplete/nzb" to "/mnt/download"
        sends notification to "nzbToCouchpotato.py" with "movies" category
    [nzbToCouchpotato.py] 
        creates hardlink from "/mnt/download" to: "/media/download/incomplete/process/movies"
        sends notification to "Couchpotato"
    [TorrentToMedia.py] 
        creates hardlink from "/mnt/download" to: "/media/download/incomplete/process/movies"
        sends notification to "Couchpotato"
    [Couchpotato] 
        renames "/media/download/incomplete/process/movies/{old_showname}" to "/media/movies/{new_showname}"
        sends notification to "Plex"
    [Plex] 
        refreshes media library

#### eBook FLOW
    [Lazy Librarian]
        User requests a book
        searches "Torrent Providers" for torrents
	        Success: sends .torrent url to "Deluge"
        searches "Usenet Providers" for nzbs
	        Success: sends .nzb url to "SabNZBD"
    [Deluge]
        downloads show from "Torrent Provider" to "/mnt/download/incomplete/torrent"
		once complete, moves from "/mnt/download/incomplete/torrent" to "/mnt/download"
	    sends notification to "TorrentToMedia.py" with "ebooks" label
    [SabNZBD] 
        downloads show from "Usenet Provider" to "/mnt/download/incomplete/nzb"
		once complete, moves from "/mnt/download/incomplete/nzb" to "/mnt/download"
        sends notification to "nzbToLazyLibrarian.py" with "ebooks" category
    [nzbToLazyLibrarian.py] 
        creates hardlink from "/mnt/download" to: "/media/download/incomplete/process/ebooks"
        sends notification to "Lazy Librarian"
    [TorrentToMedia.py] 
        creates hardlink from "/mnt/download" to: "/media/download/incomplete/process/ebooks"
        sends notification to "Lazy Librarian"
    [Lazy Librarian] 
        renames "/media/download/incomplete/process/ebooks/{old_ebookname}" to "/media/ebooks/{new_ebookname}"
        sends notification to "Calibre"
    [Calibre] 
        refreshes media library

#### Music FLOW
    [Headphones]
        User requests a song
        searches "Torrent Providers" for torrents
	        Success: sends .torrent url to "Deluge"
        searches "Usenet Providers" for nzbs
	        Success: sends .nzb url to "SabNZBD"
    [Deluge]
        downloads show from "Torrent Provider" to "/mnt/download/incomplete/torrent"
		once complete, moves from "/mnt/download/incomplete/torrent" to "/mnt/download"
	    sends notification to "TorrentToMedia.py" with "music" label
    [SabNZBD] 
        downloads show from "Usenet Provider" to "/mnt/download/incomplete/nzb"
		once complete, moves from "/mnt/download/incomplete/nzb" to "/mnt/download"
        sends notification to "nzbToHeadphones.py" with "music" category
    [nzbToHeadphones.py] 
        creates hardlink from "/mnt/download" to: "/media/download/incomplete/process/music"
        sends notification to "Headphones"
    [TorrentToMedia.py] 
        creates hardlink from "/mnt/download" to: "/media/download/incomplete/process/music"
        sends notification to "Headphones"
    [Headphones] 
        renames "/media/download/incomplete/process/music/{old_songname}" to "/media/music/{new_songname}"
        sends notification to "Madsonic"
    [Madsonic 
        refreshes media library
