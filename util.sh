#!/bin/bash

_addTmuxWindow() {
  if [ ${#1} -gt 0 ]; then scriptPath="$1"; else return; fi
  if [ ${#2} -gt 0 ]; then appName="$2"; else return; fi
  if [ ${#3} -gt 0 ]; then appConfigPath="$3"; else return; fi

  tmux splitw -t "$sessionName" "$scriptPath \"$appConfigPath\"\;"
  tmux selectl -t "$sessionName" tiled
}

_appendLine() {
  if [ ${#1} -gt 0 ]; then fileName="$1"; else echo "Missing \"\$fileName\" parameter"; return; fi
  if [ ${#2} -gt 0 ]; then searchKey="$2"; else echo "Missing \"\$searchKey\" parameter"; return; fi
  if [ ${#3} -gt 0 ]; then newValue="$3"; else echo "Missing \"\$newValue\" parameter"; return; fi
  if [ ${#4} -gt 0 ]; then searchSection="$4"; fi

	grepResults="$(_getGrepResults "$searchKey" "$fileName")";
	if [ ${#grepResults} -eq 0 ]; then
		echo "${searchKey//\\/}" >> "$fileName"
	fi
	if [ ${#searchSection} -gt 0 ];
		then
			ex "$fileName" <<-APPENDSTRING1
				/$searchSection
				/$searchKey
				a
				$newValue
				.
				wq
			APPENDSTRING1
		else
			ex "$fileName" <<-APPENDSTRING2
				/$searchKey
				a
				$newValue
				.
				wq
			APPENDSTRING2
	fi
}

_backupOldAppConfigs() {
  if [ ${#1} -gt 0 ]; then appsFolderToBackup="$1"; else appsFolderToBackup="/mnt/$zpool_name/apps"; fi
  if [ ${#2} -gt 0 ]; then backupFileDestinationPath="$2"; else backupFileDestinationPath="/mnt/$zpool_name"; fi
  filename=$backupFileDestinationPath/app_configs_bak
  if [[ -e $filename.tar.gz ]] ; then
    i=0
    while [[ -e $filename-$i.tar.gz ]] ; do
        let i++
    done
    filename=$filename-$i
  fi
  tar -zcvf "$filename".tar.gz $appsFolderToBackup
}

_checkInstallResults() {
  set +x;
  searchString="Traceback\|exceptions.ValueError\|command not found\|Error: near line \|There is no calibre library\|Pattern not found\|unescaped newline inside substitute pattern\|no such variable\|WARNING: failed to start service\|No such file or directory\|\binvalid\b\|not executable\|initial ref transaction called with existing refs"
  appName="install"
  displayRows="2"
  fileName="install"
  if [ ${#1} -gt 0 ]; then searchString="$1"; fi
  if [ ${#2} -gt 0 ]; then
    appName="$2";
    fileName="$appName-install";
  fi
  if [ ${#3} -gt 0 ]; then displayRows="$3"; fi
  logFilePath="$HOME/$fileName.log"
  logErrFilePath="$HOME/$fileName.err.log"

  grepResults="$(_getGrepResults "$searchString" "$logFilePath")";
  if [ ${#grepResults} -gt 0 ];
    then
      echo "$displayGrepResults";
      _fatalError "Application ($appName) Installation failed" "See above for error.";
    else
      _log 24 "$" "[$appName] Application's installation status" "SUCCESS" 0
  fi
  if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi
}

_checkPkgInstallResults() {
  set +x;
  searchString="Service Unavailable\|RuntimeError\|pkg error:\|Error updating repositories\|reset\|Operation timed out";
  appName="install"
  displayRows="2"
  fileName="install"
  if [ ${#1} -gt 0 ]; then searchString="$1"; fi
  if [ ${#2} -gt 0 ]; then
    appName="$2";
    fileName="$appName-install";
  fi
  if [ ${#3} -gt 0 ]; then displayRows="$3"; fi
  logFilePath="$HOME/$fileName.log"
  logErrFilePath="$HOME/$fileName.err.log"

  grepResults="$(_getGrepResults "$searchString" "$logFilePath")";
  if [ ${#grepResults} -gt 0 ];
    then
      _checkInstallResults "" "$appName" ""
      set -x;
      exec > >(tee -a "$logErrFilePath") 2>&1;
      displayGrepResults="$(_getGrepResults "$searchString" "$logFilePath" "$displayRows")";
      echo "$displayGrepResults";
      _fatalError "Application ($appName) packages failed to install" "See above for error.";
    else
      _log 24 "+" "[$appName] Package installation status" "SUCCESS" 0
  fi
  if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi
}

_checkSetupResults() {
  set +x;
  searchString="Error: near line \|Pattern not found\|unescaped newline inside substitute pattern\|no such variable\|No such file or directory\|\binvalid\b\|not executable"
  appName="setup"
  displayRows="2"
  fileName="setup"
  if [ ${#1} -gt 0 ]; then searchString="$1"; fi
  if [ ${#2} -gt 0 ]; then
    appName="$2";
    fileName="$appName-setup";
  fi
  if [ ${#3} -gt 0 ]; then displayRows="$3"; fi
  logFilePath="$HOME/$fileName.log"
  logErrFilePath="$HOME/$fileName.err.log"

  grepResults="$(_getGrepResults "$searchString" "$logFilePath")";
  if [ ${#grepResults} -gt 0 ];
    then
      set -x;
      exec > >(tee -a "$logErrFilePath") 2>&1;
      displayGrepResults="$(_getGrepResults "$searchString" "$logFilePath" "$displayRows")";
      echo "$displayGrepResults";
      _fatalError "Application ($appName) Setup failed" "See above for error.";
    else
      _log 24 "$" "[$appName] Application's Setup Status" "SUCCESS" 0
  fi
  if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi
}

_destroyJail() { # _destroyJail(isAppEnabled, jailName)
  set +x
  isAppEnabled="$1"
  jName="$2"
  if [ "$isAppEnabled" == "YES" ];
    then
        _log 16 "+" "$jName" "ENABLED" 0
        grepResults="$(_getJailExistsGrepResults $jName)"
        if [ ${#grepResults} -gt 0 ]; then iocage destroy --recursive $jName; fi
    else
        _log 0 "-" "$jName" "DISABLED" 0
  fi
  if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi
}

_fatalError() {
  if [ ${#1} -gt 0 ]; then appName="$1"; fi
  if [ ${#2} -gt 0 ]; then primaryMessage="$2"; fi
  if [ ${#3} -gt 0 ]; then secondMessage="$3"; fi
  if [ ${#4} -gt 0 ]; then logFilePath="$4"; fi

  if [ ${#primaryMessage} -gt 0 ] || [ ${#secondMessage} -gt 0 ]; then _log 8 "!" "FATAL ERROR: $primaryMessage" "$secondMessage" 2; fi
  exec > >(tee -a "$logFilePath") 2>&1;
  set +x
  _log 16 "!" "Press any key to close window" "$appName" 0;
  read -t 1 -n 10000 discard
  read -sn 1
  if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi
  exit;
}

_getJailExistsGrepResults() {
  appName="$1"
  iocage list | grep -iw "$appName";
}

_getGrepResults() {
  searchString="$1";
  fileName="$2";
  if [ ${#3} -gt 0 ];
    then
      grep --color -C "$3" -ne "$searchString" "$fileName";
    else
      grep -ne "$searchString" "$fileName";
  fi
}

_getPlexAuthToken(){
  user_name="$1"
  user_password="$2"
  curl -X "POST" "https://plex.tv/users/sign_in.json" -H "X-Plex-Version: 1.0.0" -H "X-Plex-Product: YOUR PRODUCT NAME" -H "X-Plex-Client-Identifier: YOUR-PRODUCT-ID" -H "Content-Type: application/x-www-form-urlencoded; charset=utf-8" --data-urlencode "user[password]=$user_password" --data-urlencode "user[login]=$user_name" | sed -e 's/.*authToken\":\"\(.*\)\",\"authentication_token.*/\1/'
}

_getProperty() {
  if [ ${#1} -gt 0 ]; then fileName="$1"; else fileName=""; fi
  if [ ${#2} -gt 0 ]; then key="$2"; else key=""; fi
  if [ ${#3} -gt 0 ]; then section="$3"; else section=""; fi
  value=$(sed -n '/^[ \t]*'$section'/,/\[/s/^[ \t]*'$key'[ \t]*[=:][ \t]*//p' "$fileName")
  value=$(echo "$value" | tr -d '\040\011\012\015')
  echo "$value";
}

_getSqlValue() {
  set +x
	if [ ${#1} -gt 0 ]; then table="$1"; else echo "Missing \"\$table\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#2} -gt 0 ]; then returnColName="$2"; else echo "Missing \"\$returnColName\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#4} -gt 0 ]; then whereColName="$4"; else echo "Missing \"\$whereColName\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#3} -gt 0 ]; then whereColValue="$3"; else echo "Missing \"\$whereColValue\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#5} -gt 0 ]; then dbPath="$5"; else dbPath="$appConfigPath"; fi

	sqlite3 "$dbPath" <<-SQL_STATEMENT
		SELECT $returnColName FROM $table WHERE $whereColName="$whereColValue"
	SQL_STATEMENT
  if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi
}

_getXmlAttributeValue() {
  if [ ${#1} -gt 0 ]; then fileName="$1"; else echo "Missing \"\$fileName\" parameter"; return; fi
  if [ ${#2} -gt 0 ]; then searchString="$2"; else echo "Missing \"\$searchString\" parameter"; return; fi
	sed -rn 's/.*'"$searchString"'="([^"]+)".*/\1/p' "$fileName"
}

_getXmlValue() {
  if [ ${#1} -gt 0 ]; then filePath="$1"; else echo "Missing \"\$fileName\" parameter"; return; fi
  if [ ${#2} -gt 0 ]; then xpath="$2"; else echo "Missing \"\$searchString\" parameter"; return; fi
	apikey=$(xmllint --xpath "string($xpath)" "$filePath")
	echo "${apikey//\\/}"
}

_insertLine() {
  if [ ${#1} -gt 0 ]; then fileName="$1"; else echo "Missing \"\$fileName\" parameter"; return; fi
  if [ ${#2} -gt 0 ]; then searchKey="$2"; else echo "Missing \"\$searchKey\" parameter"; return; fi
  if [ ${#3} -gt 0 ]; then newValue="$3"; else echo "Missing \"\$newValue\" parameter"; return; fi
  if [ ${#4} -gt 0 ]; then searchSection="$4"; fi

	grepResults="$(_getGrepResults "$searchKey" "$fileName")";
	if [ ${#grepResults} -eq 0 ]; then
		echo "${searchKey//\\/}" >> "$fileName"
	fi
	if [ ${#searchSection} -gt 0 ];
		then
			ex "$fileName" <<-INSERTSTRING1
				/$searchSection
				/$searchKey
				i
				$newValue
				.
				wq
			INSERTSTRING1
		else
			ex "$fileName" <<-INSERTSTRING2
				/$searchKey
				i
				$newValue
				.
				wq
			INSERTSTRING2
	fi
}

_isConfigFileCreated() {
  if [ ${#1} -gt 0 ];
    then configFilePath="$1";
    else _fatalError "No filename passed to _isConfigFileCreated()" "";
  fi
  if [ ${#2} -gt 0 ]; then countlimit="$2"; else countlimit="20"; fi
  if [ ${#3} -gt 0 ]; then delayInSeconds="$3"; else delayInSeconds="2"; fi
  set +x

  countlimit=$((countlimit + 0)) # force to integer
  delayInSeconds=$((delayInSeconds + 0)) # force to integer
  ctr=0;
  until [ $ctr -gt $countlimit ] || [ -f "$configFilePath" ]; do
    sleep $delayInSeconds;
    (( ctr++ ));
  done

  if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi
  if [ -f "$configFilePath" ]; then return 0; else return 1; fi
}

_launchScript() { # _launchConnfigurationScript(scriptPath, sessionName, appNameLabel, titleLabel, isAppEnabled)
  set +x
  scriptPath="$1"
  sessionName="$2"
  appNameLabel="$3"
  titleLabel="$4"
  isAppEnabled="$5"
  if [ "$isAppEnabled" == "YES" ];
    then
        _log 32 "+" "$appNameLabel ($titleLabel)" "ENABLED" 1
        tmux splitw -t "$sessionName" "$scriptPath $log_enabled\;"
        tmux selectl -t "$sessionName" tiled
    else
        _log 0 "-" "$appNameLabel ($titleLabel)" "DISABLED" 0
  fi
  if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi
}

_log() { # (numIndents, borderCharacter, message, status, numPaddedLines)
  set +x
  numIndents=$1
  borderCharacter="$2"
  title="$3";
  status=": $4"
  numPaddedLines=$5
  border=""
  indent=""
  now=$(date +"%T")
  if [ ! -n "$4" ]; then status=""; fi
  if [ ! -n "$borderCharacter" ]; then
      title="[$now] $title$status";
    else
      title="$borderCharacter [$now] $title$status $borderCharacter";
  fi
  if [ ! -n "$numPaddedLines" ]; then numPaddedLines=0; fi

  i=1
  while [ $i -le ${numIndents} ]; do
    indent="$indent ";
    (( i++ ));
  done

  i=1
  while [ $i -le ${#title} ]; do
    border="$border$borderCharacter";
    (( i++ ));
  done

  i=1
  while [ $i -le ${numPaddedLines} ]; do
    echo "";
    (( i++ ));
  done

  if [ ${#border} -gt 0 ]; then echo "$indent$border"; fi
  echo "$indent$title";
  if [ ${#border} -gt 0 ]; then echo "$indent$border"; fi

  i=1
  while [ $i -le ${numPaddedLines} ]; do
    echo '';
    (( i++ ));
  done

  if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi
}

_renameExistingKey() {
  if [ ${#1} -gt 0 ]; then fileName="$1"; else echo "Missing \"\$fileName\" parameter"; return; fi
  if [ ${#2} -gt 0 ]; then oldValue="$2"; else echo "Missing \"\$oldValue\" parameter"; return; fi
  if [ ${#3} -gt 0 ]; then newValue="$3"; else echo "Missing \"\$newValue\" parameter"; return; fi

	sed -i '' "s/$oldValue/$newValue/g" "$fileName";
}

_replaceConfig() {
  fileName="$1"
  section="$2"
  key="$3"
  originalValue="$4"
  newValue="$5"
	ex "$fileName" <<-REPLACESTRING1
		/$section
		/$key
		s?$originalValue?$newValue?
		wq
	REPLACESTRING1
}

_replaceSqlJsonPropertyValue() {
	if [ ${#1} -gt 0 ]; then table="$1"; else echo "Missing \"\$table\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#2} -gt 0 ]; then keyColName="$2"; else echo "Missing \"\$keyColName\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#3} -gt 0 ]; then jsonProperty="$3"; else echo "Missing \"\$jsonProperty\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#4} -gt 0 ]; then newPropertyValue="$4"; else echo "Missing \"\$newPropertyValue\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#5} -gt 0 ]; then whereColName="$5"; else echo "Missing \"\$whereColName\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#6} -gt 0 ]; then whereColValue="$6"; else echo "Missing \"\$whereColValue\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#7} -gt 0 ]; then dbPath="$7"; else dbPath="$appConfigPath"; fi

	settingsValue=$(_getSqlValue "$table" "$keyColName" "$whereColName" "$whereColValue" "$dbPath");
	newSettings=$(echo "$settingsValue" | jq -r ".$jsonProperty=\"$newPropertyValue\"")
	newSettings=$(echo "$newSettings" | tr -ds '\n\t' '[:space:]')
	sqlite3 "$dbPath" <<-SQL_STATEMENT
					UPDATE $table SET $keyColName = '$newSettings' WHERE $whereColName=$whereColValue;
	SQL_STATEMENT
}

_replaceSqlSingleValueSubstring() {
#  set +x
	if [ ${#1} -gt 0 ]; then table="$1"; else echo "Missing \"\$table\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#2} -gt 0 ]; then keyColName="$2"; else echo "Missing \"\$keyColName\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#3} -gt 0 ]; then keyColValue="$3"; else echo "Missing \"\$keyColValue\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#4} -gt 0 ]; then valueColName="$4"; else echo "Missing \"\$valueColName\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#5} -gt 0 ]; then pattern="$5"; else echo "Missing \"\$pattern\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#6} -gt 0 ]; then valueColValue="$6"; else echo "Missing \"\$valueColValue\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#7} -gt 0 ]; then dbPath="$7"; else dbPath="$appConfigPath"; fi

	sqlite3 "$dbPath" <<-SQL_STATEMENT
		UPDATE $table SET $valueColName = REPLACE($valueColName,'$pattern','$valueColValue') WHERE $keyColName=$keyColValue;
	SQL_STATEMENT
  if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi
}

_replaceXmlAttribute() {
  if [ ${#1} -gt 0 ]; then filePath="$1"; else echo "Missing \"\$filePath\" parameter"; return; fi
  if [ ${#2} -gt 0 ]; then element="$2"; else echo "Missing \"\$element\" parameter"; return; fi
  if [ ${#3} -gt 0 ]; then attribute="$3"; else echo "Missing \"\$attribute\" parameter"; return; fi
  if [ ${#4} -gt 0 ]; then value="$4"; else echo "Missing \"\$value\" parameter"; return; fi
	isAttributeAlreadyPresent=$(_getGrepResults "$attribute" "$appConfigPath");
	if [ ${#isAttributeAlreadyPresent} -gt 0 ]; then
			_replaceConfig "$filePath" "<[:space:]*$element" "$attribute[:space:]*=[:space:]*\"" "=[:space:]*\".*\"" "=\"$value\"";
		else
			_replaceConfig "$filePath" "<[:space:]*$element" "[:space:]*/>" "[:space:]*/>" " $attribute=\"$value\" />";
	fi
	sed -i '' -e 's?        ??g' "$filePath"
}

_restartService() {
  set +x
  if [ ${#1} -gt 0 ]; then sName="$1"; else sName=""; fi
  if [ ${#2} -gt 0 ]; then jName="$2"; else jName=""; fi
  if [ ${#3} -gt 0 ]; then countlimit="$3"; else countlimit="30"; fi
  if [ ${#4} -gt 0 ]; then delayInSeconds="$4"; else delayInSeconds="2"; fi
  searchString="WARNING: failed to stop service"
  stopServiceError="$(_stopService $sName $jName $countlimit | grep "$searchString")";
  if [ ${#stopServiceError} -gt 0 ]; then
    if [ "$log_enabled" == "TRUE" ];
      then set -x;
      else set +x;
    fi;
    _log 4 "!" "WARNING: failure while stopping service ($sName)" "$stopServiceError" 0;
    return;
  fi
  sleep 1
  iocage exec $jName "service $sName start";
  sleep 1
  serviceRunning="$(_serviceRunning $sName $jName)";
  countlimit=$((countlimit + 0)) # force to integer
  delayInSeconds=$((delayInSeconds + 0)) # force to integer
  ctr=0;
  until [ $ctr -gt $countlimit ] || [ ${#serviceRunning} -gt 0 ]; do
    sleep $delayInSeconds;
    serviceRunning="$(_serviceRunning $sName $jName)";
    (( ctr++ ));
  done

  if [ $ctr -gt $countlimit ]; then _log 4 "!" "WARNING: failed to start service ($sName) after $countlimit failed attempts" "" 0; fi
  if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi
}

_serviceRunning() {
  if [ ${#1} -gt 0 ]; then sName="$1"; else sName="$appServiceName"; fi
  if [ ${#2} -gt 0 ]; then jName="$2"; else jName="$jail_name"; fi
  searchString="is running as \|Started Madsonic \[PID \|Lazylibrarian (pid \|Starting headphones"
  serviceRunning="$(iocage exec $jName "service $sName status | grep \"$searchString\"")";
  serviceRunning="${serviceRunning#*pid }";
  serviceRunning="${serviceRunning#*PID }";
  serviceRunning="${serviceRunning#*as }";
  serviceRunning="${serviceRunning%.*}";
  echo "$serviceRunning";
}

_setSqlProperty() {
  set +x
	if [ ${#1} -gt 0 ]; then table="$1"; else echo "Missing \"\$table\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#2} -gt 0 ]; then keyColName="$2"; else echo "Missing \"\$keyColName\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#3} -gt 0 ]; then keyColValue="$3"; else echo "Missing \"\$keyColValue\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#4} -gt 0 ]; then valueColName="$4"; else echo "Missing \"\$valueColName\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#5} -gt 0 ]; then valueColValue="$5"; else echo "Missing \"\$valueColValue\" parameter"; if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi; return; fi
	if [ ${#6} -gt 0 ]; then dbPath="$6"; else dbPath="$appConfigPath"; fi

	sqlite3 "$dbPath" <<-SQL_STATEMENT
		REPLACE INTO $table ($keyColName,$valueColName) VALUES ($keyColValue,$valueColValue);
	SQL_STATEMENT
  if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi
}

_setXmlValue() {
  if [ ${#1} -gt 0 ]; then filePath="$1"; else echo "Missing \"\$fileName\" parameter"; return; fi
  if [ ${#2} -gt 0 ]; then xpath="$2"; else echo "Missing \"\$searchString\" parameter"; return; fi
  if [ ${#3} -gt 0 ]; then newValue="$3"; else echo "Missing \"\$newValue\" parameter"; return; fi

	xmllint --shell "$filePath" <<-EOF
					cd $xpath
					set $newValue
					save
	EOF
}

_stopService() {
  set +x
  if [ ${#1} -gt 0 ]; then sName="$1"; else sName=""; fi
  if [ ${#2} -gt 0 ]; then jName="$2"; else jName=""; fi
  if [ ${#3} -gt 0 ]; then countlimit="$3"; else countlimit="20"; fi
  if [ ${#4} -gt 0 ]; then delayInSeconds="$4"; else delayInSeconds="2"; fi
  serviceRunning="$(_serviceRunning $sName $jName)";
  sleep 1;
  if [ ${#serviceRunning} -gt 0 ];
    then
      iocage exec $jName "service $sName stop";
      ctr=0;
      countlimit=$((countlimit + 0)); # force to integer
      delayInSeconds=$((delayInSeconds + 0)); # force to integer
      while ([ ! ${#serviceRunning} -eq 0 ] && [ $ctr -le $countlimit ]); do
        sleep $delayInSeconds;
        serviceRunning="$(_serviceRunning $sName $jName)";
        (( ctr++ ));
      done
      if [ $ctr -ge $countlimit ]; then _log 4 "!" "WARNING: failed to stop service ($sName)" "" 0; fi
  fi
  if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi
}

_updateApiKeys() {
  source "$(dirname "$0")/../common-config.sh"
  zpool_name=$zpool_name
  appNames=( "sabnzbd" "plex" "medusa" "lazylibrarian" "couchpotato" "radarr" "headphones" );
  for sourceAppName in "${appNames[@]}"
  do
		case $sourceAppName in
			sabnzbd)
				if [ "$sabnzbd_enabled" == "YES" ]; then
					app_api_key=$(_getProperty "/mnt/$zpool_name/apps/$sourceAppName/sabnzbd.ini" "api_key" "\[misc\]");
					if [ ${#app_api_key} -eq 0  ]; then app_api_key=$sabnzbd_api_key; fi
					if [ ${#app_api_key} -gt 0  ]; then
						if [ "$couchpotato_enabled" == "YES" ]; 	then _replaceConfig "/mnt/$zpool_name/apps/couchpotato/settings.conf" 			"\[sabnzbd\]"	"api_key ="					"=.*$" 			"= $app_api_key"; fi
						if [ "$radarr_enabled" == "YES" ] && [ -f "/mnt/$zpool_name/apps/radarr/nzbdrone.db" ]; then
							settingsValue=$(_getSqlValue "main.DownloadClients" "Settings" "Implementation" "\"Sabnzbd\"" "/mnt/$zpool_name/apps/radarr/nzbdrone.db");
							newSettings=$(echo "$settingsValue" | jq -r ".apiKey=\"$app_api_key\"")
							newSettings=$(echo "$newSettings" | tr -ds '\n\t' '[:space:]')
							sqlite3 "/mnt/$zpool_name/apps/radarr/nzbdrone.db" <<-SQL_STATEMENT
											UPDATE main.DownloadClients SET Settings = '$newSettings' WHERE Implementation="Sabnzbd";
							SQL_STATEMENT
						fi
						if [ "$medusa_enabled" == "YES" ]; 				then _replaceConfig "/mnt/$zpool_name/apps/medusa/config.ini"               "\[SABnzbd\]" "sab_apikey ="     	"=.*$" 			"= $app_api_key"; fi
						if [ "$lazylibrarian_enabled" == "YES" ]; then _replaceConfig "/mnt/$zpool_name/apps/lazylibrarian/config.ini"        "\[SABnzbd\]" "sab_api ="        	"=.*$" 			"= $app_api_key"; fi
						if [ "$headphones_enabled" == "YES" ]; 		then _replaceConfig "/mnt/$zpool_name/apps/headphones/config.ini"           "\[SABnzbd\]" "sab_apikey ="     	"=.*$" 			"= $app_api_key"; fi
						if [ "$nzbtomedia_enabled" == "YES" ]; 		then _replaceConfig "/mnt/$zpool_name/apps/nzbtomedia/autoProcessMedia.cfg" "\[Nzb\]"     "sabnzbd_apikey =" 	"=.*$" 			"= $app_api_key"; fi
					fi
				fi
			;;
			plex)
				if [ "$plex_enabled" == "YES" ]; 	then app_api_key=$(_getXmlAttributeValue "/mnt/$zpool_name/apps/$sourceAppName/Plex Media Server/Preferences.xml" "PlexOnlineToken"); fi
				if [ ${#app_api_key} -eq 0  ]; then app_api_key=$plex_api_key; fi
				if [ ${#app_api_key} -gt 0  ]; then
					if [ "$couchpotato_enabled" == "YES" ]; 	then _replaceConfig "/mnt/$zpool_name/apps/couchpotato/settings.conf"         "\[plex\]"    "auth_token ="          "=.*$"    "= $app_api_key"; fi
					if [ "$medusa_enabled" == "YES" ]; 				then _replaceConfig "/mnt/$zpool_name/apps/medusa/config.ini"                 "\[Plex\]"    "plex_server_token ="   "=.*$"    "= $app_api_key"; fi
					if [ "$headphones_enabled" == "YES" ]; 		then _replaceConfig "/mnt/$zpool_name/apps/headphones/config.ini"             "\[Plex\]"    "plex_token ="          "=.*$"    "= $app_api_key"; fi
#					if [ "$nzbtomedia_enabled" == "YES" ]; 		then _replaceConfig "/mnt/$zpool_name/apps/nzbtomedia/autoProcessMedia.cfg"   "\[Plex\]"    "plex_token ="          "=.*$"    "= $app_api_key"; fi
				fi
			;;
			medusa)
				if [ "$medusa_enabled" == "YES" ]; 		then app_api_key=$(_getProperty "/mnt/$zpool_name/apps/$sourceAppName/config.ini" "api_key" "\[General\]"); fi
				if [ ${#app_api_key} -eq 0  ]; then app_api_key=$medusa_api_key; fi
				if [ ${#app_api_key} -gt 0  ]; then
					if [ "$nzbtomedia_enabled" == "YES" ]; 		then _replaceConfig "/mnt/$zpool_name/apps/nzbtomedia/autoProcessMedia.cfg"   "\[SickBeard\]"   		"apikey ="    "=.*$"    "= $app_api_key"; fi
				fi
			;;
			lazylibrarian)
				if [ "$lazylibrarian_enabled" == "YES" ]; 		then app_api_key=$(_getProperty "/mnt/$zpool_name/apps/$sourceAppName/config.ini" "api_key" "\[General\]"); fi
				if [ ${#app_api_key} -eq 0  ]; then app_api_key=$lazylibrarian_api_key;	fi
				if [ ${#app_api_key} -gt 0  ]; then
					if [ "$nzbtomedia_enabled" == "YES" ]; 		then _replaceConfig "/mnt/$zpool_name/apps/nzbtomedia/autoProcessMedia.cfg"   "\[LazyLibrarian\]"   "apikey ="    "=.*$"    "= $app_api_key"; fi
				fi
			;;
			couchpotato)
				if [ "$couchpotato_enabled" == "YES" ]; 	then app_api_key=$(_getProperty "/mnt/$zpool_name/apps/$sourceAppName/settings.conf" "api_key" "\[core\]"); fi
				if [ ${#app_api_key} -eq 0  ]; then app_api_key=$couchpotato_api_key;	fi
				if [ ${#app_api_key} -gt 0  ]; then
					if [ "$nzbtomedia_enabled" == "YES" ]; 		then _replaceConfig "/mnt/$zpool_name/apps/nzbtomedia/autoProcessMedia.cfg"   "\[CouchPotato\]"   	"apikey ="    "=.*$"    "= $app_api_key"; fi
				fi
			;;
			radarr)
				if [ "$radarr_enabled" == "YES" ] && [ -f "/mnt/$zpool_name/apps/$appName/nzbdrone.db" ]; then app_api_key=$(_getXmlValue "/mnt/$zpool_name/apps/$sourceAppName/config.xml" "/Config/ApiKey"); fi
				if [ ${#app_api_key} -eq 0  ]; then app_api_key=$radarr_api_key;	fi
				if [ ${#app_api_key} -gt 0  ]; then
					if [ "$nzbtomedia_enabled" == "YES" ]; 		then _replaceConfig "/mnt/$zpool_name/apps/nzbtomedia/autoProcessMedia.cfg"   "\[Radarr\]"   				"apikey ="    "=.*$"    "= $app_api_key"; fi
				fi
			;;
			headphones)
				if [ "$headphones_enabled" == "YES" ]; 		then app_api_key=$(_getProperty "/mnt/$zpool_name/apps/$sourceAppName/config.ini" "api_key" "\[General\]"); fi
				if [ ${#app_api_key} -eq 0  ]; then app_api_key=$headphones_api_key; fi
				if [ ${#app_api_key} -gt 0  ]; then
					if [ "$nzbtomedia_enabled" == "YES" ]; 		then _replaceConfig "/mnt/$zpool_name/apps/nzbtomedia/autoProcessMedia.cfg"   "\[HeadPhones\]"    	"apikey ="    "=.*$"    "= $app_api_key"; fi
				fi
			;;
			*)
			;;
		esac
  done
}