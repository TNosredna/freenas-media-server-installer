#!/bin/bash
version="1.1"
is_update=$1

chmod -R 777 $(dirname "$0") || true                  # Set permissions on installation scripts
source $(dirname "$0")/util.sh                        # this needs to be here because _log is used
#jailFreebsdVersion=`freebsd-version`
jailFreebsdVersion="13.1-RELEASE"
jailFreebsdVersion=$(echo "$jailFreebsdVersion" | tr -d '\040\011\012\015')
_replaceConfig "$HOME/common-config.sh" "jail_freebsd_version="		"jail_freebsd_version="			"=\".*\"$"		"=\"$jailFreebsdVersion\"";
source $(dirname "$0")/common-config.sh     # Import common configuration.  This must be here because freebsd version can't be updated once it's been "sourced"

if [ "updated" != "$is_update" ];
  then
    _log 16 "@" "AUTO-UPDATING INSTALLATION SCRIPT" "" 4;
    echo "#!/bin/bash" > $HOME/install.tmp.sh
    echo "source \$HOME/common-config.sh" >> $HOME/install.tmp.sh
    echo "rsync --ignore-existing \$HOME/freenas-media-server-installer/common-config.sh \$HOME/" >> $HOME/install.tmp.sh
    echo "rsync --ignore-existing \$HOME/freenas-media-server-installer/create_app_jails/openvpn/openvpn.ovpn \$HOME/" >> $HOME/install.tmp.sh
    echo "rm -rf \$HOME/freenas-media-server-installer" >> $HOME/install.tmp.sh
    echo "rm -rf \$HOME/freenas-media-server-installer.zip" >> $HOME/install.tmp.sh
    echo "wget -O freenas-media-server-installer.zip -P \$HOME \"\$checkoutURL\"" >> $HOME/install.tmp.sh
    echo "unzip -d \$HOME \$HOME/freenas-media-server-installer.zip" >> $HOME/install.tmp.sh
    echo "mv \$HOME/freenas-media-server-installer*/ \$HOME/freenas-media-server-installer" >> $HOME/install.tmp.sh
    echo "rm -rf \$HOME/freenas-media-server-installer.zip" >> $HOME/install.tmp.sh
    echo "rsync --ignore-existing \$HOME/freenas-media-server-installer/common-config.sh \$HOME/" >> $HOME/install.tmp.sh
    echo "rsync --ignore-existing \$HOME/freenas-media-server-installer/create_app_jails/openvpn/openvpn.ovpn \$HOME/" >> $HOME/install.tmp.sh
    echo "cp -f \$HOME/common-config.sh \$HOME/freenas-media-server-installer/" >> $HOME/install.tmp.sh
    echo "cp -f \$HOME/openvpn.ovpn \$HOME/freenas-media-server-installer/create_app_jails/openvpn/" >> $HOME/install.tmp.sh
    # if flag equals "updateonly", it means to quit with only doing an update.  Mostly used for testing/dev purposes.
    if [ "updateonly" != "$is_update" ]; then echo "tmux new -n Installer -s FreenasMediaInstallation \"\$HOME/freenas-media-server-installer/install.sh updated\"\; selectl tiled\;" >> $HOME/install.tmp.sh; fi
    chmod 777 $HOME/install.tmp.sh
    exec $HOME/install.tmp.sh
    exit
fi
rm -f "$HOME/install.tmp.sh"
rm -f "$HOME/install.log"
rm -f "$HOME/install.err.log"

# Change redirection to file and screen
exec > >(tee -a "$HOME/install.log") 2>&1

_log 0 "@" "Running FreeNAS iocage Media Server Installation" "v$version" 1;

iocage activate $zpool_name                                     # Activate zPool for iocage use

if [ ! -f $HOME/fetched_$jailFreebsdVersion ]; then             # if 'fetched' file exists
    iocage fetch -r "$jailFreebsdVersion"                       # 'Fetch' Freebsd iocage version for jail use
    if [ $? -eq 0 ]; then                                       # If no error fetching, create 'fetched' file
        _log 0 "" "NO ERROR" "$?" 0;
        touch $HOME/fetched_$jailFreebsdVersion
    else                                                        # if error fetching, do not create a 'fetched' file
        _log 0 "!" "ERROR" "$?" 0;
        rm $HOME/fetched_$jailFreebsdVersion
    fi
  else
    _log 0 "" "FreeBSD v$jailFreebsdVersion already fetched.  Skipping 'fetch'." "" 0;
fi

# Allow use of tunnel devices (eg VPN)
devfs rule -s 4 add path 'tun*' unhide
$(dirname "$0")/create_app_jails/setupDatasetsAndNfsShares.py -H $freenas_ip -u $root_user_name -p $root_user_password -m $media_user_name -g $media_group_name -z $zpool_name
$(dirname "$0")/create_app_jails/setupDatasetsAndNfsShares.py -H $freenas_ip -u $root_user_name -p $root_user_password -m $media_user_name -g $media_group_name -z $zpool_name -a "nzbtomedia" -c "nzbToMedia\'s (post processor) configuration, databases, and logs"
# Create download folders
mkdir -p /mnt/$zpool_name/download
mkdir -p /mnt/$zpool_name/download/incomplete
mkdir -p /mnt/$zpool_name/download/incomplete/torrent
mkdir -p /mnt/$zpool_name/download/incomplete/nzb
mkdir -p /mnt/$zpool_name/download/incomplete/process
mkdir -p /mnt/$zpool_name/download/incomplete/process/tv
mkdir -p /mnt/$zpool_name/download/incomplete/process/movies
mkdir -p /mnt/$zpool_name/download/incomplete/process/ebooks
mkdir -p /mnt/$zpool_name/download/incomplete/process/music
mkdir -p /mnt/$zpool_name/download/incomplete/process/audiobooks
mkdir -p /mnt/$zpool_name/download/manualImport

mkdir -p /mnt/$zpool_name/media/tv
mkdir -p /mnt/$zpool_name/media/movies
mkdir -p /mnt/$zpool_name/media/movies/G
mkdir -p /mnt/$zpool_name/media/movies/PG
mkdir -p /mnt/$zpool_name/media/movies/PG-13
mkdir -p /mnt/$zpool_name/media/movies/TV-14
mkdir -p /mnt/$zpool_name/media/movies/R
mkdir -p /mnt/$zpool_name/media/movies/Not\ Rated
mkdir -p /mnt/$zpool_name/media/ebooks
mkdir -p /mnt/$zpool_name/media/music
mkdir -p /mnt/$zpool_name/media/pictures
mkdir -p /mnt/$zpool_name/media/homevideos
mkdir -p /mnt/$zpool_name/media/audiobooks

if [ $backup_existing_setups == "YES" ] && [ $setup_apps_enabled == "YES" ]; then _backupOldAppConfigs; fi

if [ $install_jails_enabled == "YES" ]; then
  _destroyJail "$calibre_enabled" "$calibre_jail_name"
  _destroyJail "$couchpotato_enabled" "$couchpotato_jail_name"
  _destroyJail "$radarr_enabled" "$radarr_jail_name"
  _destroyJail "$lazylibrarian_enabled" "$lazylibrarian_jail_name"
  _destroyJail "$deluge_enabled" "$deluge_jail_name"
  _destroyJail "$headphones_enabled" "$headphones_jail_name"
  _destroyJail "$madsonic_enabled" "$madsonic_jail_name"
  _destroyJail "$medusa_enabled" "$medusa_jail_name"
  _destroyJail "$plex_enabled" "$plex_jail_name"
  _destroyJail "$sabnzbd_enabled" "$sabnzbd_jail_name"

  _launchScript "$(dirname "$0")/create_app_jails/createNzbtomediaFolder.sh" "Installer" "NZBTOMEDIA" "CREATE APPLICATION JAIL" "$nzbtomedia_enabled"
  _launchScript "$(dirname "$0")/create_app_jails/createDelugeJail.sh" "Installer" "DELUGE" "CREATE APPLICATION JAIL" "$deluge_enabled"
  _launchScript "$(dirname "$0")/create_app_jails/createSabnzbdJail.sh" "Installer" "SABNZBD" "CREATE APPLICATION JAIL" "$sabnzbd_enabled"
  _launchScript "$(dirname "$0")/create_app_jails/createMedusaJail.sh" "Installer" "MEDUSA" "CREATE APPLICATION JAIL" "$medusa_enabled"
  _launchScript "$(dirname "$0")/create_app_jails/createCouchpotatoJail.sh" "Installer" "COUCHPOTATO" "CREATE APPLICATION JAIL" "$couchpotato_enabled"
  _launchScript "$(dirname "$0")/create_app_jails/createRadarrJail.sh" "Installer" "RADARR" "CREATE APPLICATION JAIL" "$radarr_enabled"
  _launchScript "$(dirname "$0")/create_app_jails/createPlexJail.sh" "Installer" "PLEX" "CREATE APPLICATION JAIL" "$plex_enabled"
  _launchScript "$(dirname "$0")/create_app_jails/createCalibreJail.sh" "Installer" "CALIBRE" "CREATE APPLICATION JAIL" "$calibre_enabled"
  _launchScript "$(dirname "$0")/create_app_jails/createLazylibrarianJail.sh" "Installer" "LAZYLIBARIAN" "CREATE APPLICATION JAIL" "$lazylibrarian_enabled"
  _launchScript "$(dirname "$0")/create_app_jails/createMadsonicJail.sh" "Installer" "MADSONIC" "CREATE APPLICATION JAIL" "$madsonic_enabled"
  _launchScript "$(dirname "$0")/create_app_jails/createHeadphonesJail.sh" "Installer" "HEADPHONES" "CREATE APPLICATION JAIL" "$headphones_enabled"
fi
_checkInstallResults

if [ $install_jails_enabled != "YES" ] && [ $setup_apps_enabled == "YES" ]; then
  _launchScript "$(dirname "$0")/setup_app/setupNzbtomedia.sh" "Installer" "NZBTOMEDIA" "SETUP APPLICATION" "$nzbtomedia_enabled"
  _launchScript "$(dirname "$0")/setup_app/setupDeluge.sh" "Installer" "DELUGE" "SETUP APPLICATION" "$deluge_enabled"
  _launchScript "$(dirname "$0")/setup_app/setupSabnzbd.sh" "Installer" "SABNZBD" "SETUP APPLICATION" "$sabnzbd_enabled"
  _launchScript "$(dirname "$0")/setup_app/setupMedusa.sh" "Installer" "MEDUSA" "SETUP APPLICATION" "$medusa_enabled"
  _launchScript "$(dirname "$0")/setup_app/setupCouchpotato.sh" "Installer" "COUCHPOTATO" "SETUP APPLICATION" "$couchpotato_enabled"
  _launchScript "$(dirname "$0")/setup_app/setupRadarr.sh" "Installer" "RADARR" "SETUP APPLICATION" "$radarr_enabled"
  _launchScript "$(dirname "$0")/setup_app/setupPlex.sh" "Installer" "PLEX" "SETUP APPLICATION" "$plex_enabled"
  _launchScript "$(dirname "$0")/setup_app/setupCalibre.sh" "Installer" "CALIBRE" "SETUP APPLICATION" "$calibre_enabled"
  _launchScript "$(dirname "$0")/setup_app/setupLazylibrarian.sh" "Installer" "LAZYLIBARIAN" "SETUP APPLICATION" "$lazylibrarian_enabled"
  _launchScript "$(dirname "$0")/setup_app/setupMadsonic.sh" "Installer" "MADSONIC" "SETUP APPLICATION" "$madsonic_enabled"
  _launchScript "$(dirname "$0")/setup_app/setupHeadphones.sh" "Installer" "HEADPHONES" "SETUP APPLICATION" "$headphones_enabled"
fi

# Restore standard redirection
exec &>/dev/tty
