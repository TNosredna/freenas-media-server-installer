#!/bin/bash
log_enabled="$1"

appName="lazylibrarian"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-install.log"
log_err_file_path="$HOME/$appName-install.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="lazylibrarian"
appServiceName="lazylibrarian"
applicationPort="$lazylibrarian_jail_port"
appURI="$lazylibrarian_web_root"
#applicationPackageDependencies="perl5 python27 py27-sqlite3 py27-openssl py27-urllib3"
applicationPackageDependencies="perl5 python36 py36-sqlite3 py36-openssl py36-urllib3"
#applicationPackageDependencies="perl5 python35 py35-sqlite3 security/py-openssl net/py-urllib3"
appAutoUpdateString='cd /usr/local/share/'$appPackageName'\nprev=\$(git rev-parse HEAD)\ngit pull --rebase\ntest \"\$prev\" != \"\$(git rev-parse HEAD)\" && service '$appServiceName' restart'
appComment="Lazy Librarian\'s (eBook searcher) configuration, databases, and logs"
appConfigPath="/mnt/$zpool_name/apps/$appName/config.ini"

source $(dirname "$0")/createBaseMediaJail.sh

_log 8 "/" "Setting up the application jail ($jail_name) home" "STARTING" 1;
# Setup a Calibre instance to make the binaries available for post processing operations
iocage exec $jail_name sysrc calibre_enable="NO"
iocage exec $jail_name sysrc calibre_user="$media_user_name"
iocage exec $jail_name sysrc calibre_group="$media_group_name"
iocage exec $jail_name sysrc calibre_port="$calibre_jail_port"
iocage exec $jail_name sysrc calibre_home="$jail_config"
iocage exec $jail_name sysrc calibre_logfile="$jail_config/calibre-server.log"
iocage exec $jail_name sysrc calibre_library="$jail_media_folder_ebooks"
iocage exec $jail_name sysrc calibre_flags="--userdb $jail_config/users.sqlite --enable-auth"

iocage exec $jail_name sysrc $appServiceName\_enable="$lazylibrarian_enabled"
iocage exec $jail_name sysrc $appServiceName\_user="$media_user_name"
iocage exec $jail_name sysrc $appServiceName\_group="$media_group_name"
iocage exec $jail_name sysrc $appServiceName\_dir="/usr/local/$appPackageName"
iocage exec $jail_name sysrc $appServiceName\_datadir="$jail_config"
iocage exec $jail_name sysrc $appServiceName\_pid="$jail_config/$appServiceName.pid"
iocage exec $jail_name sysrc $appServiceName\_conf="$jail_config/config.ini"

iocage exec $jail_name pkg install -y deskutils/calibre
_checkPkgInstallResults "" "$appName" ""

iocage exec $jail_name ln -sf /usr/local/bin/python3.6 /usr/bin/python
#iocage exec $jail_name ln -sf /usr/local/bin/python2.7 /usr/bin/python
iocage exec $jail_name ln -sf /usr/local/bin/unrar /usr/bin/unrar
_log 8 "+" "Setting up the application jail ($jail_name) home" "COMPLETED" 1;

_log 8 "/" "Installing application ($appName) in jail ($jail_name)" "STARTING" 1;
iocage exec $jail_name git clone "https://gitlab.com/LazyLibrarian/LazyLibrarian.git" "/usr/local/$appPackageName"
iocage exec $jail_name chown -Rf $media_user_name:$media_group_name "/usr/local/$appPackageName"
iocage exec $jail_name cp "/usr/local/$appPackageName/init/freebsd.initd" "/usr/local/etc/rc.d/$appServiceName"
iocage exec $jail_name chmod u+x "/usr/local/etc/rc.d/$appServiceName"
cp "$(dirname "$0")/$appName/config.ini" "$appConfigPath" # LazyLibrarian doesn't generate a config.ini until you manually click Save Config from the gui

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_media_ebooks"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $download"

_restartService "$appServiceName" "$jail_name"
if ! _isConfigFileCreated "$appConfigPath"; then _fatalError "Application ($appName) failed to create configuration file" "$appConfigPath"; fi
_stopService "$appServiceName" "$jail_name"
_replaceConfig "$appConfigPath" "\[General\]"   "launch_browser = "   "= 1$"            "= 0";
_replaceConfig "$appConfigPath" "\[General\]"   "api_key = "          "= $"             "= $lazylibrarian_api_key";
_replaceConfig "$appConfigPath" "\[General\]"   "api_enabled = "      "= 0$"            "= 1";
_replaceConfig "$appConfigPath" "\[General\]"   "http_host = "        "= 127.0.0.1$"    "= 0.0.0.0";
_replaceConfig "$appConfigPath" "\[General\]"   "http_port = "        "= 5299$"         "= $applicationPort";
_replaceConfig "$appConfigPath" "\[General\]"   "download_dir = "     "= $"             "= $download_incomplete_process_ebooks";
_replaceConfig "$appConfigPath" "\[General\]"   "ebook_dir = "        "= $"             "= $jail_media_folder_ebooks";
_replaceConfig "$appConfigPath" "\[General\]"   "audio_dir = "        "= $"             "= $jail_media_folder_audiobooks";
_replaceConfig "$appConfigPath" "\[General\]"   "calibre_server = "   "= $"             "= $calibre_jail_ip";
_replaceConfig "$appConfigPath" "\[General\]"   "calibre_user = "     "= $"             "= $media_user_name";
_replaceConfig "$appConfigPath" "\[General\]"   "calibre_pass = "     "= $"             "= $media_user_password";
_replaceConfig "$appConfigPath" "\[General\]"   "imp_calibredb = "    "= $"             "= /usr/local/bin/calibredb";
_restartService "$appServiceName" "$jail_name"

_checkInstallResults "" "$appName" ""

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2;

_log 8 "+" "Installing application ($appName) in jail ($jail_name)" "COMPLETED" 1;

if [ $setup_apps_enabled == "YES" ]; then
  _launchScript "$(dirname "$0")/setup_app/setupLazylibrarian.sh" "Installer" "LAZYLIBARIAN" "SETUP APPLICATION" "$lazylibrarian_enabled"
fi

# Restore standard redirection
exec &>/dev/tty
