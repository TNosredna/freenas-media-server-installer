#!/bin/bash
log_enabled="$1"

appName="calibre"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-install.log"
log_err_file_path="$HOME/$appName-install.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="calibre"
appServiceName="calibre"
applicationPort="$calibre_jail_port"
appURI="$calibre_web_root"
applicationPackageDependencies="expect"
appAutoUpdateString='pkg update '$appPackageName' && service '$appServiceName' restart'
appComment="Calibre\'s (eBook manager) configuration, databases, and logs"
appConfigPath=""

source $(dirname "$0")/createBaseMediaJail.sh

_log 8 "/" "Setting up the application jail ($jail_name) home" "STARTING" 1
iocage exec $jail_name mkdir -p $jail_config/bin/
iocage exec $jail_name ln -sf /usr/local/bin/$appPackageName $jail_config/bin/$appPackageName
iocage exec $jail_name ln -sf /usr/local/bin/$appPackageName-complete $jail_config/bin/$appPackageName-complete
iocage exec $jail_name ln -sf /usr/local/bin/$appPackageName-customize $jail_config/bin/$appPackageName-customize
iocage exec $jail_name ln -sf /usr/local/bin/$appPackageName-debug $jail_config/bin/$appPackageName-debug
iocage exec $jail_name ln -sf /usr/local/bin/$appPackageName-parallel $jail_config/bin/$appPackageName-parallel
iocage exec $jail_name ln -sf /usr/local/bin/$appPackageName-server $jail_config/bin/$appPackageName-server
iocage exec $jail_name ln -sf /usr/local/bin/$appPackageName-smtp $jail_config/bin/$appPackageName-smtp
iocage exec $jail_name ln -sf /usr/local/bin/calibredb $jail_config/bin/calibredb
iocage exec $jail_name sysrc $appServiceName\_enable="$calibre_enabled"
iocage exec $jail_name sysrc $appServiceName\_user="$media_user_name"
iocage exec $jail_name sysrc $appServiceName\_group="$media_group_name"
iocage exec $jail_name sysrc $appServiceName\_port="$applicationPort"
iocage exec $jail_name sysrc $appServiceName\_home="$jail_config"
iocage exec $jail_name sysrc $appServiceName\_logfile="$jail_config/$appServiceName-server.log"
iocage exec $jail_name sysrc $appServiceName\_library="$jail_media_folder_ebooks"
iocage exec $jail_name sysrc $appServiceName\_flags="--userdb $jail_config/users.sqlite --enable-auth"
_log 8 "+" "Setting up the application jail ($jail_name) home" "COMPLETED" 1

_log 8 "/" "Installing application ($appName) in jail ($jail_name)" "STARTING" 1
iocage exec $jail_name pkg install -y deskutils/$appPackageName
_checkPkgInstallResults "" "$appName" ""

iocage exec $jail_name chmod 755 /usr/local/etc/rc.d/$appServiceName
cp $(dirname "$0")/$appName/metadata.db /mnt/$zpool_name/media/ebooks
cp $(dirname "$0")/$appName/metadata_db_prefs_backup.json /mnt/$zpool_name/media/ebooks
cp $(dirname "$0")/$appName/autoadd_calibre.sh /mnt/$zpool_name/apps/$appName/bin
if iocage exec $jail_name sqlite3 $jail_config/users.sqlite 'select * from users' | grep -q "$media_user_name|$media_group_name"; then
  _log 0 "" "Found existing $appName database, skipping user creation." "" 0
else
  cp $(dirname "$0")/$appName/addUser.sh /mnt/$zpool_name/apps/$appName/bin
  iocage exec $jail_name $jail_config/bin/addUser.sh "$jail_config" "$media_user_name" "$media_user_password"
  iocage exec $jail_name rm "$jail_config/bin/addUser.sh"
fi

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_media_folder_ebooks"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $download"

_restartService "$appServiceName" "$jail_name"

_checkInstallResults "" "$appName" ""

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2

_log 8 "+" "Installing application ($appName) in jail ($jail_name)" "COMPLETED" 1

if [ $setup_apps_enabled == "YES" ]; then
  _launchScript "$(dirname "$0")/setup_app/setupCalibre.sh" "Installer" "CALIBRE" "SETUP APPLICATION" "$calibre_enabled"
fi

# Restore standard redirection
exec &>/dev/tty
