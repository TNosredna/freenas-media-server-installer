#!/usr/local/bin/python

import argparse
import json
import sys

import requests


class Startup(object):

    def __init__(self, hostname, user, secret, media_username, media_userpassword, media_groupname, zpoolname, app_name, app_comment, execute, ip, dnsname):
        self._hostname = hostname
        self._user = user
        self._secret = secret
        self._media_user_name = media_username
        self._media_user_password = media_userpassword
        self._media_group_name = media_groupname
        self._zpoolname = zpoolname
        self._appname = app_name
        self._appcomment = app_comment
        self._execute = execute
        self._ip = ip
        self._dnsname = dnsname

        self._ep = 'http://%s/api/v1.0' % hostname

    def request(self, resource, method='GET', data=None):
        if data is None:
            data = ''
        r = requests.request(
            method,
            '%s/%s/' % (self._ep, resource),
            data=json.dumps(data),
            headers={'Content-Type': "application/json"},
            auth=(self._user, self._secret),
            )
        if r.ok:
            try:
                return r.json()
            except:
                return r.text

        raise ValueError(r)

    def create_system_tunable(self, tun_var, tun_comment, tun_value, tun_enabled, tun_type):
        try:
            print('--> Creating System Tunable: %s=%s [%s]' % (tun_var, tun_value, tun_type))
            self.request('system/tunable/', method='POST', data={
                'tun_var': tun_var,
                'tun_comment': tun_comment,
                'tun_value': tun_value,
                'tun_enabled': tun_enabled,
                'tun_type': tun_type,
            })
        except ValueError as requestErr:
            if requestErr.args[0].status_code == 409:
                print('ALREADY EXISTS --> System Tunable: %s=%s [%s]' % (tun_var, tun_value, tun_type))
            else:
                print('Error creating system tunable  %s=%s [%s]:' % (tun_var, tun_value, tun_type), requestErr)

    def create_dataset(self, parent_path_name, name, comment):
        try:
            print('--> Creating Dataset: %s/%s' % (parent_path_name, name))
            self.request('storage/dataset/%s' % parent_path_name, method='POST', data={
                'name': name,
                'comment': comment
            })
        except ValueError as requestErr:
            if requestErr.args[0].status_code == 400:
                print('ALREADY EXISTS --> Dataset: %s/%s' % (parent_path_name, name))
            else:
                print('Error creating dataset (PATH=%s)(NAME=%s)(COMMENT=%s):' % (parent_path_name, name, comment), requestErr)

    def create_nfs_share(self, username, groupname, paths, comment, is_alldirs, is_quiet):
        try:
            print('--> Creating NFS Share: %s' % paths)
            self.request('sharing/nfs', method='POST', data={
                'nfs_paths': paths,
                'nfs_comment': comment,
                'nfs_mapall_user': username,
                'nfs_mapall_group': groupname,
                'nfs_alldirs': is_alldirs,
                'nfs_quiet': is_quiet
            })
        except ValueError as requestErr:
            if requestErr.args[0].status_code == 409:
                print('ALREADY EXISTS --> NFS share: %s' % paths)
            else:
                print('Error creating NFS share (PATHS=%s)(COMMENT=%s):' % (paths, comment), requestErr)

    def create_user(self, bsdusr_username, bsdusr_password, bsdusr_full_name="", bsdusr_uid=None, bsdusr_gid=None, bsdusr_creategroup=True, bsdusr_mode="775", bsdusr_shell="/bin/csh", bsdusr_password_disabled=False, bsdusr_locked=False, bsdusr_sudo=True, bsdusr_sshpubkey=""):
        try:
            print('--> Creating User: %s' % bsdusr_username)
            self.request('sharing/nfs', method='POST', data={
                'bsdusr_username': bsdusr_username,
                'bsdusr_full_name': bsdusr_full_name,
                'bsdusr_password': bsdusr_password,
                'bsdusr_uid': bsdusr_uid,
                'bsdusr_group': bsdusr_gid,
                'bsdusr_creategroup': bsdusr_creategroup,
                'bsdusr_mode': bsdusr_mode,
                'bsdusr_shell': bsdusr_shell,
                'bsdusr_password_disabled': bsdusr_password_disabled,
                'bsdusr_locked': bsdusr_locked,
                'bsdusr_sudo': bsdusr_sudo,
                'bsdusr_sshpubkey': bsdusr_sshpubkey
            })
        except ValueError as requestErr:
            if requestErr.args[0].status_code == 409:
                print('ALREADY EXISTS --> User: %s' % bsdusr_username)
            else:
                print('Error creating user (%s):' % (bsdusr_username), requestErr)

    def update_hosts(self):
        try:
            current_config = self.request('network/globalconfiguration/')
            current_config["gc_hosts"]=current_config["gc_hosts"] + "\n" + self._ip + "\t" + self._dnsname
            if (self._ip + "\t" + self._dnsname) not in current_config["gc_hosts"]:
                print('--> Updating hosts to: %s' % (current_config['gc_hosts']))
                self.request('network/globalconfiguration/', method='PUT', data={
                    "gc_hosts": current_config['gc_hosts']
                })
        except ValueError as requestErr:
            print('Error Adding DNS entry to hosts (%s\t%s): ' % (self._ip, self._dnsname), requestErr)

    def service_start(self, name):
        self.request('services/services/%s' % name, method='PUT', data={
            'srv_enable': True,
        })

    def run(self):
        if self._appname:
            self.create_dataset(self._zpoolname + '/' + 'apps', self._appname, self._appcomment)
            # self.update_hosts("10.11.12.61", "deluge")
        elif self._execute:
            method_name = str(self._execute)
            method = getattr(self, method_name, lambda: "")
            return method()
        else:
            # self.create_user(self._media_user_name, self._media_user_password)
            self.create_dataset(self._zpoolname, 'apps', 'Application configurations')
            self.create_dataset(self._zpoolname, 'download', 'Media download staging area')
            self.create_dataset(self._zpoolname, 'media', 'Media library')
            self.create_dataset(self._zpoolname + '/' + 'media', 'audiobooks', 'Audiobook library')
            self.create_dataset(self._zpoolname + '/' + 'media', 'ebooks', 'eBook library')
            self.create_dataset(self._zpoolname + '/' + 'media', 'homevideos', 'Home video library')
            self.create_dataset(self._zpoolname + '/' + 'media', 'movies', 'Movie library')
            self.create_dataset(self._zpoolname + '/' + 'media', 'music', 'Music library')
            self.create_dataset(self._zpoolname + '/' + 'media', 'pictures', 'Picture library')
            self.create_dataset(self._zpoolname + '/' + 'media', 'tv', 'TV library')

            download='/mnt/%s/download' % self._zpoolname
            media_audiobooks='/mnt/%s/media/audiobooks' % self._zpoolname
            media_ebooks='/mnt/%s/media/ebooks' % self._zpoolname
            media_movies='/mnt/%s/media/movies' % self._zpoolname
            media_music='/mnt/%s/media/music' % self._zpoolname
            media_tv='/mnt/%s/media/tv' % self._zpoolname
            home_calibre='/mnt/%s/apps/calibre' % self._zpoolname
            home_couchpotato='/mnt/%s/apps/couchpotato' % self._zpoolname
            home_deluge='/mnt/%s/apps/deluge' % self._zpoolname
            home_headphones='/mnt/%s/apps/headphones' % self._zpoolname
            home_lazylibrarian='/mnt/%s/apps/lazylibrarian' % self._zpoolname
            home_madsonic='/mnt/%s/apps/madsonic' % self._zpoolname
            home_medusa='/mnt/%s/apps/medusa' % self._zpoolname
            home_nzbtomedia='/mnt/%s/apps/nzbtomedia' % self._zpoolname
            home_plex='/mnt/%s/apps/plex' % self._zpoolname
            home_sabnzbd='/mnt/%s/apps/sabnzbd' % self._zpoolname
            self.create_nfs_share(self._media_user_name, self._media_group_name, [download], 'Share to media download staging area. Proxyed as "%s:%s"' % (self._media_user_name, self._media_group_name), True, True)
            self.create_nfs_share(self._media_user_name, self._media_group_name, [media_audiobooks], 'Share to audiobook media collection. Proxyed as "%s:%s"' % (self._media_user_name, self._media_group_name), True, True)
            self.create_nfs_share(self._media_user_name, self._media_group_name, [media_ebooks], 'Share to eBook media collection. Proxyed as "%s:%s"' % (self._media_user_name, self._media_group_name), True, True)
            self.create_nfs_share(self._media_user_name, self._media_group_name, [media_movies], 'Share to movie media collection. Proxyed as "%s:%s"' % (self._media_user_name, self._media_group_name), True, True)
            self.create_nfs_share(self._media_user_name, self._media_group_name, [media_music], 'Share to music media collection. Proxyed as "%s:%s"' % (self._media_user_name, self._media_group_name), True, True)
            self.create_nfs_share(self._media_user_name, self._media_group_name, [media_tv], 'Share to TV media collection. Proxyed as "%s:%s"' % (self._media_user_name, self._media_group_name), True, True)

            self.create_nfs_share(self._media_user_name, self._media_group_name, [home_calibre], 'Share to the Calibre home folder. Proxyed as "%s:%s"' % (self._media_user_name, self._media_group_name), True, True)
            self.create_nfs_share(self._media_user_name, self._media_group_name, [home_couchpotato], 'Share to the Couchpotato home folder. Proxyed as "%s:%s"' % (self._media_user_name, self._media_group_name), True, True)
            self.create_nfs_share(self._media_user_name, self._media_group_name, [home_deluge], 'Share to the Deluge home folder. Proxyed as "%s:%s"' % (self._media_user_name, self._media_group_name), True, True)
            self.create_nfs_share(self._media_user_name, self._media_group_name, [home_headphones], 'Share to the Headphones home folder. Proxyed as "%s:%s"' % (self._media_user_name, self._media_group_name), True, True)
            self.create_nfs_share(self._media_user_name, self._media_group_name, [home_lazylibrarian], 'Share to the LazyLibrarian home folder. Proxyed as "%s:%s"' % (self._media_user_name, self._media_group_name), True, True)
            self.create_nfs_share(self._media_user_name, self._media_group_name, [home_madsonic], 'Share to the Madsonic home folder. Proxyed as "%s:%s"' % (self._media_user_name, self._media_group_name), True, True)
            self.create_nfs_share(self._media_user_name, self._media_group_name, [home_medusa], 'Share to the Medusa home folder. Proxyed as "%s:%s"' % (self._media_user_name, self._media_group_name), True, True)
            self.create_nfs_share(self._media_user_name, self._media_group_name, [home_nzbtomedia], 'Share to the nzbToMedia home folder. Proxyed as "%s:%s"' % (self._media_user_name, self._media_group_name), True, True)
            self.create_nfs_share(self._media_user_name, self._media_group_name, [home_plex], 'Share to the Plex home folder. Proxyed as "%s:%s"' % (self._media_user_name, self._media_group_name), True, True)
            self.create_nfs_share(self._media_user_name, self._media_group_name, [home_sabnzbd], 'Share to the Sabnzbd home folder. Proxyed as "%s:%s"' % (self._media_user_name, self._media_group_name), True, True)
            self.service_start('nfs')

            self.create_system_tunable("net.inet.ip.forwarding", "", 1, True, "sysctl")
            # self.create_system_tunable("net.link.bridge.pfil_onlyip", "", 0, True, "sysctl")
            # self.create_system_tunable("net.link.bridge.pfil_bridge", "", 0, True, "sysctl")
            # self.create_system_tunable("net.link.bridge.pfil_member", "", 0, True, "sysctl")
            # self.create_system_tunable("net.link.bridge.pfil_member", "", 0, True, "sysctl")
            # self.create_system_tunable("cloned_interfaces", "", "bridge0", True, "rc")
            # self.create_system_tunable("ifconfig_bridge0", "", "addm em0 up", True, "rc")
            # self.create_system_tunable("ifconfig_em0", "", "up", True, "rc")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-H', '--hostName', required=False, type=str)
    parser.add_argument('-u', '--user', required=False, type=str)
    parser.add_argument('-p', '--passwd', required=False, type=str)
    parser.add_argument('-m', '--media_user_name', required=False, type=str)
    parser.add_argument('-g', '--media_group_name', required=False, type=str)
    parser.add_argument('-n', '--media_user_password', required=False, type=str)
    parser.add_argument('-z', '--zpoolName', required=False, type=str)
    parser.add_argument('-a', '--appName', required=False, type=str)
    parser.add_argument('-c', '--appComment', required=False, type=str)
    parser.add_argument('-x', '--execute', required=False, type=str)
    parser.add_argument('-i', '--ip', required=False, type=str)
    parser.add_argument('-d', '--dnsname', required=False, type=str)

    args = parser.parse_args(sys.argv[1:])

    startup = Startup(args.hostName, args.user, args.passwd, args.media_user_name, args.media_user_password, args.media_group_name, args.zpoolName, args.appName, args.appComment, args.execute, args.ip, args.dnsname)
    startup.run()


if __name__ == '__main__':
    main()
