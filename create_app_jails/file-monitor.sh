#!/bin/bash

if [ -n "$1" ]; then appConfigPath="$1"; else return; fi
rm -f "$appConfigPath.created"

while :; do
  if [ -f "$appConfigPath" ];
    then
      now=$(date +"%T")
      echo "$now" > "$appConfigPath.created"
      break
  fi
done

#echo "Press any key to close window"
#read -t 1 -n 10000 discard
#read -sn 1
