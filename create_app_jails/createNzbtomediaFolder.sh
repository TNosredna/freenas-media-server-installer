#!/bin/bash
log_enabled="$1"

appName="nzbtomedia"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-install.log"
log_err_file_path="$HOME/$appName-install.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="nzbtomedia"
appServiceName="nzbtomedia"

_log 8 "/" "Installing application ($appName) in folder (/mnt/$zpool_name/apps/$appName/)" "STARTING" 1;
_log 0 "" "" "" 0;
if [ -d /mnt/$zpool_name/apps/$appName/.git ];
  then
    cd /mnt/$zpool_name/apps/$appName/ && git pull;
  else
  	# Need to use nightly because a bug was initroduced and hasn't made it to master branch yet
    git clone -b nightly https://github.com/clinton-hall/nzbToMedia.git "/mnt/$zpool_name/apps/$appName/."
#    git clone git://github.com/clinton-hall/nzbToMedia.git "/mnt/$zpool_name/apps/$appName/."
    cp "/mnt/$zpool_name/apps/$appName/autoProcessMedia.cfg.spec" "/mnt/$zpool_name/apps/$appName/autoProcessMedia.cfg"
fi

chown -R $media_user_name:$media_group_name "/mnt/$zpool_name/apps/$appName" || true
chmod -R 775 "/mnt/$zpool_name/apps/$appName" || true
chown -R $media_user_name:$media_group_name "/mnt/$zpool_name/download" || true
chmod -R 775 "//mnt/$zpool_name/download" || true
chown -R $media_user_name:$media_group_name "/mnt/$zpool_name/media" || true
chmod -R 775 "/mnt/$zpool_name/media" || true

_checkInstallResults "" "$appName" ""

_log 8 "+" "Installing application ($appName) in folder (/mnt/$zpool_name/apps/$appName/)" "COMPLETED" 1;

if [ $setup_apps_enabled == "YES" ]; then
  _launchScript "$(dirname "$0")/setup_app/setupNzbtomedia.sh" "Installer" "NZBTOMEDIA" "SETUP APPLICATION" "$nzbtomedia_enabled"
fi

# Restore standard redirection
exec &>/dev/tty
