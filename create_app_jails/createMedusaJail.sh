#!/bin/bash
log_enabled="$1"

appName="medusa"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-install.log"
log_err_file_path="$HOME/$appName-install.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="medusa"
appServiceName="medusa"
applicationPort="$medusa_jail_port"
appURI="$medusa_web_root"
applicationPackageDependencies="mediainfo python36 py36-sqlite3 devel/py-cheetah3 py36-cryptography py36-sabyenc3 py36-pymediainfo"
appAutoUpdateString='cd /usr/local/'$appPackageName'\nprev=\$(git rev-parse HEAD)\ngit pull --rebase\ntest \"\$prev\" != \"\$(git rev-parse HEAD)\" && service '$appServiceName' restart'
appComment="Medusa\'s (TV searcher) configuration, databases, and logs"
appConfigPath="/mnt/$zpool_name/apps/$appName/config.ini"

source $(dirname "$0")/createBaseMediaJail.sh

_log 8 "/" "Setting up the application jail ($jail_name) home" "STARTING" 1;
iocage exec $jail_name sysrc $appServiceName\_enable="$medusa_enabled"
iocage exec $jail_name sysrc $appServiceName\_user="$media_user_name"
iocage exec $jail_name sysrc $appServiceName\_group="$media_group_name"
iocage exec $jail_name sysrc $appServiceName\_dir="/usr/local/medusa"
iocage exec $jail_name sysrc $appServiceName\_datadir="$jail_config"
iocage exec $jail_name sysrc $appServiceName\_python_dir="/usr/local/bin/python3.6"

iocage exec $jail_name ln -sf /usr/local/bin/python3.6 /usr/local/bin/python
iocage exec $jail_name ln -sf /usr/local/bin/python3.6 /usr/local/bin/python3
iocage exec $jail_name ln -sf /usr/local/bin/python3.6 /usr/bin/python
iocage exec $jail_name ln -sf /usr/local/bin/python3.6 /usr/bin/python3
_log 8 "+" "Setting up the application jail ($jail_name) home" "COMPLETED" 1;

_log 8 "/" "Installing application ($appName) in jail ($jail_name)" "STARTING" 1;
iocage exec $jail_name git clone "https://github.com/pyMedusa/Medusa.git" "/usr/local/$appPackageName"
iocage exec $jail_name chown -Rf $media_user_name:$media_group_name "/usr/local/$appPackageName"
iocage exec $jail_name cp "/usr/local/$appPackageName/runscripts/init.freebsd" "/usr/local/etc/rc.d/$appServiceName"
iocage exec $jail_name chmod 744 "/usr/local/etc/rc.d/$appServiceName"

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_media_folder_tv"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $download"

_restartService "$appServiceName" "$jail_name"
if ! _isConfigFileCreated "$appConfigPath"; then _fatalError "Application ($appName) failed to create configuration file" "$appConfigPath"; fi
_stopService "$appServiceName" "$jail_name"
_replaceConfig "$appConfigPath" "\[General\]"      "api_key = "                 "= \"\"$"        "= $medusa_api_key";
_replaceConfig "$appConfigPath" "\[General\]"      "root_dirs = "               "= ,$"           "= 0, $jail_media_folder_tv";
_replaceConfig "$appConfigPath" "\[General\]"      "web_log = "                 "= 0$"           "= 1";
_replaceConfig "$appConfigPath" "\[General\]"      "web_host = "                "= 127.0.0.1$"   "= 0.0.0.0";
_replaceConfig "$appConfigPath" "\[General\]"      "web_host = "                "= localhost$"   "= 0.0.0.0";
_replaceConfig "$appConfigPath" "\[General\]"      "web_port = "                "= 8081$"        "= $applicationPort";
_replaceConfig "$appConfigPath" "\[General\]"      "ssl_verify = "              "= 1$"           "= 0";
_replaceConfig "$appConfigPath" "\[General\]"      "debug = "                   "= 0$"           "= 1";
_replaceConfig "$appConfigPath" "\[General\]"      "launch_browser = "          "= 1$"           "= 0";
_replaceConfig "$appConfigPath" "\[SABnzbd\]"      "sab_apikey = "              "= \"\"$"        "= $sabnzbd_api_key";
_replaceConfig "$appConfigPath" "\[SABnzbd\]"      "sab_host = "                "= \"\"$"        "= $sabnzbd_jail_ip:$sabnzbd_jail_port";
_replaceConfig "$appConfigPath" "\[TORRENT\]"      "torrent_host = "            "= \"\"$"        "= scgi://$deluge_jail_ip:$deluge_jail_port";
_replaceConfig "$appConfigPath" "\[TORRENT\]"      "torrent_label = "           "= \"\"$"        "= tv";
_replaceConfig "$appConfigPath" "\[TORRENT\]"      "torrent_username = "        "= \"\"$"        "= $media_user_name";
_replaceConfig "$appConfigPath" "\[TORRENT\]"      "torrent_password = "        "= \"\"$"        "= $media_user_password";
_replaceConfig "$appConfigPath" "\[TORRENT\]"      "torrent_path = "            "= \"\"$"        "= $download_incomplete_torrent";
_replaceConfig "$appConfigPath" "\[TORRENT\]"      "torrent_seed_location = "   "= \"\"$"        "= $download";
_replaceConfig "$appConfigPath" "\[TORRENT\]"      "torrent_label_anime = "     "= \"\"$"        "= anime";
_restartService "$appServiceName" "$jail_name"

_checkInstallResults "" "$appName" ""

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2;

_log 8 "+" "Installing application ($appName) in jail ($jail_name)" "COMPLETED" 1;

if [ $setup_apps_enabled == "YES" ]; then
  _launchScript "$(dirname "$0")/setup_app/setupMedusa.sh" "Installer" "MEDUSA" "SETUP APPLICATION" "$medusa_enabled"
fi

# Restore standard redirection
exec &>/dev/tty
