#!/bin/bash
log_enabled="$1"

appName="couchpotato"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-install.log"
log_err_file_path="$HOME/$appName-install.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="couchpotato"
appServiceName="couchpotato"
applicationPort="$couchpotato_jail_port"
appURI="$couchpotato_web_root"
applicationPackageDependencies="python27 py27-sqlite3 py27-openssl py27-lxml fpc-libcurl docbook-xml"
appAutoUpdateString='cd /usr/local/'$appPackageName'\nprev=\$(git rev-parse HEAD)\ngit pull --rebase\ntest \"\$prev\" != \"\$(git rev-parse HEAD)\" && service '$appServiceName' restart'
appComment="Couchpotato\'s (movie searcher) configuration, databases, and logs"
appConfigPath="/mnt/$zpool_name/apps/$appName/settings.conf"

source $(dirname "$0")/createBaseMediaJail.sh

_log 8 "/" "Setting up the application jail ($jail_name) home" "STARTING" 1;
iocage exec $jail_name sysrc $appServiceName\_enable="$couchpotato_enabled"
iocage exec $jail_name sysrc $appServiceName\_user="$media_user_name"
iocage exec $jail_name sysrc $appServiceName\_group="$media_group_name"
iocage exec $jail_name sysrc $appServiceName\_dir="/usr/local/$appPackageName"
iocage exec $jail_name sysrc $appServiceName\_datadir="$jail_config"
iocage exec $jail_name sysrc $appServiceName\_pid="$jail_config/$appServiceName.pid"
iocage exec $jail_name sysrc $appServiceName\_conf="$jail_config/settings.conf"

iocage exec $jail_name ln -sf /usr/local/bin/python2.7 /usr/bin/python
iocage exec $jail_name ln -sf /usr/local/bin/python2.7 /usr/bin/python2
_log 8 "+" "Setting up the application jail ($jail_name) home" "COMPLETED" 1;

_log 8 "/" "Installing application ($appName) in jail ($jail_name)" "STARTING" 1;
iocage exec $jail_name git clone "https://github.com/CouchPotato/CouchPotatoServer.git" "/usr/local/$appPackageName"
iocage exec $jail_name chown -Rf $media_user_name:$media_group_name "/usr/local/$appPackageName"
iocage exec $jail_name cp "/usr/local/$appPackageName/init/freebsd" "/usr/local/etc/rc.d/$appServiceName"
iocage exec $jail_name sed -i '' -e 's?command_interpreter="/usr/local/bin/python"?command_interpreter="python"?g' /usr/local/etc/rc.d/$appServiceName
iocage exec $jail_name sed -i '' -e 's?ssl\.PROTOCOL_TLSv3?ssl\.PROTOCOL_TLSv1?g' /usr/local/couchpotato/libs/synchronousdeluge/transfer.py
iocage exec $jail_name chmod 755 "/usr/local/etc/rc.d/$appServiceName"

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_media_folder_movies"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $download"

_restartService "$appServiceName" "$jail_name"
if ! _isConfigFileCreated "$appConfigPath"; then _fatalError "Application ($appName) failed to create configuration file" "$appConfigPath"; fi
_stopService "$appServiceName" "$jail_name"
_replaceConfig "$appConfigPath" "\[core\]"      "data_dir = "                 "= $"        "= $jail_config";
_replaceConfig "$appConfigPath" "\[core\]"      "debug = "                    "= 0$"       "= 1";
_replaceConfig "$appConfigPath" "\[core\]"      "launch_browser = "           "= True$"    "= 0";
_replaceConfig "$appConfigPath" "\[core\]"      "port = "                     "= 5050$"    "= $applicationPort";
_replaceConfig "$appConfigPath" "\[core\]"      "api_key_internal_meta = "    "= ro$"      "= rw";
_restartService "$appServiceName" "$jail_name"

_checkInstallResults "" "$appName" ""

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2;

_log 8 "+" "Installing application ($appName) in jail ($jail_name)" "COMPLETED" 1;

if [ $setup_apps_enabled == "YES" ]; then
  _launchScript "$(dirname "$0")/setup_app/setupCouchpotato.sh" "Installer" "COUCHPOTATO" "SETUP APPLICATION" "$couchpotato_enabled"
fi

# Restore standard redirection
exec &>/dev/tty
