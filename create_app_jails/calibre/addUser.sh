#!/usr/local/bin/expect -f

set jail_config [lindex $argv 0];
set media_user_name [lindex $argv 1];
set media_user_password [lindex $argv 2];

set timeout -1

spawn calibre-server --userdb $jail_config/users.sqlite --manage-users
expect "What do you want to do?" {send -- "1\r"}
expect "Enter the username" {send -- "$media_user_name\r"}
expect "Enter the new password for $media_user_name" {send -- "$media_user_password\r"}
expect "Re-enter the new password for $media_user_name, to verify" {send -- "$media_user_password\r"}
expect eof
