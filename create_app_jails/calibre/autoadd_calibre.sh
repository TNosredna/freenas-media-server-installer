#!/bin/bash

if [ "$(ls -A $download_incomplete_manual)" ]; then
     calibredb add -r "$download_incomplete_manual" --library-path="$jail_media_folder_ebooks"
     rm $download_incomplete_manual/*
fi
