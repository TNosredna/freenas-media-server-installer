#!/bin/bash

appName="$appName"
appPackageName="$appPackageName"
appServiceName="$appServiceName"
appAutoUpdateString="$appAutoUpdateString"
commonPackageDependencies="sudo unrar unzip p7zip wget ffmpeg git-lite openvpn ca_root_nss nano archivers/par2cmdline-tbb gsed gawk"
applicationPackageDependencies=$applicationPackageDependencies
appComment="$appComment"

jailName_var=$appName\_jail_name
jail_name=${!jailName_var}
jail_ip_var=$appName\_jail_ip
jail_ip=${!jail_ip_var}
jail_mac_var=$appName\_jail_mac
jail_mac=${!jail_mac_var}
openvpn_enabled_var=$appName\_openvpn_enabled
openvpn_enabled=${!openvpn_enabled_var}
ipfw_enabled_var=$appName\_ipfw_enabled
ipfw_enabled=${!ipfw_enabled_var}

_jailExists() {
  iocage list | grep $jail_name;
}
jailExists="$(_jailExists)";
if [ ${#jailExists} -gt 0 ]; then iocage destroy --recursive $jail_name; fi

_log 16 "/" "CREATING JAIL --> \"$jail_name\"" "STARTING" 1;
# Create jail
iocage create -n $jail_name -r "$jail_freebsd_version" host_hostname="$appName" vnet="on" ip4_addr="none" ip6_addr="none" defaultrouter="none" defaultrouter6="none" vnet0_mac="none" bpf="yes" dhcp="on" allow_raw_sockets="1" boot="on" allow_tun="1"
# Set NIC's data
iocage set defaultrouter="$router_ip" $jail_name
iocage set vnet0_mac="$jail_mac" $jail_name
iocage set ip4_addr="vnet0|$jail_ip/24" $jail_name
# Enable SSH access
iocage exec $jail_name "sysrc sshd_enable=\"YES\""
# Add a password to '$root_user_name' user, to allow SSH login
iocage exec $jail_name "echo \"$root_user_password\" | pw usermod \"$root_user_name\" -h 0"
# Allow $root_user_name to ssh
iocage exec $jail_name sed -i '' -e 's?#PermitRootLogin no?PermitRootLogin yes?g' /etc/ssh/sshd_config
iocage exec $jail_name sed -i '' -e 's?#StrictModes yes?StrictModes no?g' /etc/ssh/sshd_config
iocage exec $jail_name sed -i '' -e 's?#VerifyHostKeyDNS yes?VerifyHostKeyDNS no?g' /etc/ssh/ssh_config
iocage exec $jail_name "mkdir -p /usr/local/etc/pkg/repos"
# Enable Sudo
iocage exec $jail_name "echo user ALL=\(ALL\) ALL >> /usr/local/etc/sudoers"
iocage exec $jail_name "echo setenv LC_ALL en_US.UTF-8 >> /$root_user_name/.cshrc"
iocage exec $jail_name "echo setenv LANG en_US.UTF-8 >> /$root_user_name/.cshrc"
iocage exec $jail_name "echo setenv MM_CHARSET UTF-8 >> /$root_user_name/.cshrc"
iocage exec $jail_name "echo setenv LC_ALL en_US.UTF-8 >> /etc/profile"
iocage exec $jail_name "echo setenv LANG en_US.UTF-8 >> /etc/profile"
iocage exec $jail_name "echo setenv MM_CHARSET UTF-8 >> /etc/profile"
## Restart jail to enable SSH
_log 0 "" "Restarting Jail" "$jail_name" 0;
iocage restart $jail_name

_log 8 "/" "Installing Packages" "STARTING" 1;
# Update pkg environment variables
iocage exec $jail_name "echo FETCH_TIMEOUT = 200 >> /usr/local/etc/pkg.conf"
iocage exec $jail_name "echo FETCH_RETRY = 3 >> /usr/local/etc/pkg.conf"
iocage exec $jail_name "echo DEBUG_SCRIPTS = false >> /usr/local/etc/pkg.conf"
iocage exec $jail_name "echo DEBUG_LEVEL = 0 >> /usr/local/etc/pkg.conf"
iocage exec $jail_name "echo ASSUME_ALWAYS_YES = true >> /usr/local/etc/pkg.conf"
iocage exec $jail_name "echo DEFAULT_ALWAYS_YES = true >> /usr/local/etc/pkg.conf"
iocage exec $jail_name "echo AUTOCLEAN = false >> /usr/local/etc/pkg.conf"
iocage exec $jail_name "echo IGNORE_OSVERSION = YES >> /usr/local/etc/pkg.conf"
iocage exec $jail_name pkg install -y $commonPackageDependencies
iocage exec $jail_name pkg install -y $applicationPackageDependencies
_log 8 "+" "Installing Packages" "COMPLETED" 2;
_log 8 "/" "Updating Packages" "STARTING" 1;
# Increase Jail's updates to latest versions
#iocage exec $jail_name 'echo FreeBSD: { url: \"pkg+http://pkg.FreeBSD.org/\${ABI}/latest\" } > /usr/local/etc/pkg/repos/FreeBSD.conf'
iocage exec $jail_name pkg update
#iocage exec $jail_name pkg upgrade
_checkPkgInstallResults "" "$appName" ""
_log 8 "+" "Updating Packages" "COMPLETED" 2;

_log 8 "/" "SETTING UP BASE MEDIA STRUCTURE" "STARTING" 3
# Create 'media' group & user
iocage exec $jail_name "pw groupadd \"$media_group_name\" -g 8675309"
iocage exec $jail_name "pw useradd -n \"$media_user_name\" -u 8675309 -g 8675309 -m -s /bin/csh"
iocage exec $jail_name "echo \"$media_user_password\" | pw usermod \"$media_user_name\" -h 0"
iocage exec $jail_name "pw groupmod video -m \"$media_user_name\""
iocage exec $jail_name "pw groupmod operator -m \"$media_user_name\""
iocage exec $jail_name "pw groupmod \"$media_group_name\" -m \"$root_user_name\""

$(dirname "$0")/setupDatasetsAndNfsShares.py -H $freenas_ip -u $root_user_name -p $root_user_password -m $media_user_name -g $media_group_name -z $zpool_name -a "$appName" -c "$appComment"
$(dirname "$0")/setupDatasetsAndNfsShares.py -x "update_hosts" -H "$freenas_ip" -u "$root_user_name" -p "$root_user_password" -d "$jail_name" -i "$jail_ip"

iocage exec $jail_name mkdir -p $jail_config
iocage exec $jail_name mkdir -p $jail_nzbtomedia
iocage exec $jail_name mkdir -p $download
iocage exec $jail_name mkdir -p $jail_media_folder_audiobooks
iocage exec $jail_name mkdir -p $jail_media_folder_ebooks
iocage exec $jail_name mkdir -p $jail_media_folder_audiobooks
iocage exec $jail_name mkdir -p $jail_media_folder_home_videos
iocage exec $jail_name mkdir -p $jail_media_folder_movies
iocage exec $jail_name mkdir -p $jail_media_folder_music
iocage exec $jail_name mkdir -p $jail_media_folder_pictures
iocage exec $jail_name mkdir -p $jail_media_folder_tv

# Mount jail's share folders
iocage fstab -a $jail_name /mnt/$zpool_name/apps/$appName $jail_config nullfs rw 0 0
iocage fstab -a $jail_name /mnt/$zpool_name/apps/nzbtomedia $jail_nzbtomedia nullfs rw 0 0
iocage fstab -a $jail_name /mnt/$zpool_name/download $download nullfs rw 0 0
iocage fstab -a $jail_name /mnt/$zpool_name/media/ebooks $jail_media_folder_ebooks nullfs rw 0 0
iocage fstab -a $jail_name /mnt/$zpool_name/media/ebooks $jail_media_folder_audiobooks nullfs rw 0 0
iocage fstab -a $jail_name /mnt/$zpool_name/media/tv $jail_media_folder_tv nullfs rw 0 0
iocage fstab -a $jail_name /mnt/$zpool_name/media/movies $jail_media_folder_movies nullfs rw 0 0
iocage fstab -a $jail_name /mnt/$zpool_name/media/music $jail_media_folder_music nullfs rw 0 0
iocage fstab -a $jail_name /mnt/$zpool_name/media/pictures $jail_media_folder_pictures nullfs rw 0 0
iocage fstab -a $jail_name /mnt/$zpool_name/media/homevideos $jail_media_folder_home_videos nullfs rw 0 0
iocage fstab -a $jail_name /mnt/$zpool_name/media/audiobooks $jail_media_folder_audiobooks nullfs rw 0 0

# Create auto-update script
iocage exec $jail_name "echo -e \"$appAutoUpdateString\" >> /$root_user_name/update-$appPackageName.sh"
iocage exec $jail_name "chmod u+x /$root_user_name/update-$appPackageName.sh"
# Add auto-update to daily crontab
iocage exec $jail_name "( crontab -l ; echo \"0 */24 * * * /$root_user_name/update-$appPackageName.sh\" ) | crontab -"
_log 8 "+" "SETTING UP BASE MEDIA STRUCTURE" "COMPLETED" 3

# Configure OpenVPN
_log 8 "/" "Configuring OpenVPN" "STARTING" 1;
mkdir -p /mnt/$zpool_name/apps/$appName/openvpn
cp $(dirname "$0")/openvpn/openvpn.ovpn /mnt/$zpool_name/apps/$appName/openvpn/openvpn.ovpn
cp $(dirname "$0")/openvpn/syslog.conf /mnt/$zpool_name/apps/$appName/openvpn/syslog.conf
iocage exec $jail_name cp -f $jail_config/openvpn/syslog.conf /etc/syslog.conf
iocage exec $jail_name "echo \"$jail_config/openvpn/openvpn.log         600     30  *   @T00    ZC\" >> /etc/newsyslog.conf"
iocage exec $jail_name "echo $openvpn_user_name >> $jail_config/openvpn/credentials"
iocage exec $jail_name "echo $openvpn_user_password >> $jail_config/openvpn/credentials"
iocage exec $jail_name "sysrc openvpn_dir=\"$jail_config/openvpn\""
iocage exec $jail_name "sysrc openvpn_configfile=\"$jail_config/openvpn/openvpn.ovpn\""
iocage exec $jail_name "sysrc openvpn_enable=\"$openvpn_enabled\""
iocage exec $jail_name "sysrc openvpn_if=\"tun\""
if [ "$openvpn_enabled" == "YES" ]; then
  _restartService "openvpn" "$jail_name"
fi
_log 8 "+" "Configuring OpenVPN" "COMPLETED" 1;

# Configure ipfw
_log 8 "/" "Configuring IPFW" "STARTING" 1;
sed -i.tmp -e 's?{{{subnet}}}?'$subnet'?g' $(dirname "$0")/$appName/ipfw.rules
cp $(dirname "$0")/$appName/ipfw.rules /mnt/$zpool_name/apps/$appName/ipfw.rules
mv -f $(dirname "$0")/$appName/ipfw.rules.tmp $(dirname "$0")/$appName/ipfw.rules
iocage exec $jail_name "sysrc firewall_script=\"$jail_config/ipfw.rules\""
iocage exec $jail_name "sysrc firewall_enable=\"$ipfw_enabled\""
if [ "$ipfw_enabled" == "YES" ]; then
  serviceRunning="$(_serviceRunning ipfw $jail_name)"
  if [ ${#serviceRunning} -gt 0 ]; then
      iocage exec $jail_name "sh $jail_config/ipfw.rules"
    else
      iocage exec $jail_name "service ipfw start"
  fi
fi
_log 8 "+" "Configuring IPFW" "COMPLETED" 1;

# Restart jail for fresh state of services
_log 0 "" "Restarting Jail" "$jail_name" 0;
iocage restart $jail_name

# Set jail's share folders permissions
chown -R $media_user_name:$media_group_name /mnt/$zpool_name/apps || true
chmod -R 775 /mnt/$zpool_name/apps || true
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_nzbtomedia"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $download"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_media_folder"
_log 16 "+" "CREATING JAIL --> \"$jail_name\"" "COMPLETED" 2;

# Create server certificate
iocage exec $jail_name "openssl req -nodes -newkey rsa:1024 -keyout \"$jail_config\"/$jail_name.key.pem -out \"$jail_config\"/$jail_name.cert.pem -subj \"/C=US/ST=Utah/L=Utah/O=FreeNAS Media Appliance/OU=$jail_name/CN=$notify_gmail_address\""