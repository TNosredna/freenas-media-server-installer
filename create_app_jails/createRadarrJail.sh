#!/bin/bash
log_enabled="$1"

appName="radarr"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-install.log"
log_err_file_path="$HOME/$appName-install.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="Radarr"
appServiceName="radarr"
applicationPort="$radarr_jail_port"
appURI="$radarr_web_root"
applicationPackageDependencies="mono mediainfo sqlite3"
appAutoUpdateString=""
appComment="Radarr\'s (movie searcher) configuration, databases, and logs"
appConfigPath="/mnt/$zpool_name/apps/$appName/nzbdrone.db"
appWebConfigPath="/mnt/$zpool_name/apps/$appName/config.xml"

pkg lock -y sqlite3
source $(dirname "$0")/createBaseMediaJail.sh

_log 8 "/" "Setting up the application jail ($jail_name) home" "STARTING" 1;
iocage exec $jail_name sysrc $appServiceName\_enable="$radarr_enabled"
iocage exec $jail_name sysrc $appServiceName\_user="$media_user_name"
iocage exec $jail_name sysrc $appServiceName\_group="$media_group_name"
iocage exec $jail_name sysrc $appServiceName\_data_dir="$jail_config"

#needed this to stop error:Could not find libgdiplus. Cannot test if image is corrupt.: Couldn't load GDIPlus library
iocage exec $jail_name pkg install -y libgdiplus
#this is needed for updates within Radarr
iocage exec $jail_name "ln -s /usr/local/bin/mono /bin"
_log 8 "+" "Setting up the application jail ($jail_name) home" "COMPLETED" 1;

_log 8 "/" "Installing application ($appName) in jail ($jail_name)" "STARTING" 1;
iocage exec $jail_name "wget https://github.com/Radarr/Radarr/releases/download/v0.2.0.1450/Radarr.develop.0.2.0.1450.linux.tar.gz -P /usr/local/ && mv /usr/local/*.tar.gz /usr/local/$appPackageName.tar.gz"
iocage exec $jail_name "cd /usr/local/ && tar -zxvf $appPackageName.tar.gz && rm $appPackageName.tar.gz"
iocage exec $jail_name chown -Rf $media_user_name:$media_group_name "/usr/local/$appPackageName"
cp $(dirname "$0")/$appName/$appServiceName /mnt/$zpool_name/apps/$appName/$appServiceName
iocage exec $jail_name mv "/mnt/config/$appServiceName" "/usr/local/etc/rc.d/$appServiceName"

iocage exec $jail_name "chmod 555 /usr/local/etc/rc.d/$appServiceName"
iocage exec $jail_name "chmod -R 777 $jail_config"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_media_folder_movies"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $download"

_restartService "$appServiceName" "$jail_name"
_stopService "$appServiceName" "$jail_name"
#_replaceSqlProperty "...table..." "...key..." "...value...";
_restartService "$appServiceName" "$jail_name"

_checkInstallResults "" "$appName" ""

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2;

_log 8 "+" "Installing application ($appName) in jail ($jail_name)" "COMPLETED" 1;

if [ $setup_apps_enabled == "YES" ]; then
  _launchScript "$(dirname "$0")/setup_app/setupRadarr.sh" "Installer" "RADARR" "SETUP APPLICATION" "$radarr_enabled"
fi

# Restore standard redirection
exec &>/dev/tty
