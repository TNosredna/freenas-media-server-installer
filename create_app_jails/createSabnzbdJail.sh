#!/bin/bash
log_enabled="$1"

appName="sabnzbd"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-install.log"
log_err_file_path="$HOME/$appName-install.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="sabnzbdplus"
appServiceName="sabnzbd"
applicationPort="$sabnzbd_jail_port"
appURI="$sabnzbd_web_root"
applicationPackageDependencies="python databases/py-sqlite3 security/py-openssl net/py-urllib3"
appAutoUpdateString='pkg update && pkg install -f '$appPackageName' && service '$appServiceName' restart'
appComment="SabNZBD\'s (NZB downloader) configuration, databases, and logs"
appConfigPath="/mnt/$zpool_name/apps/$appName/sabnzbd.ini"

source $(dirname "$0")/createBaseMediaJail.sh

_log 8 "/" "Setting up the application jail ($jail_name) home" "STARTING" 1;
iocage exec $jail_name sysrc $appServiceName\_enable="$sabnzbd_enabled"
iocage exec $jail_name sysrc $appServiceName\_conf_dir="$jail_config"
iocage exec $jail_name sysrc $appServiceName\_user="$media_user_name"
iocage exec $jail_name sysrc $appServiceName\_group="$media_group_name"
_log 8 "+" "Setting up the application jail ($jail_name) home" "COMPLETED" 1;

_log 8 "/" "Installing application ($appName) in jail ($jail_name)" "STARTING" 1;
iocage exec $jail_name pkg install -y $appPackageName
_checkPkgInstallResults "" "$appName" ""

cp $(dirname "$0")/$appName/$appServiceName /mnt/$zpool_name/apps/$appName/$appServiceName

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_media_folder"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $download"

_restartService "$appServiceName" "$jail_name"
if ! _isConfigFileCreated "$appConfigPath"; then _fatalError "Application ($appName) failed to create configuration file" "$appConfigPath"; fi
_stopService "$appServiceName" "$jail_name"
_replaceConfig "$appConfigPath" "\[misc\]"      "^host ="           "= 127.0.0.1$"              "= 0.0.0.0";
_replaceConfig "$appConfigPath" "\[misc\]"      "^port ="           "= 8080$"                   "= $applicationPort";
_replaceConfig "$appConfigPath" "\[misc\]"      "^local_ranges ="   "= ,$"                      "= ${subnet%.*}.,";
_replaceConfig "$appConfigPath" "\[misc\]"      "^download_dir ="   "= Downloads/incomplete$"   "= $download_incomplete_nzb";
_replaceConfig "$appConfigPath" "\[misc\]"      "^complete_dir ="   "= Downloads/complete$"     "= $download";
_replaceConfig "$appConfigPath" "\[misc\]"      "^script_dir ="     "= \"\"$"                   "= $jail_nzbtomedia";
_replaceConfig "$appConfigPath" "\[misc\]"      "^api_key ="        "= \"\"$"                   "= $sabnzbd_api_key";
_restartService "$appServiceName" "$jail_name"

_checkInstallResults "" "$appName" ""

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2;

_log 8 "+" "Installing application ($appName) in jail ($jail_name)" "COMPLETED" 1;

if [ $setup_apps_enabled == "YES" ]; then
  _launchScript "$(dirname "$0")/setup_app/setupSabnzbd.sh" "Installer" "SABNZBD" "SETUP APPLICATION" "$sabnzbd_enabled"
fi

# Restore standard redirection
exec &>/dev/tty
