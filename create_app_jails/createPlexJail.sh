#!/bin/bash
log_enabled="$1"

appName="plex"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-install.log"
log_err_file_path="$HOME/$appName-install.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="plexmediaserver-plexpass"
appServiceName="plexmediaserver_plexpass"
applicationPort="$plex_jail_port"
appURI="$plex_web_root"
applicationPackageDependencies=""
appAutoUpdateString='pkg update && pkg upgrade '$appPackageName' && service '$appServiceName' restart'
appComment="Plex Media Server\'s (Movie manager) configuration, databases, and logs"
appConfigPath="/mnt/$zpool_name/apps/$appName/Plex Media Server/Preferences.xml"
iocage exec $jail_name 'echo FreeBSD: { url: \"pkg+http://pkg.FreeBSD.org/\${ABI}/latest\" } > /usr/local/etc/pkg/repos/FreeBSD.conf'

source $(dirname "$0")/createBaseMediaJail.sh

_log 8 "/" "Setting up the application jail ($jail_name) home" "STARTING" 1;
iocage exec $jail_name sysrc $appServiceName\_enable="$plex_enabled"
iocage exec $jail_name sysrc $appServiceName\_user="$media_user_name"
iocage exec $jail_name sysrc $appServiceName\_group="$media_group_name"
iocage exec $jail_name sysrc $appServiceName\_support_path="$jail_config"
_log 8 "+" "Setting up the application jail ($jail_name) home" "COMPLETED" 1;

_log 8 "/" "Installing application ($appName) in jail ($jail_name)" "STARTING" 1;
mkdir -p /mnt/$zpool_name/apps/$appName/Plex\ Media\ Server/
iocage exec $jail_name pkg install -y $appPackageName
_checkPkgInstallResults "" "$appName" ""

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_media_folder"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $download"

_restartService "$appServiceName" "$jail_name"
if ! _isConfigFileCreated "$appConfigPath"; then _fatalError "Application ($appName) failed to create configuration file" "$appConfigPath"; fi

_checkInstallResults "" "$appName" ""

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2;

_log 8 "+" "Installing application ($appName) in jail ($jail_name)" "COMPLETED" 1;

if [ $setup_apps_enabled == "YES" ]; then
  _launchScript "$(dirname "$0")/setup_app/setupPlex.sh" "Installer" "PLEX" "SETUP APPLICATION" "$plex_enabled"
fi

# Restore standard redirection
exec &>/dev/tty
