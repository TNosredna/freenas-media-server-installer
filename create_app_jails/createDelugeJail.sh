#!/bin/bash
log_enabled="$1"

appName="deluge"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-install.log"
log_err_file_path="$HOME/$appName-install.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="deluge"
appServiceName="deluged"
appWebServiceName="deluge_web"
applicationPort="$deluge_jail_port"
appURI="$deluge_web_root"
applicationPackageDependencies="databases/py-sqlite3 security/py-openssl net/py-urllib3"
#appAutoUpdateString='cd /usr/src/contrib/'$appPackageName';\nprev=\$(git rev-parse HEAD)\ngit pull --rebase\ntest \"\$prev\" != \"\$(git rev-parse HEAD)\" && service '$appServiceName' restart && service '$appWebServiceName' restart'
appAutoUpdateString=""
appComment="Deluge\'s (torrent downloader) configuration, databases, and logs"
appConfigPath_auth="/mnt/$zpool_name/apps/$appName/auth"
appConfigPath_web_auth="/mnt/$zpool_name/apps/$appName/auth"
appConfigPath_core="/mnt/$zpool_name/apps/$appName/core.conf"
appConfigPath_web="/mnt/$zpool_name/apps/$appName/web.conf"
appConfigPath_hostlist="/mnt/$zpool_name/apps/$appName/hostlist.conf.1.2"

source $(dirname "$0")/createBaseMediaJail.sh

_log 8 "/" "Setting up the application jail ($jail_name) home" "STARTING" 1;
iocage exec $jail_name mkdir -p "$jail_config"/plugins
#cp $(dirname "$0")/$appName/web.conf /mnt/$zpool_name/apps/$appName/
iocage exec $jail_name sysrc $appServiceName\_enable="$deluge_enabled"
iocage exec $jail_name sysrc $appServiceName\_user="$media_user_name"
iocage exec $jail_name sysrc $appServiceName\_group="$media_group_name"
iocage exec $jail_name sysrc $appServiceName\_confdir="$jail_config"
iocage exec $jail_name sysrc $appServiceName\_logfile="$jail_config/$appServiceName.log"
iocage exec $jail_name sysrc $appServiceName\_loglevel="debug"
iocage exec $jail_name sysrc $appWebServiceName\_enable="$deluge_enabled"
iocage exec $jail_name sysrc $appWebServiceName\_user="$media_user_name"
iocage exec $jail_name sysrc $appWebServiceName\_group="$media_group_name"
iocage exec $jail_name sysrc $appWebServiceName\_confdir="$jail_config"
iocage exec $jail_name sysrc $appWebServiceName\_logfile="$jail_config/$appWebServiceName.log"
iocage exec $jail_name sysrc $appWebServiceName\_loglevel="debug"

iocage exec $jail_name ln -sf /usr/local/bin/python2.7 /usr/local/bin/python
iocage exec $jail_name ln -sf /usr/local/bin/python2.7 /usr/bin/python
iocage exec $jail_name ln -sf /usr/local/bin/python2.7 /usr/bin/python2
_log 8 "+" "Setting up the application jail ($jail_name) home" "COMPLETED" 1;

_log 8 "/" "Installing application ($appName) in jail ($jail_name)" "STARTING" 1;
#iocage exec $jail_name git clone "git://deluge-torrent.org/deluge.git" "/usr/src/contrib/$appPackageName"

#iocage exec $jail_name mkdir -p "/usr/local/$appPackageName"
#iocage exec $jail_name "export PYTHONPATH=/usr/local/$appPackageName; cd /usr/src/contrib/$appPackageName; python /usr/src/contrib/$appPackageName/setup.py develop --install-dir /usr/local/$appPackageName;"
#iocage exec $jail_name "export PYTHONPATH=/usr/local/$appPackageName; cd /usr/src/contrib/$appPackageName; python /usr/src/contrib/$appPackageName/setup.py build_plugins develop;"
#iocage exec $jail_name ln -sf /usr/local/$appPackageName/deluge_web /usr/bin/deluge_web
#iocage exec $jail_name ln -sf /usr/local/$appPackageName/$appServiceName /usr/bin/$appServiceName
#iocage exec $jail_name ln -sf /usr/local/$appPackageName/deluge-console /usr/bin/deluge-console

#iocage exec $jail_name 'ln -sf /usr/src/contrib/'$appPackageName'/'$appPackageName'/plugins/AutoAdd-1.8-py3.6.egg '$jail_config'/plugins/'
#iocage exec $jail_name 'ln -sf /usr/src/contrib/'$appPackageName'/'$appPackageName'/plugins/Blocklist-1.4-py3.6.egg '$jail_config'/plugins/'
#iocage exec $jail_name 'ln -sf /usr/src/contrib/'$appPackageName'/'$appPackageName'/plugins/Execute-1.3-py3.6.egg '$jail_config'/plugins/'
#iocage exec $jail_name 'ln -sf /usr/src/contrib/'$appPackageName'/'$appPackageName'/plugins/Extractor-0.7-py3.6.egg '$jail_config'/plugins/'
#iocage exec $jail_name 'ln -sf /usr/src/contrib/'$appPackageName'/'$appPackageName'/plugins/Label-0.3-py3.6.egg '$jail_config'/plugins/'
#iocage exec $jail_name 'ln -sf /usr/src/contrib/'$appPackageName'/'$appPackageName'/plugins/Notifications-0.3-py3.6.egg '$jail_config'/plugins/'
#iocage exec $jail_name 'ln -sf /usr/src/contrib/'$appPackageName'/'$appPackageName'/plugins/Scheduler-0.3-py3.6.egg '$jail_config'/plugins/'
#iocage exec $jail_name 'ln -sf /usr/src/contrib/'$appPackageName'/'$appPackageName'/plugins/Stats-0.4-py3.6.egg '$jail_config'/plugins/'
#iocage exec $jail_name 'ln -sf /usr/src/contrib/'$appPackageName'/'$appPackageName'/plugins/Toggle-0.4-py3.6.egg '$jail_config'/plugins/'
#iocage exec $jail_name 'ln -sf /usr/src/contrib/'$appPackageName'/'$appPackageName'/plugins/WebUi-0.2-py3.6.egg '$jail_config'/plugins/'

#cp $(dirname "$0")/$appName/$appServiceName /mnt/$zpool_name/apps/$appName/
#cp $(dirname "$0")/$appName/$appWebServiceName /mnt/$zpool_name/apps/$appName/
#iocage exec $jail_name mv "$jail_config/$appServiceName" "/usr/local/etc/rc.d/"
#iocage exec $jail_name mv "$jail_config/$appWebServiceName" "/usr/local/etc/rc.d/"

iocage exec $jail_name pkg install -y $appPackageName
_checkPkgInstallResults "" "$appName" ""

iocage exec $jail_name chmod 755 "/usr/local/etc/rc.d/$appServiceName"
iocage exec $jail_name chmod 755 "/usr/local/etc/rc.d/$appWebServiceName"

#iocage exec $jail_name chown -R $media_user_name:$media_group_name "/usr/local/$appPackageName"
iocage exec $jail_name chown -R $media_user_name:$media_group_name "$jail_config"
iocage exec $jail_name chown -R $media_user_name:$media_group_name "$jail_media_folder"
iocage exec $jail_name chown -R $media_user_name:$media_group_name "$download"

_restartService "$appServiceName" "$jail_name"
if ! _isConfigFileCreated "$appConfigPath_core"; then _restartService "$appServiceName" "$jail_name"; fi
if ! _isConfigFileCreated "$appConfigPath_core"; then _fatalError "Application ($appName) failed to create configuration file" "$appConfigPath_core"; fi
_restartService "$appWebServiceName" "$jail_name"
if ! _isConfigFileCreated "$appConfigPath_auth"; then _restartService "$appWebServiceName" "$jail_name"; fi
if ! _isConfigFileCreated "$appConfigPath_auth"; then _fatalError "Application ($appName) failed to create configuration file" "$appConfigPath_auth"; fi
#if ! _isConfigFileCreated "$appConfigPath_web"; then _fatalError "Application ($appName) failed to create configuration file" "$appConfigPath_web"; fi
_stopService "$appWebServiceName" "$jail_name"
_stopService "$appServiceName" "$jail_name"

authHasUser=$(cat "$appConfigPath_auth" | grep "$media_user_name:$media_user_password")
if [ ${#authHasUser} -eq 0 ]; then (echo "$media_user_name:$media_user_password:10" >> "$appConfigPath_auth"); fi
authHasUser=$(cat "$appConfigPath_web_auth" | grep "$media_user_name:$media_user_password")
if [ ${#authHasUser} -eq 0 ]; then (echo "$media_user_name:$media_user_password:10" >> "$appConfigPath_web_auth"); fi

_replaceConfig "$appConfigPath_core"	"\"format\":"	"^[[:space:]]*\"allow_remote\":"	":.*$"										": true, ";
_replaceConfig "$appConfigPath_core"	"\"format\":"	"\"move_completed_path\":"				"\"/Downloads\",.*$"			"\"$download\", ";
_replaceConfig "$appConfigPath_core"	"\"format\":"	"\"torrentfiles_location\":"			"\"/Downloads\",.*$"			"\"$download\", ";
_replaceConfig "$appConfigPath_core"	"\"format\":"	"\"download_location\":"					"\"/Downloads\",.*$"			"\"$download_incomplete_torrent\", ";
#_replaceConfig "$appConfigPath_core"	"\"format\":"	"\"plugins_location\":"						"\"/config/plugins\",.*$"	"\"$jail_config/plugins\", ";

_replaceConfig "$appConfigPath_hostlist"			"\"hosts\":"			"^[[:space:]]*\"127.0.0.1\",[[:space:]]*$"				"\"127.0.0.1\",[[:space:]]*$"				"\"0.0.0.0\", ";
#_replaceConfig "$appConfigPath_hostlist"			"\"hosts\":"			"^[[:space:]]*58846,"								"^[[:space:]]*58846[[:space:]]*$"		"58846, ";
_replaceConfig "$appConfigPath_hostlist"			"\"hosts\":"			"^[[:space:]]*\"\",[[:space:]]*$"	"\"\",[[:space:]]*$"								"\"$media_user_name\", ";
_replaceConfig "$appConfigPath_hostlist"			"\"hosts\":"			"^[[:space:]]*\"\"[[:space:]]*$"	"\".*\"[[:space:]]*$"								"\"$media_user_password\"";

pwd_salt=$(awk -F ":" '/pwd_salt/ {print $2}' "$appConfigPath_web")
pwd_salt="${pwd_salt%\", }"
pwd_salt="${pwd_salt%\",}"
pwd_salt="${pwd_salt# \"}"
pwd_sha1=$(echo -n "$pwd_salt$media_user_password" | openssl dgst -sha1 |awk '{print $2}')
_replaceConfig "$appConfigPath_web"		"\"format\":"		"\"pwd_sha1\":"		": \".*$"		": \"$pwd_sha1\", ";

ln -s /mnt/$zpool_name/apps/$appName/$jail_name.key.pem /mnt/$zpool_name/apps/$appName/ssl/$jail_name.key.pem
ln -s /mnt/$zpool_name/apps/$appName/$jail_name.cert.pem /mnt/$zpool_name/apps/$appName/ssl/$jail_name.cert.pem

_restartService "$appServiceName" "$jail_name"
_restartService "$appWebServiceName" "$jail_name"

_checkInstallResults "" "$appName" ""

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2;

_log 8 "+" "Installing application ($appName) in jail ($jail_name)" "COMPLETED" 1;

if [ $setup_apps_enabled == "YES" ]; then
  _launchScript "$(dirname "$0")/setup_app/setupDeluge.sh" "Installer" "DELUGE" "SETUP APPLICATION" "$deluge_enabled"
fi

# Restore standard redirection
exec &>/dev/tty
