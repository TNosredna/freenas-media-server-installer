#!/bin/bash
log_enabled="$1"

appName="headphones"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-install.log"
log_err_file_path="$HOME/$appName-install.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="headphones"
appServiceName="headphones"
applicationPort="$headphones_jail_port"
appURI="$headphones_web_root"
applicationPackageDependencies="python27 py27-sqlite3 security/py-openssl"
appAutoUpdateString='cd /usr/local/'$appPackageName'\nprev=\$(git rev-parse HEAD)\ngit pull --rebase\ntest \"\$prev\" != \"\$(git rev-parse HEAD)\" && service '$appServiceName' restart'
appComment="Headphone\'s (Music searcher) configuration, databases, and logs'"
appConfigPath="/mnt/$zpool_name/apps/$appName/config.ini"
#appConfigPath="/mnt/$zpool_name/apps/$appName/settings.conf"

source $(dirname "$0")/createBaseMediaJail.sh

_log 8 "/" "Setting up the application jail ($jail_name) home" "STARTING" 1;
iocage exec $jail_name sysrc $appServiceName\_enable="$headphones_enabled"
iocage exec $jail_name sysrc $appServiceName\_user="$media_user_name"
iocage exec $jail_name sysrc $appServiceName\_group="$media_group_name"
iocage exec $jail_name sysrc $appServiceName\_dir="/usr/local/$appPackageName"
iocage exec $jail_name sysrc $appServiceName\_conf="$jail_config/config.ini"
iocage exec $jail_name sysrc $appServiceName\_flags="--datadir $jail_config --host=0.0.0.0"

iocage exec $jail_name ln -sf /usr/local/bin/python2.7 /usr/bin/python
iocage exec $jail_name ln -sf /usr/local/bin/python2.7 /usr/local/bin/python
iocage exec $jail_name ln -sf /usr/local/bin/python2.7 /usr/bin/python2
_log 8 "+" "Setting up the application jail ($jail_name) home" "COMPLETED" 1;

_log 8 "/" "Installing application ($appName) in jail ($jail_name)" "STARTING" 1;
iocage exec $jail_name git clone "https://github.com/rembo10/headphones.git" "/usr/local/$appPackageName"
iocage exec $jail_name chown -Rf $media_user_name:$media_group_name "/usr/local/$appPackageName"
iocage exec $jail_name cp "/usr/local/$appPackageName/init-scripts/init.freebsd" "/usr/local/etc/rc.d/$appServiceName"
iocage exec $jail_name chmod u+x "/usr/local/etc/rc.d/$appServiceName"
iocage exec $jail_name sed -i '' -e 's?/var/run/headphones/headphones.pid?'$jail_config'/'$appServiceName'.pid?g' /usr/local/etc/rc.d/$appServiceName
iocage exec $jail_name sed -i '' -e 's?command_interpreter="/usr/local/bin/python"?command_interpreter="python"?g' /usr/local/etc/rc.d/$appServiceName

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_media_music"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $download"

_restartService "$appServiceName" "$jail_name"
_restartService "$appServiceName" "$jail_name"  # need second restart for the config.ini to be created
if ! _isConfigFileCreated "$appConfigPath"; then _fatalError "Application ($appName) failed to create configuration file" "$appConfigPath"; fi
_stopService "$appServiceName" "$jail_name"
_replaceConfig "$appConfigPath" "\[General\]"   "launch_browser = "           "= 1$"            "= 0";
_replaceConfig "$appConfigPath" "\[General\]"   "api_key = "                  "= \"\"$"         "= $headphones_api_key";
_replaceConfig "$appConfigPath" "\[General\]"   "api_enabled = "              "= 0$"            "= 1";
_replaceConfig "$appConfigPath" "\[General\]"   "http_port = "                "= 8181$"         "= $applicationPort";
_replaceConfig "$appConfigPath" "\[General\]"   "http_host = "                "= localhost$"    "= 0.0.0.0";
_replaceConfig "$appConfigPath" "\[General\]"   "download_dir = "             "= \"\"$"         "= $download_incomplete_process_music";
_replaceConfig "$appConfigPath" "\[General\]"   "download_torrent_dir = "     "= \"\"$"         "= $download_incomplete_process_music";
_replaceConfig "$appConfigPath" "\[SABnzbd\]"   "sab_host = "                 "= \"\"$"         "= $sabnzbd_jail_ip:$sabnzbd_jail_port";
_replaceConfig "$appConfigPath" "\[SABnzbd\]"   "sab_category = "             "= \"\"$"         "= music";
_replaceConfig "$appConfigPath" "\[SABnzbd\]"   "sab_apikey = "               "= \"\"$"         "= $sabnzbd_api_key";
_replaceConfig "$appConfigPath" "\[Deluge\]"    "deluge_host = "              "= \"\"$"         "= $deluge_jail_ip:$deluge_jail_port";
_replaceConfig "$appConfigPath" "\[Deluge\]"    "deluge_done_directory = "    "= \"\"$"         "= $download_incomplete_process_music";
_replaceConfig "$appConfigPath" "\[Deluge\]"    "deluge_label = "             "= \"\"$"         "= music";
_replaceConfig "$appConfigPath" "\[Deluge\]"    "deluge_password = "          "= \"\"$"         "= $jail_media_folder_music";
_restartService "$appServiceName" "$jail_name"

_checkInstallResults "" "$appName" ""

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2;

_log 8 "+" "Installing application ($appName) in jail ($jail_name)" "COMPLETED" 1;

if [ $setup_apps_enabled == "YES" ]; then
  _launchScript "$(dirname "$0")/setup_app/setupHeadphones.sh" "Installer" "HEADPHONES" "SETUP APPLICATION" "$headphones_enabled"
fi

# Restore standard redirection
exec &>/dev/tty
