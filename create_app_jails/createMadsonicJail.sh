#!/bin/bash
log_enabled="$1"

appName="madsonic"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-install.log"
log_err_file_path="$HOME/$appName-install.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="madsonic"
appServiceName="madsonic"
applicationPort="$madsonic_jail_port"
appURI="$madsonic_web_root"
applicationPackageDependencies="openjdk8"
appAutoUpdateString=""
appComment="Madsonic\'s (Music manager) configuration, databases, and logs"
appConfigPath="/mnt/$zpool_name/apps/$appName/madsonic.properties"

source $(dirname "$0")/createBaseMediaJail.sh

_log 8 "/" "Setting up the application jail ($jail_name) home" "STARTING" 1;
iocage exec $jail_name sysrc $appServiceName\_enable="$madsonic_enabled"
iocage exec $jail_name sysrc $appServiceName\_user="$media_user_name"
iocage exec $jail_name sysrc $appServiceName\_group="$media_group_name"
iocage exec $jail_name sysrc $appServiceName\_home="$jail_config"
iocage exec $jail_name sysrc $appServiceName\_music_folder="$jail_media_folder_music"
iocage exec $jail_name sysrc $appServiceName\_port="$applicationPort"
iocage exec $jail_name sysrc $appServiceName\_bin="/usr/local/$appPackageName/$appServiceName.sh"
iocage exec $jail_name sysrc $appServiceName\_podcast_folder="$jail_media_folder_music/podcasts"
iocage exec $jail_name sysrc $appServiceName\_playlist_folder="$jail_media_folder_music/playlists"

iocage exec $jail_name mkdir -p $jail_media_folder_music/podcasts
iocage exec $jail_name mkdir -p $jail_media_folder_music/playlists
iocage exec $jail_name chown $media_user_name:$media_group_name $jail_media_folder_music/podcasts
iocage exec $jail_name chown $media_user_name:$media_group_name $jail_media_folder_music/playlists
_log 8 "+" "Setting up the application jail ($jail_name) home" "COMPLETED" 1;

_log 8 "/" "Installing application ($appName) in jail ($jail_name)" "STARTING" 1;
cp $(dirname "$0")/$appName/$appServiceName "/mnt/$zpool_name/apps/$appName/$appServiceName"
iocage exec $jail_name mkdir -p /usr/local/$appPackageName
iocage exec $jail_name "wget https://download.madsonic.org/6.2/20190425_madsonic-6.2.9084-standalone.zip -P /usr/local/$appPackageName && mv /usr/local/$appPackageName/*.zip /usr/local/$appPackageName/$appPackageName.zip"
iocage exec $jail_name cd /usr/local/$appPackageName
iocage exec $jail_name unzip -d /usr/local/$appPackageName /usr/local/$appPackageName/$appPackageName.zip
iocage exec $jail_name rm /usr/local/$appPackageName/$appPackageName.zip
iocage exec $jail_name ln -sf $jail_config/$appName /usr/local/etc/rc.d/$appServiceName
iocage exec $jail_name chmod a+x /usr/local/etc/rc.d/$appServiceName

iocage exec $jail_name "wget https://download.madsonic.org/transcode/20190425_madsonic-transcode-linux-x64.zip -P /usr/local/$appPackageName && mv /usr/local/$appPackageName/*.zip /usr/local/$appPackageName/$appPackageName-transcode.zip"
iocage exec $jail_name cd /usr/local/$appPackageName
iocage exec $jail_name unzip -d /usr/local/$appPackageName /usr/local/$appPackageName/$appPackageName-transcode.zip
iocage exec $jail_name rm /usr/local/$appPackageName/$appPackageName-transcode.zip
iocage exec $jail_name ln -sf /usr/local/bin/ffmpeg /usr/local/$appPackageName/transcode/ffmpeg
iocage exec $jail_name chown -Rf $media_user_name:$media_group_name /usr/local/$appPackageName
iocage exec $jail_name chmod 776 /usr/local/$appPackageName

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_media_music"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $download"

_restartService "$appServiceName" "$jail_name"
if ! _isConfigFileCreated "$appConfigPath"; then _fatalError "Application ($appName) failed to create configuration file" "$appConfigPath"; fi

_checkInstallResults "" "$appName" ""

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2;

_log 8 "+" "Installing application ($appName) in jail ($jail_name)" "COMPLETED" 1;

if [ $setup_apps_enabled == "YES" ]; then
  _launchScript "$(dirname "$0")/setup_app/setupMadsonic.sh" "Installer" "MADSONIC" "SETUP APPLICATION" "$madsonic_enabled"
fi

# Restore standard redirection
exec &>/dev/tty
