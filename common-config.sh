#!/bin/bash
set +x

# Enable Applications
deluge_enabled="NO"
sabnzbd_enabled="NO"
plex_enabled="NO"
couchpotato_enabled="NO"
radarr_enabled="NO"
medusa_enabled="NO"
calibre_enabled="NO"
lazylibrarian_enabled="NO"
madsonic_enabled="NO"
headphones_enabled="NO"


# Jail Installation Configuration (install_jails_enabled="YES")
install_jails_enabled="NO"
# General Settings (Jail Installation)
subnet=""
router_ip=""
freenas_ip=""
zpool_name="Barn"
root_user_name="root"
root_user_password=""
media_user_name="media"
media_user_password=""
media_group_name="media"
notify_gmail_address=""
openvpn_user_name=""
openvpn_user_password=""
# Torrent downloader
deluge_jail_ip=""
deluge_jail_mac=""          # eg "xx:xx:xx:xx:xx:xx xx:xx:xx:xx:xx:xx"
# NZB downloader
sabnzbd_jail_ip=""
sabnzbd_jail_mac=""         # eg "xx:xx:xx:xx:xx:xx xx:xx:xx:xx:xx:xx"
# Media manager
plex_jail_ip=""
plex_jail_mac=""            # eg "xx:xx:xx:xx:xx:xx xx:xx:xx:xx:xx:xx"
# Movie searcher
couchpotato_jail_ip=""
couchpotato_jail_mac=""     # eg "xx:xx:xx:xx:xx:xx xx:xx:xx:xx:xx:xx"
# Movie searcher
radarr_jail_ip=""
radarr_jail_mac=""     			# eg "xx:xx:xx:xx:xx:xx xx:xx:xx:xx:xx:xx"
# TV searcher
medusa_jail_ip=""
medusa_jail_mac=""          # eg "xx:xx:xx:xx:xx:xx xx:xx:xx:xx:xx:xx"
# eBook manager
calibre_jail_ip=""
calibre_jail_mac=""         # eg "xx:xx:xx:xx:xx:xx xx:xx:xx:xx:xx:xx"
# eBook searcher
lazylibrarian_jail_ip=""
lazylibrarian_jail_mac=""   # eg "xx:xx:xx:xx:xx:xx xx:xx:xx:xx:xx:xx"
# Music manager
madsonic_jail_ip=""
madsonic_jail_mac=""        # eg "xx:xx:xx:xx:xx:xx xx:xx:xx:xx:xx:xx"
# Music searcher
headphones_jail_ip=""
headphones_jail_mac=""      # eg "xx:xx:xx:xx:xx:xx xx:xx:xx:xx:xx:xx"


# Application Setup Configuration (setup_apps_enabled="YES")
setup_apps_enabled="NO"
backup_existing_setups="YES"
# General Settings (Application Setup)
notify_gmail_password=""
usenet_provider_user_name=""
usenet_provider_user_password=""
usenet_provider_retention=""
git_user_name=""
git_user_password=""
omdb_api_key=""
tmdb_api_key=""
newznab_api_key=""
nzbgeek_api_key=""
nzbsu_api_key=""
rating_host=""
rating_api_key=""
goodreads_api_key=""
googlebooks_api_key=""

# Media manager
plex_online_user_name=""
plex_online_user_password=""
plex_online_email=""
plex_jail_name="plex"
plex_jail_port="32400"
plex_web_root="/web/index.html"
plex_sections="movie,3|tv,4"
plex_server_friendly_name="Media Appliance Plex Server"
plex_openvpn_enabled="NO"
plex_ipfw_enabled="NO"
# NZB downloader
sabnzbd_nzb_key=""
sabnzbd_indexer_api_key=""
sabnzbd_jail_name="sabnzbd"
sabnzbd_jail_port="8080"
sabnzbd_web_root="/sabnzbd"
sabnzbd_openvpn_enabled="NO"
sabnzbd_ipfw_enabled="NO"
# Torrent downloader
deluge_jail_name="deluge"
deluge_jail_port="58846"
deluge_jail_http_port="8112"
deluge_web_root=""
deluge_openvpn_enabled="NO"
deluge_ipfw_enabled="NO"
# Movie searcher
couchpotato_jail_name="couchpotato"
couchpotato_jail_port="5050"
couchpotato_web_root=""
couchpotato_openvpn_enabled="NO"
couchpotato_ipfw_enabled="NO"
# Movie searcher
radarr_jail_name="radarr"
radarr_jail_port="7878"
radarr_web_root=""
radarr_openvpn_enabled="NO"
radarr_ipfw_enabled="NO"
# TV searcher
medusa_jail_name="medusa"
medusa_jail_port="8081"
medusa_web_root="/home"
medusa_openvpn_enabled="NO"
medusa_ipfw_enabled="NO"
# eBook manager
calibre_jail_name="calibre"
calibre_jail_port="8082"
calibre_web_root="/"
calibre_openvpn_enabled="NO"
calibre_ipfw_enabled="NO"
# eBook searcher
lazylibrarian_jail_name="lazylibrarian"
lazylibrarian_jail_port="5299"
lazylibrarian_web_root=""
lazylibrarian_openvpn_enabled="NO"
lazylibrarian_ipfw_enabled="NO"
# Music manager
madsonic_jail_name="madsonic"
madsonic_jail_port="4040"
madsonic_web_root="/index.view"
madsonic_openvpn_enabled="NO"
madsonic_ipfw_enabled="NO"
# Music searcher
headphones_jail_name="headphones"
headphones_jail_port="8181"
headphones_web_root="/"
headphones_openvpn_enabled="NO"
headphones_ipfw_enabled="NO"


### Don't change the following unless you know what you're doing and have a very good reason ###
nzbtomedia_enabled="YES"
log_enabled="TRUE"

#Media Processing Folder Structure
download="/mnt/download"                                                                # All content is being downloaded here (categorised and uncategorised )
download_incomplete="/mnt/download/incomplete"                                          # Not used
download_incomplete_torrent="/mnt/download/incomplete/torrent"                          # (deluge | Download | Download to)
download_incomplete_nzb="/mnt/download/incomplete/nzb"                                  # (sabnzbd | incomplete)
download_incomplete_process="/mnt/download/incomplete/process"                          # (autoProcessMedia | [Torrent] | outputDirectory)
download_incomplete_process_tv="/mnt/download/incomplete/process/tv"                    # (medusa | config | postProcessing | config/postProcessing)
download_incomplete_process_movies="/mnt/download/incomplete/process/movies"            # (couchpotato | settings | renamer | from)
download_incomplete_process_ebooks="/mnt/download/incomplete/process/ebooks"            # (lazylibrarian | settings | renamer | from)
download_incomplete_process_audiobooks="/mnt/download/incomplete/process/audioobooks"
download_incomplete_process_music="/mnt/download/incomplete/process/music"              # (headphones | settings | renamer | from)
download_incomplete_manual="/mnt/download/manualImport"
jail_media_folder_tv="/mnt/media/tv"                                                    # (medusa | tv | series | destination)
jail_media_folder_movies="/mnt/media/movies"                                            # (couchpotato | settings | renamer | To)
jail_media_folder_ebooks="/mnt/media/ebooks"                                            # (lazylibrarian | settings | renamer | To)
jail_media_folder_music="/mnt/media/music"                                              # (headphones | settings | renamer | To)
jail_media_folder_audiobooks="/mnt/media/audiobooks"
jail_media_folder_pictures="/mnt/media/pictures"
jail_media_folder_home_videos="/mnt/media/homeVideos"
jail_media_folder="/mnt/media"
jail_config="/mnt/config"
jail_nzbtomedia="/mnt/nzbtomedia"

couchpotato_api_key="944cb8ca9dc1f5ca4db9067e31881494"																	# until you can generate a new one from the gui
radarr_api_key="944cb8ca9dc1f5ca4db9067e31881494"																				# until you can generate a new one from the gui
headphones_api_key="944cb8ca9dc1f5ca4db9067e31881494"                                   # until you can generate a new one from the gui
lazylibrarian_api_key="944cb8ca9dc1f5ca4db9067e31881494"                                # until you can generate a new one from the gui
medusa_api_key="944cb8ca9dc1f5ca4db9067e31881494"																				# until you can generate a new one from the gui
plex_online_auth_token=""
sabnzbd_api_key="944cb8ca9dc1f5ca4db9067e31881494"                                      # until you can generate a new one from the gui

jail_freebsd_version=""

#checkoutURL="https://gitlab.com/TNosredna/freenas-media-server-installer/-/archive/master/freenas-media-server-installer-master.zip"
checkoutURL="https://gitlab.com/TNosredna/freenas-media-server-installer/-/archive/develop/freenas-media-server-installer-develop.zip"

if [ "$log_enabled" == "TRUE" ]; then set -x; else set +x; fi
appCounter=0