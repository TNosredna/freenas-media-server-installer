#!/bin/bash
log_enabled="$1"

appName="lazylibrarian"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-setup.log"
log_err_file_path="$HOME/$appName-setup.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="lazylibrarian"
appServiceName="lazylibrarian"
applicationPort="$lazylibrarian_jail_port"
appURI="$lazylibrarian_web_root"
appConfigPath="/mnt/$zpool_name/apps/$appName/config.ini"

jailName_var=$appName\_jail_name
jail_name=${!jailName_var}
jail_ip_var=$appName\_jail_ip
jail_ip=${!jail_ip_var}
jail_mac_var=$appName\_jail_mac
jail_mac=${!jail_mac_var}
openvpn_enabled_var=$appName\_openvpn_enabled
openvpn_enabled=${!openvpn_enabled_var}
ipfw_enabled_var=$appName\_ipfw_enabled
ipfw_enabled=${!ipfw_enabled_var}

_log 8 "/" "Setting up application ($appName) configuration" "STARTING" 1;
_stopService "$appServiceName" "$jail_name"

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config || true"

_replaceConfig "$appConfigPath"		 "\[API\]"						"book_api ="		 							"=.*$"			"= GoodReads";
_replaceConfig "$appConfigPath"		 "\[API\]"						"gb_api ="			 							"=.*$"			"= $googlebooks_api_key";
_replaceConfig "$appConfigPath"		 "\[API\]"						"gr_api ="			 							"=.*$"			"= $goodreads_api_key";

_replaceConfig "$appConfigPath"		 "\[Custom\]"					"custom_script ="							"=.*$"			"= $jail_nzbtomedia/nzbToLazyLibrarian.py";
_replaceConfig "$appConfigPath"		 "\[Custom\]"					"use_custom ="								"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[Custom\]"					"custom_notify_ondownload ="	"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[Custom\]"					"custom_notify_onsnatch ="		"=.*$"			"= 0";

_replaceConfig "$appConfigPath"		 "\[DELUGE\]"					"deluge_base ="								"=.*$"			"= $deluge_web_root";
_replaceConfig "$appConfigPath"		 "\[DELUGE\]"					"deluge_dir ="			 					"=.*$"			"= $download_incomplete_process_ebooks";
_replaceConfig "$appConfigPath"		 "\[DELUGE\]"					"deluge_host ="								"=.*$"			"= $deluge_jail_ip";
_replaceConfig "$appConfigPath"		 "\[DELUGE\]"					"deluge_label ="		 					"=.*$"			"= ebooks";
_replaceConfig "$appConfigPath"		 "\[DELUGE\]"					"deluge_port ="								"=.*$"			"= $deluge_jail_port";
_replaceConfig "$appConfigPath"		 "\[DELUGE\]"					"deluge_pass ="								"=.*$"			"= $media_user_password";
_replaceConfig "$appConfigPath"		 "\[DELUGE\]"					"deluge_user ="								"=.*$"			"= $media_user_name";

_replaceConfig "$appConfigPath"		 "\[Email\]"					"email_smtp_password ="				"=.*$"			"= $notify_gmail_password";
_replaceConfig "$appConfigPath"		 "\[Email\]"					"email_from ="								"=.*$"			"= $notify_gmail_address";
_replaceConfig "$appConfigPath"		 "\[Email\]"					"email_tls ="									"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[Email\]"					"email_to ="									"=.*$"			"= $notify_gmail_address";
_replaceConfig "$appConfigPath"		 "\[Email\]"					"use_email ="									"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[Email\]"					"email_smtp_port ="						"=.*$"			"= 587";
_replaceConfig "$appConfigPath"		 "\[Email\]"					"email_smtp_user ="						"=.*$"			"= $notify_gmail_address";
_replaceConfig "$appConfigPath"		 "\[Email\]"					"email_smtp_server ="					"=.*$"			"= smtp.gmail.com";

_replaceConfig "$appConfigPath"		 "\[General\]"				"admin_email ="								"=.*$"			"= $notify_gmail_address";
_replaceConfig "$appConfigPath"		 "\[General\]"				"alternate_dir ="							"=.*$"			"= $download_incomplete_manual";
_replaceConfig "$appConfigPath"		 "\[General\]"				"api_enabled ="								"=.*$"			"= 1";
api_key=$(_getProperty "$appConfigPath" "api_key" "\[General\]")
if [ ${#api_key} -eq 0 ]; then
	_replaceConfig "$appConfigPath"	 "\[General\]"				"api_key ="										"=.*$"			"= $lazylibrarian_api_key";
fi
_replaceConfig "$appConfigPath"		 "\[General\]"				"audio_dir ="									"=.*$"			"= $jail_media_folder_audiobooks";
_replaceConfig "$appConfigPath"		 "\[General\]"				"author_img ="								"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[General\]"				"blacklist_failed ="					"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[General\]"				"bookstrap_theme ="						"=.*$"			"= slate";
_replaceConfig "$appConfigPath"		 "\[General\]"				"calibre_pass ="							"=.*$"			"= $media_user_password";
_replaceConfig "$appConfigPath"		 "\[General\]"				"calibre_server ="						"=.*$"			"= http://$calibre_jail_ip:$calibre_jail_port/#ebooks";
_replaceConfig "$appConfigPath"		 "\[General\]"				"calibre_use_server ="				"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[General\]"				"calibre_user ="							"=.*$"			"= $media_user_name";
_replaceConfig "$appConfigPath"		 "\[General\]"				"dir_perm ="									"=.*$"			"= 0o775";
_replaceConfig "$appConfigPath"		 "\[General\]"				"download_dir ="							"=.*$"			"= $download_incomplete_process_ebooks";
_replaceConfig "$appConfigPath"		 "\[General\]"				"ebook_dir ="									"=.*$"			"= $jail_media_folder_ebooks";
_replaceConfig "$appConfigPath"		 "\[General\]"				"ebook_type ="								"=.*$"			"= epub, mobi, pdf, azw3";
_replaceConfig "$appConfigPath"		 "\[General\]"				"file_perm ="									"=.*$"			"= 0o775";
_replaceConfig "$appConfigPath"		 "\[General\]"				"genre_tags ="								"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[General\]"				"git_program ="								"=.*$"			"= /usr/local/bin/git";
_replaceConfig "$appConfigPath"		 "\[General\]"				"http_host ="									"=.*$"			"= 0.0.0.0";
_replaceConfig "$appConfigPath"		 "\[General\]"				"http_port ="									"=.*$"			"= $applicationPort";
_replaceConfig "$appConfigPath"		 "\[General\]"				"http_root ="									"=.*$"			"= $lazylibrarian_web_root";
_replaceConfig "$appConfigPath"		 "\[General\]"				"imp_autoadd ="								"=.*$"			"= ";
_replaceConfig "$appConfigPath"		 "\[General\]"				"imp_autoadd_copy ="					"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[General\]"				"imp_autoaddmag_copy ="				"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[General\]"				"imp_calibredb ="							"=.*$"			"= /usr/local/bin/calibredb";
_replaceConfig "$appConfigPath"		 "\[General\]"				"imp_magcover ="							"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[General\]"				"imp_magopf ="								"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[General\]"				"imp_rename ="								"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[General\]"				"imp_singlebook ="						"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[General\]"				"launch_browser ="						"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[General\]"				"loglevel ="									"=.*$"			"= 2";
_replaceConfig "$appConfigPath"		 "\[General\]"				"loglimit ="									"=.*$"			"= 2000";
_replaceConfig "$appConfigPath"		 "\[General\]"				"logsize ="										"=.*$"			"= 1000000";
_replaceConfig "$appConfigPath"		 "\[General\]"				"mag_age ="										"=.*$"			"= ";
_replaceConfig "$appConfigPath"		 "\[General\]"				"mag_img ="										"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[General\]"				"mag_single ="								"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[General\]"				"mag_tab ="										"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[General\]"				"mag_type ="									"=.*$"			"= pdf";
_replaceConfig "$appConfigPath"		 "\[General\]"				"notify_with_title ="					"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[General\]"				"opf_tags ="									"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[General\]"				"reject_audio ="							"=.*$"			"= epub, mobi";
_replaceConfig "$appConfigPath"		 "\[General\]"				"reject_magmin ="							"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[General\]"				"reject_magsize ="						"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[General\]"				"reject_maxaudio ="						"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[General\]"				"reject_maxsize ="						"=.*$"			"= 75";
_replaceConfig "$appConfigPath"		 "\[General\]"				"reject_minaudio ="						"=.*$"			"= 0"; #=
_replaceConfig "$appConfigPath"		 "\[General\]"				"reject_words ="							"=.*$"			"= audiobook, mp3, nmr, unabr";
_replaceConfig "$appConfigPath"		 "\[General\]"				"show_genres ="								"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[General\]"				"sort_definite ="							"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[General\]"				"sort_surname ="							"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[General\]"				"task_age ="									"=.*$"			"= 2";
_replaceConfig "$appConfigPath"		 "\[General\]"				"wall_columns ="							"=.*$"			"= 6";
_replaceConfig "$appConfigPath"		 "\[General\]"				"wishlist_tags ="							"=.*$"			"= 0";

_replaceConfig "$appConfigPath"		 "\[Git\]"						"auto_update ="		 						"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[Git\]"						"git_branch ="								"=.*$"			"= master";
_replaceConfig "$appConfigPath"		 "\[Git\]"						"git_host ="									"=.*$"			"= gitlab.com";
_replaceConfig "$appConfigPath"		 "\[Git\]"						"git_repo ="									"=.*$"			"= lazylibrarian";
_replaceConfig "$appConfigPath"		 "\[Git\]"						"git_user ="									"=.*$"			"= LazyLibrarian";
_replaceConfig "$appConfigPath"		 "\[Git\]"						"install_type ="							"=.*$"			"= git";

_replaceConfig "$appConfigPath"		 "\[KAT\]"						"kat ="								 				"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[KAT\]"						"kat_dltypes ="				 				"=.*$"			"= E";
_replaceConfig "$appConfigPath"		 "\[KAT\]"						"kat_dlpriority ="						"=.*$"			"= 2";

_replaceConfig "$appConfigPath"		 "\[LibraryScan\]"		"found_status ="							"=.*$"			"= Open";
_replaceConfig "$appConfigPath"		 "\[LibraryScan\]"		"full_scan ="									"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[LibraryScan\]"		"imp_googleimage ="						"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[LibraryScan\]"		"imp_ignore ="								"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[LibraryScan\]"		"newauthor_books ="						"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[LibraryScan\]"		"newseries_status ="					"=.*$"			"= Active";

_replaceConfig "$appConfigPath"		 "\[Newznab0\]"				"api ="												"=.*$"			"= $nzbgeek_api_key";
_replaceConfig "$appConfigPath"		 "\[Newznab0\]"				"bookcat ="										"=.*$"			"= 7020";
_replaceConfig "$appConfigPath"		 "\[Newznab0\]"				"dispname ="					 				"=.*$"			"= NZBGeek";
_replaceConfig "$appConfigPath"		 "\[Newznab0\]"				"dlpriority ="				 				"=.*$"			"= 4";
_replaceConfig "$appConfigPath"		 "\[Newznab0\]"				"dltypes ="										"=.*$"			"= E";
_replaceConfig "$appConfigPath"		 "\[Newznab0\]"				"enabled ="										"=.*$"			"= True";
_replaceConfig "$appConfigPath"		 "\[Newznab0\]"				"extended ="					 				"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[Newznab0\]"				"generalsearch ="							"=.*$"			"= search";
_replaceConfig "$appConfigPath"		 "\[Newznab0\]"				"host ="							 				"=.*$"			"= https://api.nzbgeek.info/";

_replaceConfig "$appConfigPath"		 "\[OPDS\]"						"opds_authentication ="				"=.*$"		 	"= 1";
_replaceConfig "$appConfigPath"		 "\[OPDS\]"						"opds_enabled ="					 		"=.*$"		 	"= 1";
_replaceConfig "$appConfigPath"		 "\[OPDS\]"						"opds_metainfo ="							"=.*$"		 	"= 1";
_replaceConfig "$appConfigPath"		 "\[OPDS\]"						"opds_page ="									"=.*$"		 	"= 30";
_replaceConfig "$appConfigPath"		 "\[OPDS\]"						"opds_password ="							"=.*$"		 	"= $media_user_password";
_replaceConfig "$appConfigPath"		 "\[OPDS\]"						"opds_username ="							"=.*$"		 	"= $media_user_name";

_replaceConfig "$appConfigPath"		 "\[PostProcess\]"		"audiobook_dest_folder ="			"=.*$"		 	"= \$Author/\$Title";
_replaceConfig "$appConfigPath"		 "\[PostProcess\]"		"mag_delfolder ="							"=.*$"		 	"= 0";
_replaceConfig "$appConfigPath"		 "\[PostProcess\]"		"mag_dest_file ="							"=.*$"		 	"= \$IssueDate - \$Title";
_replaceConfig "$appConfigPath"		 "\[PostProcess\]"		"mag_dest_folder ="						"=.*$"		 	"= _Magazines/\$Title/\$IssueDate";
_replaceConfig "$appConfigPath"		 "\[PostProcess\]"		"mag_relative ="							"=.*$"		 	"= 0";
_replaceConfig "$appConfigPath"		 "\[PostProcess\]"		"one_format ="								"=.*$"		 	"= 1";

_replaceConfig "$appConfigPath"		 "\[SABnzbd\]"				"sab_api ="							 			"=.*$"			"= $sabnzbd_api_key";
_replaceConfig "$appConfigPath"		 "\[SABnzbd\]"				"sab_cat ="							 			"=.*$"			"= ebooks";
_replaceConfig "$appConfigPath"		 "\[SABnzbd\]"				"sab_host ="									"=.*$"			"= http://$sabnzbd_jail_ip";
_replaceConfig "$appConfigPath"		 "\[SABnzbd\]"				"sab_pass ="									"=.*$"			"= ";
_replaceConfig "$appConfigPath"		 "\[SABnzbd\]"				"sab_port ="									"=.*$"			"= $sabnzbd_jail_port";
_replaceConfig "$appConfigPath"		 "\[SABnzbd\]"				"sab_subdir ="								"=.*$"			"= sabnzbd";
_replaceConfig "$appConfigPath"		 "\[SABnzbd\]"				"sab_user ="									"=.*$"			"= ";

_replaceConfig "$appConfigPath"		 "\[SearchScan\]"			"delaysearch ="								"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[SearchScan\]"			"search_maginterval ="				"=.*$"			"= ";

_replaceConfig "$appConfigPath"		 "\[TDL\]"						"tdl ="												"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[TDL\]"						"tdl_dlpriority ="						"=.*$"			"= 3";
_replaceConfig "$appConfigPath"		 "\[TDL\]"						"tdl_dltypes ="								"=.*$"			"= E";
_replaceConfig "$appConfigPath"		 "\[TDL\]"						"tdl_host ="									"=.*$"			"= https://yts.ag/";

_replaceConfig "$appConfigPath"		 "\[TORRENT\]"				"keep_seeding ="							"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[TORRENT\]"				"seed_wait ="									"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[TORRENT\]"				"tor_downloader_deluge ="			"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[TORRENT\]"				"torrent_dir ="								"=.*$"			"= $download_incomplete_torrent";

_replaceConfig "$appConfigPath"		 "\[Torrof\]"					"trf ="												"=.*$"			"= 0";
_replaceConfig "$appConfigPath"		 "\[Torrof\]"					"trf_dltypes ="								"=.*$"			"= E";
_replaceConfig "$appConfigPath"		 "\[Torrof\]"					"trf_host ="									"=.*$"			"= https://thepiratebay.se/";

_replaceConfig "$appConfigPath"		 "\[TPB\]"						"tpb ="											 	"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[TPB\]"						"tpb_dlpriority ="						"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[TPB\]"						"tpb_dltypes ="							 	"=.*$"			"= E";

_replaceConfig "$appConfigPath"		 "\[USENET\]"					"nzb_downloader_sabnzbd ="		"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[USENET\]"					"usenet_retention ="					"=.*$"			"= $usenet_provider_retention";

_replaceConfig "$appConfigPath"		 "\[WWT\]"						"wwt ="												"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[WWT\]"						"wwt_dlpriority ="						"=.*$"			"= 2";
_replaceConfig "$appConfigPath"		 "\[WWT\]"						"wwt_dltypes ="								"=.*$"			"= E";
_replaceConfig "$appConfigPath"		 "\[WWT\]"						"wwt_host ="									"=.*$"			"= https://katcr.co";

_replaceConfig "$appConfigPath"		 "\[ZOO\]"						"zoo ="												"=.*$"			"= 1";
_replaceConfig "$appConfigPath"		 "\[ZOO\]"						"zoo_dlpriority ="						"=.*$"			"= 2";
_replaceConfig "$appConfigPath"		 "\[ZOO\]"						"zoo_dltypes ="								"=.*$"			"= E";
_replaceConfig "$appConfigPath"		 "\[ZOO\]"						"zoo_host ="									"=.*$"			"= https://torrentz2.eu/";

newUsenetServer=$(cat <<-USENETSERVER
		[Newznab1]
		api = $nzbsu_api_key
		apilimit = 0
		audiocat = 3030
		audiosearch =
		bookcat = 7020
		booksearch = book
		comiccat =
		comicsearch =
		dispname = NZB.su
		dlpriority = 3
		dltypes = E
		enabled = True
		extended = 1
		generalsearch = search
		host = http://api.nzb.su/
		magcat =
		magsearch =
		manual = False
		updated =

	USENETSERVER
)
_insertLine "$appConfigPath" "\[Newznab0\]" "$newUsenetServer"

newUsenetServer=$(cat <<-USENETSERVER
		[Newznab2]
		api =
		apilimit = 0
		audiocat = 3030
		audiosearch =
		bookcat = 7020
		booksearch = book
		comiccat =
		comicsearch =
		dispname = RARBG
		dlpriority = 3
		dltypes = E
		enabled = True
		extended = 1
		generalsearch = search
		host = https://rarbg.to/torrents.php
		magcat =
		magsearch =
		manual = False
		updated =

	USENETSERVER
)
_insertLine "$appConfigPath" "\[Newznab0\]" "$newUsenetServer"

newUsenetServer=$(cat <<-USENETSERVER
		[Newznab3]
		api =
		apilimit = 0
		audiocat = 3030
		audiosearch =
		bookcat = 7020
		booksearch = book
		comiccat =
		comicsearch =
		dispname = Binsearch
		dlpriority = 1
		dltypes = E
		enabled = True
		extended = 1
		generalsearch = search
		host = https://www.binsearch.info/
		magcat =
		magsearch =
		manual = False
		updated =

	USENETSERVER
)
_insertLine "$appConfigPath" "\[Newznab0\]" "$newUsenetServer"

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config || true"
_restartService "$appServiceName" "$jail_name"

# Get new app's api key from it's configuration and add it to common-config.sh
api_key=$(_getProperty "$appConfigPath" "api_key" "\[General\]")
if [ ${#api_key} -gt 0 ]; then
	_replaceConfig "$(dirname "$0")/../common-config.sh"		 "#!\/bin\/bash"		 "^[[:space:]]*lazylibrarian_api_key="		 "=\".*\""		 "=\"$api_key\"";
	_updateApiKeys
fi

_checkSetupResults "" "$appName" ""

_log 8 "+" "Setting up application ($appName) configuration" "COMPLETED" 1;

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2;

# Restore standard redirection
exec &>/dev/tty
