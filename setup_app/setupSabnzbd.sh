#!/bin/bash
log_enabled="$1"

appName="sabnzbd"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-setup.log"
log_err_file_path="$HOME/$appName-setup.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="sabnzbdplus"
appServiceName="sabnzbd"
applicationPort="$sabnzbd_jail_port"
appURI="$sabnzbd_web_root"
appConfigPath="/mnt/$zpool_name/apps/$appName/sabnzbd.ini"

jailName_var=$appName\_jail_name
jail_name=${!jailName_var}
jail_ip_var=$appName\_jail_ip
jail_ip=${!jail_ip_var}
jail_mac_var=$appName\_jail_mac
jail_mac=${!jail_mac_var}
openvpn_enabled_var=$appName\_openvpn_enabled
openvpn_enabled=${!openvpn_enabled_var}
ipfw_enabled_var=$appName\_ipfw_enabled
ipfw_enabled=${!ipfw_enabled_var}

_log 8 "/" "Setting up the application ($appName) configuration" "STARTING" 1;
_stopService "$appServiceName" "$jail_name"
rm -f "$appConfigPath.bak" # this will cause SAB to not start (with errors), which is what we want if the config is bad.	Otherwise it'll silently overwrite your config.

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config || true"
api_key=$(_getProperty "$appConfigPath" "api_key" "\[misc\]")
if [ ${#api_key} -eq 0 ]; then
	_replaceConfig "$appConfigPath"	"\[misc\]"	 "^api_key ="												 "=.*$"	 "= $sabnzbd_api_key";
fi
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^action_on_unwanted_extensions ="		"=.*$"	 "= 0";	#=2
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^api_warnings ="										"=.*$"	 "= 1";	#=0
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^auto_sort ="												"=.*$"	 "= 1";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^cleanup_list ="										"=.*$"	 "= png, sfv, nfo";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^complete_dir ="										"=.*$"	 "= $download_incomplete_process";	#=/mnt/downloads/incomplete/process
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^debug_log_decoding ="							"=.*$"	 "= 1";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^direct_unpack ="										"=.*$"	 "= 1";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^direct_unpack_tested ="						"=.*$"	 "= 0";	#=1
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^download_dir ="										"=.*$"	 "= $download_incomplete_nzb";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^email_account ="										"=.*$"	 "= $notify_gmail_address";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^email_from ="											"=.*$"	 "= $notify_gmail_address";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^email_full ="											"=.*$"	 "= 1";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^email_pwd ="												"=.*$"	 "= $notify_gmail_password";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^email_server ="										"=.*$"	 "= smtp.gmail.com:587";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^email_to ="												"=.*$"	 "= $notify_gmail_address";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^empty_postproc ="									"=.*$"	 "= 1";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^enable_all_par ="									"=.*$"	 "= 1";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^enable_bonjour ="									"=.*$"	 "= 0";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^enable_https_verification ="				"=.*$"	 "= 0";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^flat_unpack ="											"=.*$"	 "= 1";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^folder_rename ="										"=.*$"	 "= 0";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^history_retention ="								"=.*$"	 "= 0";	#=4d
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^host ="														"=.*$"	 "= 0.0.0.0";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^ignore_samples ="									"=.*$"	 "= 1";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^ignore_unrar_dates ="							"=.*$"	 "= 1";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^inet_exposure ="										"=.*$"	 "= 4";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^load_balancing ="									"=.*$"	 "= 2";	#=1
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^local_ranges ="										"=.*$"	 "= ${subnet%.*},";	#=10.11.12.1,
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^movie_categories ="								"=.*$"	 "= movies,";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^new_nzb_on_failure ="							"=.*$"	 "= 1";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^no_penalties ="										"=.*$"	 "= 0";	#=1
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^notified_new_skin ="								"=.*$"	 "= 2";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^nzb_key ="													"=.*$"	 "= $sabnzbd_nzb_key";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^osx_menu ="												"=.*$"	 "= 0";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^osx_speed ="												"=.*$"	 "= 0";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^overwrite_files ="									"=.*$"	 "= 1";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^pause_on_pwrar ="									"=.*$"	 "= 2";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^permissions ="											"=.*$"	 "= 777";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^pre_check ="												"=.*$"	 "= 1";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^quota_resume ="										"=.*$"	 "= 0"; #=1
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^rating_api_key ="									"=.*$"	 "= $rating_api_key";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^rating_enable ="										"=.*$"	 "= 1";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^rating_host ="											"=.*$"	 "= $rating_host";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^reject_duplicate_files ="					"=.*$"	 "= 0";	#=1
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^refresh_rate ="										"=.*$"	 "= 0";	#=1
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^replace_dots ="										"=.*$"	 "= 1";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^rss_filenames ="										"=.*$"	 "= 1";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^safe_postproc ="										"=.*$"	 "= 0";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^sanitize_safe ="										"=.*$"	 "= 0";	#=1
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^script_can_fail ="									"=.*$"	 "= 1";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^script_dir ="											"=.*$"	 "= $jail_nzbtomedia";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^tv_categories ="										"=.*$"	 "= tv,";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^url_base ="												"=.*$"	 "= $sabnzbd_web_root";
_replaceConfig "$appConfigPath"	"\[misc\]"	 "^win_menu ="												"=.*$"	 "= 0";

_renameExistingKey "$appConfigPath" "\[\[news-eu\.newshosting\.com\]\]" "\[\[news-eu\.newshosting\.com-OLD\]\]"
newUsenetServer=$(cat <<-USENETSERVER2
		[[news-eu.newshosting.com]]
		username = $usenet_provider_user_name
		priority = 1
		enable = 1
		displayname = EU Usenet Server
		name = news-eu.newshosting.com
		ssl_ciphers = ""
		notes = ""
		connections = 15
		ssl = 1
		host = news-eu.newshosting.com
		timeout = 60
		ssl_verify = 1
		send_group = 0
		password = $usenet_provider_user_password
		optional = 1
		port = 443
		retention = $usenet_provider_retention
	USENETSERVER2
)
_appendLine "$appConfigPath" "\[servers\]" "$newUsenetServer"

_renameExistingKey "$appConfigPath" "\[\[news\.newshosting\.com\]\]" "\[\[news\.newshosting\.com-OLD\]\]"
newUsenetServer=$(cat <<-USENETSERVER1
		[[news.newshosting.com]]
		username = $usenet_provider_user_name
		priority = 0
		enable = 1
		displayname = Geo Auto-Select Usenet Server
		name = news.newshosting.com
		ssl_ciphers = ""
		notes = ""
		connections = 15
		ssl = 1
		host = news.newshosting.com
		timeout = 60
		ssl_verify = 1
		send_group = 0
		password = $usenet_provider_user_password
		optional = 0
		port = 443
		retention = $usenet_provider_retention
	USENETSERVER1
)
_appendLine "$appConfigPath" "\[servers\]" "$newUsenetServer"

_renameExistingKey "$appConfigPath" "\[\[\*\]\]" "\[\[\*-OLD\]\]"
newCategory=$(cat <<-CATEGORY1
		[[*]]
		priority = 2
		pp = 2
		name = *
		script = None
		newzbin = ""
		order = 0
		dir = ""
	CATEGORY1
)
_appendLine "$appConfigPath" "\[categories\]" "$newCategory"

_renameExistingKey "$appConfigPath" "\[\[manual\]\]" "\[\[manual-OLD\]\]"
newCategory=$(cat <<-CATEGORY2
		[[manual]]
		priority = -100
		pp = 2
		name = manual
		script = None
		newzbin = ""
		order = 0
		dir = $download
	CATEGORY2
)
_appendLine "$appConfigPath" "\[categories\]" "$newCategory"

_renameExistingKey "$appConfigPath" "\[\[ebooks\]\]" "\[\[ebooks-OLD\]\]"
newCategory=$(cat <<-CATEGORY3
		[[ebooks]]
		priority = -100
		pp = 2
		name = ebooks
		script = nzbToLazyLibrarian.py
		newzbin = ""
		order = 0
		dir = "ebooks"
	CATEGORY3
)
_appendLine "$appConfigPath" "\[categories\]" "$newCategory"

_renameExistingKey "$appConfigPath" "\[\[music\]\]" "\[\[music-OLD\]\]"
newCategory=$(cat <<-CATEGORY4
		[[music]]
		priority = -100
		pp = 3
		name = music
		script = nzbToHeadPhones.py
		newzbin = ""
		order = 0
		dir = "music"
	CATEGORY4
)
_appendLine "$appConfigPath" "\[categories\]" "$newCategory"

_renameExistingKey "$appConfigPath" "\[\[movies\]\]" "\[\[movies-OLD\]\]"
if [ "$radarr_enabled" == "YES" ];
	then
		scriptName="nzbToRadarr.py"
	else
		scriptName="nzbToCouchPotato.py"
fi
newCategory=$(cat <<-CATEGORY5
		[[movies]]
		priority = 1
		pp = 3
		name = movies
		script = $scriptName
		newzbin = ""
		order = 0
		dir = "movies"
	CATEGORY5
)
_appendLine "$appConfigPath" "\[categories\]" "$newCategory"

_renameExistingKey "$appConfigPath" "\[\[tv\]\]" "\[\[tv-OLD\]\]"
newCategory=$(cat <<-CATEGORY6
		[[tv]]
		priority = 2
		pp = 3
		name = tv
		script = nzbToSickBeard.py
		newzbin = ""
		order = 0
		dir = "tv"
	CATEGORY6
)
_appendLine "$appConfigPath" "\[categories\]" "$newCategory"

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config || true"
_restartService "$appServiceName" "$jail_name"

# Get new app's api key from it's configuration and add it to common-config.sh
api_key=$(_getProperty "$appConfigPath" "api_key" "\[misc\]")
if [ ${#api_key} -gt 0 ]; then
	_replaceConfig "$(dirname "$0")/../common-config.sh"		 "#!\/bin\/bash"		 "^[[:space:]]*sabnzbd_api_key="		 "=\".*\""		 "=\"$api_key\"";
	_updateApiKeys
fi

_checkSetupResults "" "$appName" ""

_log 8 "+" "Setting up application ($appName) configuration" "COMPLETED" 1;

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2;

# Restore standard redirection
exec &>/dev/tty
