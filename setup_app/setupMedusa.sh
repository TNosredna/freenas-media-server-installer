#!/bin/bash
log_enabled="$1"

appName="medusa"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-setup.log"
log_err_file_path="$HOME/$appName-setup.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="medusa"
appServiceName="medusa"
applicationPort="$medusa_jail_port"
appURI="$medusa_web_root"
appConfigPath="/mnt/$zpool_name/apps/$appName/config.ini"

jailName_var=$appName\_jail_name
jail_name=${!jailName_var}
jail_ip_var=$appName\_jail_ip
jail_ip=${!jail_ip_var}
jail_mac_var=$appName\_jail_mac
jail_mac=${!jail_mac_var}
openvpn_enabled_var=$appName\_openvpn_enabled
openvpn_enabled=${!openvpn_enabled_var}
ipfw_enabled_var=$appName\_ipfw_enabled
ipfw_enabled=${!ipfw_enabled_var}

_log 8 "/" "Setting up application ($appName) configuration" "STARTING" 1;
_stopService "$appServiceName" "$jail_name"

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config || true"
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*airdate_episodes ="							"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*auto_update ="									"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*calendar_icons ="								"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*calendar_unprotected ="					"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*create_missing_show_dirs ="			"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*dbdebug ="											"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*debug ="												"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*del_rar_contents ="							"=.*$"		"= 0";	#=1
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*ep_default_deleted_status ="		"=.*$"		"= 5";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*fallback_plex_notifications ="	"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*git_username ="									"=.*$"		"= $git_user_name";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*git_password ="									"=.*$"		"= $git_user_password";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*handle_reverse_proxy ="					"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*ignore_und_subs ="							"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*ignore_words ="									"=.*$"		"= german, french, core2hd, dutch, swedish, reenc, MrLss, dubbed, spanish, chinese, japan, korean";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*ignored_subs_list ="						"=.*$"		"= dk, fin, heb, kor, nor, nordic, pl, swe, spa";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*indexer_default ="							"=.*$"		"= 0";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*keep_processed_dir ="						"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*launch_browser ="								"=.*$"		"= 0";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*metadata_kodi ="								"=.*$"		"= 1, 1, 1, 1, 1, 1, 1, 1, 1, 1";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*move_associated_files ="				"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*naming_pattern ="								"=.*$"		"= $jail_media_folder_tv/%SN/Season %0S/%SN - S%0SE%0E - %EN";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*no_delete ="										"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*nzb_method ="										"=.*$"		"= sabnzbd";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*privacy_level ="								"=.*$"		"= low";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*process_method ="								"=.*$"		"= move"; #=copy
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*provider_order ="								"=.*$"		"= nzb_su, nzbgeek, rarbg, binsearch, avistaz, shanaproject, torrentz2, cinemaz, hdbits, abnormal, nzbs_org, alpharatio, scenetime, animebytes, btn, sdbits, hdspace, torrentbytes, torrenting, animetorrents, speedcd, xthor, shazbat_tv, bithdtv, privatehd, yggtorrent, bj_share, morethantv, tvchaosuk, omgwtfnzbs, iptorrents, danishbits, anizb, nyaa, torrentday, pretome, btdb, elitetracker, usenet_crawler, tokyotoshokan, hdtorrents, thepiratebay, bitcannon, nebulance, dognzb, nzb_cat, archetorrent, torrentleech, anidex, limetorrents, norbits, hebits, zooqle, nordicbits, ncore, gimmepeers, beyond_hd";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*proxy_indexers ="								"=.*$"		"= 0";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*quality_default ="							"=.*$"		"= 3022846";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*remove_from_client ="						"=.*$"		"= 0";	#=1
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*rename_episodes ="							"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*root_dirs ="										"=.*$"		"= 0, $jail_media_folder_tv";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*showupdate_hour ="							"=.*$"		"= 2";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*ssl_verify ="										"=.*$"		"= 0";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*status_default ="								"=.*$"		"= 3";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*torrent_method ="								"=.*$"		"= deluged";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*tv_download_dir ="							"=.*$"		"= $download_incomplete_process_tv";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*unpack ="												"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*use_nzbs ="											"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*usenet_retention ="							"=.*$"		"= $usenet_provider_retention";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*web_port ="											"=.*$"		"= $applicationPort";
_replaceConfig "$appConfigPath"			"\[General\]"			"^[[:space:]]*web_log ="											"=.*$"		"= 1";

_replaceConfig "$appConfigPath"			"\[NZBGEEK\]"			"^[[:space:]]*nzbgeek ="											"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[NZBGEEK\]"			"^[[:space:]]*nzbgeek_name ="									"=.*$"		"= NZBGeek";
_replaceConfig "$appConfigPath"			"\[NZBGEEK\]"			"^[[:space:]]*nzbgeek_url ="									"=.*$"		"= https://api.nzbgeek.info/";
_replaceConfig "$appConfigPath"			"\[NZBGEEK\]"			"^[[:space:]]*nzbgeek_cat_ids ="							"=.*$"		"= 5000, 5030, 5040, 5045, 5070";
_replaceConfig "$appConfigPath"			"\[NZBGEEK\]"			"^[[:space:]]*nzbgeek_api_key ="							"=.*$"		"= $nzbgeek_api_key";
_replaceConfig "$appConfigPath"			"\[NZBGEEK\]"			"^[[:space:]]*nzbgeek_search_mode ="					"=.*$"		"= eponly";
_replaceConfig "$appConfigPath"			"\[NZBGEEK\]"			"^[[:space:]]*nzbgeek_search_fallback ="			"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[NZBGEEK\]"			"^[[:space:]]*nzbgeek_enable_daily ="					"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[NZBGEEK\]"			"^[[:space:]]*nzbgeek_enable_backlog ="				"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[NZBGEEK\]"			"^[[:space:]]*nzbgeek_enable_manualsearch ="	"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[NZBGEEK\]"			"^[[:space:]]*nzbgeek_enable_search_delay ="	"=.*$"		"= 0";
_replaceConfig "$appConfigPath"			"\[NZBGEEK\]"			"^[[:space:]]*nzbgeek_search_delay ="					"=.*$"		"= 60";

_replaceConfig "$appConfigPath"			"\[RARBG\]"		 		"^[[:space:]]*rarbg ="												"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[RARBG\]"		 		"^[[:space:]]*rarbg_search_fallback ="				"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[RARBG\]"		 		"^[[:space:]]*rarbg_search_delay ="						"=.*$"		"= 60";

_replaceConfig "$appConfigPath"			"\[BINSEARCH\]"		"^[[:space:]]*binsearch ="										"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[BINSEARCH\]"		"^[[:space:]]*binsearch_search_fallback ="		"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[BINSEARCH\]"		"^[[:space:]]*binsearch_search_delay ="				"=.*$"		"= 60";

_replaceConfig "$appConfigPath"			"\[SABnzbd\]"			"^[[:space:]]*sab_apikey ="										"=.*$"		"= $sabnzbd_api_key";
_replaceConfig "$appConfigPath"			"\[SABnzbd\]"			"^[[:space:]]*sab_host ="											"=.*$"		"= http://$sabnzbd_jail_ip:$sabnzbd_jail_port";
_replaceConfig "$appConfigPath"			"\[SABnzbd\]"			"^[[:space:]]*sab_forced ="										"=.*$"		"= 1";

_replaceConfig "$appConfigPath"			"\[TORRENT\]"			"^[[:space:]]*torrent_username ="							"=.*$"		"= $media_user_name";
_replaceConfig "$appConfigPath"			"\[TORRENT\]"			"^[[:space:]]*torrent_password ="							"=.*$"		"= $media_user_password";
_replaceConfig "$appConfigPath"			"\[TORRENT\]"			"^[[:space:]]*torrent_host ="									"=.*$"		"= scgi://$deluge_jail_ip:$deluge_jail_port";
_replaceConfig "$appConfigPath"			"\[TORRENT\]"			"^[[:space:]]*torrent_path ="									"=.*$"		"= $download_incomplete_torrent";
_replaceConfig "$appConfigPath"			"\[TORRENT\]"			"^[[:space:]]*torrent_label ="								"=.*$"		"= tv";
_replaceConfig "$appConfigPath"			"\[TORRENT\]"			"^[[:space:]]*torrent_label_anime ="					"=.*$"		"= anime";
_replaceConfig "$appConfigPath"			"\[TORRENT\]"			"^[[:space:]]*torrent_seed_location ="				"=.*$"		"= $download_incomplete_process_tv";	#=/mnt/download/incomplete/process

_replaceConfig "$appConfigPath"			"\[Plex\]"				"^[[:space:]]*use_plex_server ="							"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[Plex\]"				"^[[:space:]]*plex_update_library ="					"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[Plex\]"				"^[[:space:]]*plex_server_host ="							"=.*$"		"= $plex_jail_ip:$plex_jail_port";
_replaceConfig "$appConfigPath"			"\[Plex\]"				"^[[:space:]]*plex_server_token ="						"=.*$"		"= $plex_online_auth_token";

_replaceConfig "$appConfigPath"			"\[Newznab\]"			"^[[:space:]]*newznab_providers ="						"=.*$"		"= NZB.su,";

_replaceConfig "$appConfigPath"			"\[GUI\]"					"^[[:space:]]*home_layout ="									"=.*$"		"= simple";
_replaceConfig "$appConfigPath"			"\[GUI\]"					"^[[:space:]]*display_show_specials ="				"=.*$"		"= 0";
_replaceConfig "$appConfigPath"			"\[GUI\]"					"^[[:space:]]*fuzzy_dating ="									"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[GUI\]"					"^[[:space:]]*trim_zero ="										"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[GUI\]"					"^[[:space:]]*layout_wide ="									"=.*$"		"= 1";

_replaceConfig "$appConfigPath"			"\[Email\]"				"^[[:space:]]*use_email ="										"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[Email\]"				"^[[:space:]]*email_host ="										"=.*$"		"= smtp.gmail.com";
_replaceConfig "$appConfigPath"			"\[Email\]"				"^[[:space:]]*email_port ="										"=.*$"		"= 587";
_replaceConfig "$appConfigPath"			"\[Email\]"				"^[[:space:]]*email_tls ="										"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[Email\]"				"^[[:space:]]*email_user ="										"=.*$"		"= $notify_gmail_address";
_replaceConfig "$appConfigPath"			"\[Email\]"				"^[[:space:]]*email_password ="								"=.*$"		"= $notify_gmail_password";
_replaceConfig "$appConfigPath"			"\[Email\]"				"^[[:space:]]*email_from ="										"=.*$"		"= $notify_gmail_address";

_replaceConfig "$appConfigPath"			"\[FailedDownloads\]"	"^[[:space:]]*use_failed_downloads ="		 	"= .*$"		"= 0"; #=1
_replaceConfig "$appConfigPath"			"\[FailedDownloads\]"	"^[[:space:]]*delete_failed ="						"= .*$"		"= 0"; #=1

grepResults=""
grepResults="$(_getGrepResults '\[NZB_SU\]' $appConfigPath)";
if [ ${#grepResults} -eq 0 ]; then
	echo "[NZB_SU]" >> $appConfigPath
	echo "nzb_su = 1" >> $appConfigPath
	echo "nzb_su_name = NZB.su" >> $appConfigPath
	echo "nzb_su_url = http://api.nzb.su/" >> $appConfigPath
	echo "nzb_su_cat_ids = 5000, 5040, 5030, 5045" >> $appConfigPath
	echo "nzb_su_api_key = $nzbsu_api_key" >> $appConfigPath
	echo "nzb_su_search_mode = eponly" >> $appConfigPath
	echo "nzb_su_search_fallback = 1" >> $appConfigPath
	echo "nzb_su_enable_daily = 1" >> $appConfigPath
	echo "nzb_su_enable_backlog = 1" >> $appConfigPath
	echo "nzb_su_enable_manualsearch = 1" >> $appConfigPath
	echo "nzb_su_enable_search_delay = 0" >> $appConfigPath
	echo "nzb_su_search_delay = 60" >> $appConfigPath
fi
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config || true"

_restartService "$appServiceName" "$jail_name"

# Get new app's api key from it's configuration and add it to common-config.sh
api_key=$(_getProperty "$appConfigPath" "api_key" "\[General\]")
if [ ${#api_key} -gt 0 ]; then
	_replaceConfig "$(dirname "$0")/../common-config.sh"		 "#!\/bin\/bash"		 "^[[:space:]]*medusa_api_key="		 "=\".*\""		 "=\"$api_key\"";
	_updateApiKeys
fi

_checkSetupResults "" "$appName" ""

_log 8 "+" "Setting up application ($appName) configuration" "COMPLETED" 1;

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2;

# Restore standard redirection
exec &>/dev/tty
