#!/bin/bash
log_enabled="$1"

appName="deluge"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-setup.log"
log_err_file_path="$HOME/$appName-setup.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="deluge"
appServiceName="deluged"
appWebServiceName="deluge_web"
applicationPort="$deluge_jail_port"
appURI="$deluge_web_root"
appConfigPath_auth="/mnt/$zpool_name/apps/$appName/auth"
appConfigPath_web_auth="/mnt/$zpool_name/apps/$appName/auth"
appConfigPath_core="/mnt/$zpool_name/apps/$appName/core.conf"
appConfigPath_web="/mnt/$zpool_name/apps/$appName/web.conf"
appConfigPath_hostlist="/mnt/$zpool_name/apps/$appName/hostlist.conf.1.2"
appConfigPath_label="/mnt/$zpool_name/apps/$appName/label.conf"
appConfigPath_extractor="/mnt/$zpool_name/apps/$appName/extractor.conf"
appConfigPath_execute="/mnt/$zpool_name/apps/$appName/execute.conf"

jailName_var=$appName\_jail_name
jail_name=${!jailName_var}
jail_ip_var=$appName\_jail_ip
jail_ip=${!jail_ip_var}
jail_mac_var=$appName\_jail_mac
jail_mac=${!jail_mac_var}
openvpn_enabled_var=$appName\_openvpn_enabled
openvpn_enabled=${!openvpn_enabled_var}
ipfw_enabled_var=$appName\_ipfw_enabled
ipfw_enabled=${!ipfw_enabled_var}

_log 8 "/" "Setting up application ($appName) configuration" "STARTING" 1;
_stopService "$appWebServiceName" "$jail_name"
_stopService "$appServiceName" "$jail_name"

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config || true"
_replaceConfig "$appConfigPath_core"		"\"format\":"			"^[[:space:]]*\"allow_remote\":"									":.*$"		": true,";
_replaceConfig "$appConfigPath_core"		"\"format\":"			"^[[:space:]]*\"auto_managed\":"									":.*$"		": false,";
_replaceConfig "$appConfigPath_core"		"\"format\":"			"^[[:space:]]*\"dont_count_slow_torrents\":"			":.*$"		": true,";
_replaceConfig "$appConfigPath_core"		"\"format\":"			"^[[:space:]]*\"download_location\":"							":.*$"		": \"$download_incomplete_torrent\",";	 #=/mnt/download/incomplete/torrent
_replaceConfig "$appConfigPath_core"		"\"format\":"			"^[[:space:]]*\"enabled_plugins\":"								":.*$"		": \[ \"Execute\", \"Extractor\", \"Label\" \],";
_replaceConfig "$appConfigPath_core"		"\"format\":"			"^[[:space:]]*\"ignore_limits_on_local_network\":" ":.*$"		": false,";
_replaceConfig "$appConfigPath_core"		"\"format\":"			"^[[:space:]]*\"listen_interface\":"							":[[:space:]]*\".*\""		": \"0.0.0.0\"";
_replaceConfig "$appConfigPath_core"    "\"format\":"     "\"max_active_downloading\":"       							":.*$"    ": 15,";	# 3(<400kbs) 4(400-1200kbs) 10(1200-2000kbs) 15(>2000kbs)
_replaceConfig "$appConfigPath_core"    "\"format\":"     "\"max_active_limit\":"       										":.*$"    ": 15,";	# 3(<400kbs) 5(400-1200kbs) 10(1200-2000kbs) 15(>2000kbs)
_replaceConfig "$appConfigPath_core"    "\"format\":"     "\"max_active_seeding\":"           							":.*$"    ": 15,";	# 3(<400kbs) 5(400-1200kbs) 10(1200-2000kbs) 15(>2000kbs)
_replaceConfig "$appConfigPath_core"    "\"format\":"     "\"max_connections_global\":"  										":.*$"    ": 250,";	# 250
_replaceConfig "$appConfigPath_core"    "\"format\":"     "\"max_connections_per_second\":"  								":.*$"    ": 20,";	# 20
_replaceConfig "$appConfigPath_core"    "\"format\":"     "\"max_connections_per_torrent\":"  							":.*$"    ": 120,";	# 30(<400kbs) 50(400-1200kbs) 80(1200-2000kbs) 120(>2000kbs)
_replaceConfig "$appConfigPath_core"    "\"format\":"     "\"max_download_speed\":"  												":.*$"    ": 34035,"; # 80-95% of tested network download speed
_replaceConfig "$appConfigPath_core"    "\"format\":"     "\"max_download_speed_per_torrent\":"							":.*$"    ": -1,";	# -1
_replaceConfig "$appConfigPath_core"    "\"format\":"     "\"max_half_open_connections\":"									":.*$"    ": 25,"; # 10-50
_replaceConfig "$appConfigPath_core"    "\"format\":"     "\"max_upload_speed\":"  													":.*$"    ": 29817,";	 # 80% of tested network upload speed
_replaceConfig "$appConfigPath_core"    "\"format\":"     "\"max_upload_speed_per_torrent\":"  							":.*$"    ": -1,";	# -1
_replaceConfig "$appConfigPath_core"    "\"format\":"     "\"max_upload_slots_global\":" 										":.*$"    ": 4,";	#! 4
_replaceConfig "$appConfigPath_core"    "\"format\":"     "\"max_upload_slots_per_torrent\":" 							":.*$"    ": 8,";	# 4(<400kbs) 5(400-1200kbs) 7(1200-2000kbs) 8(>2000kbs)
_replaceConfig "$appConfigPath_core"		"\"format\":"			"^[[:space:]]*\"move_completed\":"								":.*$"		": true,";
_replaceConfig "$appConfigPath_core"		"\"format\":"			"^[[:space:]]*\"move_completed_path\":"						":.*$"		": \"$download_incomplete_process\",";		#=/mnt/download/incomplete/process
_replaceConfig "$appConfigPath_core"		"\"format\":"			"^[[:space:]]*\"new_release_check\":"							":.*$"		": true,";
_replaceConfig "$appConfigPath_core"		"\"format\":"			"^[[:space:]]*\"queue_new_to_top\":"							":.*$"		": true,";
_replaceConfig "$appConfigPath_core"		"\"format\":"			"^[[:space:]]*\"plugins_location\":"							":.*$"		": \"$jail_config/plugins\",";	 #=/config/plugins
_replaceConfig "$appConfigPath_core"		"\"format\":"			"^[[:space:]]*\"remove_seed_at_ratio\":"					":.*$"		": true,";
_replaceConfig "$appConfigPath_core"    "\"format\":"     "\"seed_time_limit\":"              							":.*$"    ": 1,";
_replaceConfig "$appConfigPath_core"		"\"format\":"			"^[[:space:]]*\"seed_time_ratio_limit\":"					":.*$"		": 0.001,";
_replaceConfig "$appConfigPath_core"    "\"format\":"     "\"share_ratio_limit\":"            							":.*$"    ": 0.001,";
_replaceConfig "$appConfigPath_core"		"\"format\":"			"^[[:space:]]*\"stop_seed_at_ratio\":"						":.*$"		": true,";
_replaceConfig "$appConfigPath_core"		"\"format\":"			"^[[:space:]]*\"stop_seed_ratio\":"								":.*$"		": 0.001,";
_replaceConfig "$appConfigPath_core"		"\"format\":"			"^[[:space:]]*\"torrentfiles_location\":"					":.*$"		": \"$download\",";		#=/Downloads

authHasUser=$(cat "$appConfigPath_auth" | grep "$media_user_name:$media_user_password")
if [ ${#authHasUser} -eq 0 ]; then echo "$media_user_name:$media_user_password:10" > "$appConfigPath_auth"; fi

isMediaUserPresent=$(cat "$appConfigPath_hostlist" | grep "$media_user_name")
if [ ${#isMediaUserPresent} -eq 0 ]; then
_replaceConfig "$appConfigPath_hostlist"			"\"hosts\":"			"^[[:space:]]*\"127.0.0.1\""			"\"127.0.0.1\",[[:space:]]*$"				"\"0.0.0.0\",";
#_replaceConfig "$appConfigPath_hostlist"			"\"hosts\":"			"^[[:space:]]*58846,"								"^[[:space:]]*58846[[:space:]]*$"			"58846,";
_replaceConfig "$appConfigPath_hostlist"			"\"hosts\":"			"^[[:space:]]*\"localclient\","			"\"localclient\",[[:space:]]*$"				"\"$media_user_name\",";
_replaceConfig "$appConfigPath_hostlist"			"\"hosts\":"			"^[[:space:]]*\".*\"[[:space:]]*$"	"\".*\"[[:space:]]*$"									"\"$media_user_password\"";
fi

pwd_salt=$(awk -F ":" '/pwd_salt/ {print $2}' "$appConfigPath_web")
pwd_salt="${pwd_salt%\", }"
pwd_salt="${pwd_salt%\",}"
pwd_salt="${pwd_salt# \"}"
pwd_sha1=$(echo -n "$pwd_salt$media_user_password" | openssl dgst -sha1 |awk '{print $2}')
daemon_id=$(awk -v RS='"' 'NR==8 {print $1}' "$appConfigPath_hostlist");
if [ ${#daemon_id} -gt 0 ]; then _replaceConfig "$appConfigPath_web"	"\"format\":"	"^[[:space:]]*\"default_daemon\":"	":.*$"	": \"$daemon_id\","; fi
_replaceConfig "$appConfigPath_web"		"\"format\":"							"^[[:space:]]*\"enabled_plugins\":"						":.*$"		": \[ \"Execute\", \"Extractor\", \"Label\" \],";
_replaceConfig "$appConfigPath_web"		"\"format\":"							"^[[:space:]]*\"first_login\":"								":.*$"		": false,";
_replaceConfig "$appConfigPath_web"		"\"format\":"							"^[[:space:]]*\"https\":"											":.*$"		": false,";
_replaceConfig "$appConfigPath_web"		"\"format\":"							"^[[:space:]]*\"interface\":"									":.*$"		": \"0.0.0.0\",";
_replaceConfig "$appConfigPath_web"		"\"format\":"							"^[[:space:]]*\"pkey\":"											":.*$"		": \"ssl/daemon.pkey\",";
_replaceConfig "$appConfigPath_web"		"\"format\":"							"^[[:space:]]*\"pwd_sha1\":"									":.*$"		": \"$pwd_sha1\",";
_replaceConfig "$appConfigPath_web"		"\"format\":"							"^[[:space:]]*\"show_session_speed\":"				":.*$"		": true,";

cp -f $(dirname "$0")/$appName/label.conf $appConfigPath_label;
_replaceConfig "$appConfigPath_label"		"\"tv\":"								"^[[:space:]]*\"apply_max\":"									":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"tv\":"								"^[[:space:]]*\"apply_move_completed\":"			":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"tv\":"								"^[[:space:]]*\"apply_queue\":"								":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"tv\":"								"^[[:space:]]*\"auto_add\":"									":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"tv\":"								"^[[:space:]]*\"auto_add_trackers\":"					":.*$"		": \[\],";
_replaceConfig "$appConfigPath_label"		"\"tv\":"								"^[[:space:]]*\"is_auto_managed\":"						":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"tv\":"								"^[[:space:]]*\"max_connections\":"						":.*$"		": 20,";
_replaceConfig "$appConfigPath_label"		"\"tv\":"								"^[[:space:]]*\"max_download_speed\":"				":.*$"		": -1,";
_replaceConfig "$appConfigPath_label"		"\"tv\":"								"^[[:space:]]*\"max_upload_slots\":"					":.*$"		": 8,";
_replaceConfig "$appConfigPath_label"		"\"tv\":"								"^[[:space:]]*\"max_upload_speed\":"					":.*$"		": -1,";
_replaceConfig "$appConfigPath_label"		"\"tv\":"								"^[[:space:]]*\"move_completed\":"						":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"tv\":"								"^[[:space:]]*\"move_completed_path\":"				":.*$"		": \"$download_incomplete_process_tv\",";
_replaceConfig "$appConfigPath_label"		"\"tv\":"								"^[[:space:]]*\"remove_at_ratio\":"						":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"tv\":"								"^[[:space:]]*\"stop_at_ratio\":"							":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"tv\":"								"^[[:space:]]*\"stop_ratio\":"								":.*$"		": 0.01";

_replaceConfig "$appConfigPath_label"		"\"music\":"						"^[[:space:]]*\"apply_max\":"									":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"music\":"						"^[[:space:]]*\"apply_move_completed\":"			":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"music\":"						"^[[:space:]]*\"apply_queue\":"								":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"music\":"						"^[[:space:]]*\"auto_add\":"									":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"music\":"						"^[[:space:]]*\"auto_add_trackers\":"					":.*$"		": \[\],";
_replaceConfig "$appConfigPath_label"		"\"music\":"						"^[[:space:]]*\"is_auto_managed\":"						":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"music\":"						"^[[:space:]]*\"max_connections\":"						":.*$"		": 20,";
_replaceConfig "$appConfigPath_label"		"\"music\":"						"^[[:space:]]*\"max_download_speed\":"				":.*$"		": -1,";
_replaceConfig "$appConfigPath_label"		"\"music\":"						"^[[:space:]]*\"max_upload_slots\":"					":.*$"		": 8,";
_replaceConfig "$appConfigPath_label"		"\"music\":"						"^[[:space:]]*\"max_upload_speed\":"					":.*$"		": -1,";
_replaceConfig "$appConfigPath_label"		"\"music\":"						"^[[:space:]]*\"move_completed\":"						":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"music\":"						"^[[:space:]]*\"move_completed_path\":"				":.*$"		": \"$download_incomplete_process_music\",";
_replaceConfig "$appConfigPath_label"		"\"music\":"						"^[[:space:]]*\"remove_at_ratio\":"						":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"music\":"						"^[[:space:]]*\"stop_at_ratio\":"							":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"music\":"						"^[[:space:]]*\"stop_ratio\":"								":.*$"		": 0.01";

_replaceConfig "$appConfigPath_label"		"\"movies\":"						"^[[:space:]]*\"apply_max\":"									":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"movies\":"						"^[[:space:]]*\"apply_move_completed\":"			":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"movies\":"						"^[[:space:]]*\"apply_queue\":"								":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"movies\":"						"^[[:space:]]*\"auto_add\":"									":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"movies\":"						"^[[:space:]]*\"auto_add_trackers\":"					":.*$"		": \[\],";
_replaceConfig "$appConfigPath_label"		"\"movies\":"						"^[[:space:]]*\"is_auto_managed\":"						":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"movies\":"						"^[[:space:]]*\"max_connections\":"						":.*$"		": 20,";
_replaceConfig "$appConfigPath_label"		"\"movies\":"						"^[[:space:]]*\"max_download_speed\":"				":.*$"		": -1,";
_replaceConfig "$appConfigPath_label"		"\"movies\":"						"^[[:space:]]*\"max_upload_slots\":"					":.*$"		": 8,";
_replaceConfig "$appConfigPath_label"		"\"movies\":"						"^[[:space:]]*\"max_upload_speed\":"					":.*$"		": -1,";
_replaceConfig "$appConfigPath_label"		"\"movies\":"						"^[[:space:]]*\"move_completed\":"						":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"movies\":"						"^[[:space:]]*\"move_completed_path\":"				":.*$"		": \"$download_incomplete_process_movies\",";
_replaceConfig "$appConfigPath_label"		"\"movies\":"						"^[[:space:]]*\"remove_at_ratio\":"						":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"movies\":"						"^[[:space:]]*\"stop_at_ratio\":"							":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"movies\":"						"^[[:space:]]*\"stop_ratio\":"								":.*$"		": 0.01";

_replaceConfig "$appConfigPath_label"		"\"audiobooks\":"				"^[[:space:]]*\"apply_max\":"									":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"audiobooks\":"				"^[[:space:]]*\"apply_move_completed\":"			":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"audiobooks\":"				"^[[:space:]]*\"apply_queue\":"								":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"audiobooks\":"				"^[[:space:]]*\"auto_add\":"									":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"audiobooks\":"				"^[[:space:]]*\"auto_add_trackers\":"					":.*$"		": \[\],";
_replaceConfig "$appConfigPath_label"		"\"audiobooks\":"				"^[[:space:]]*\"is_auto_managed\":"						":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"audiobooks\":"				"^[[:space:]]*\"max_connections\":"						":.*$"		": 20,";
_replaceConfig "$appConfigPath_label"		"\"audiobooks\":"				"^[[:space:]]*\"max_download_speed\":"				":.*$"		": -1,";
_replaceConfig "$appConfigPath_label"		"\"audiobooks\":"				"^[[:space:]]*\"max_upload_slots\":"					":.*$"		": 8,";
_replaceConfig "$appConfigPath_label"		"\"audiobooks\":"				"^[[:space:]]*\"max_upload_speed\":"					":.*$"		": -1,";
_replaceConfig "$appConfigPath_label"		"\"audiobooks\":"				"^[[:space:]]*\"move_completed\":"						":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"audiobooks\":"				"^[[:space:]]*\"move_completed_path\":"				":.*$"		": \"$download_incomplete_process_audiobooks\",";
_replaceConfig "$appConfigPath_label"		"\"audiobooks\":"				"^[[:space:]]*\"remove_at_ratio\":"						":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"audiobooks\":"				"^[[:space:]]*\"prioritize_first_last\":"			":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"audiobooks\":"				"^[[:space:]]*\"stop_at_ratio\":"							":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"audiobooks\":"				"^[[:space:]]*\"stop_ratio\":"								":.*$"		": 0.01";

_replaceConfig "$appConfigPath_label"		"\"ebooks\":"						"^[[:space:]]*\"apply_max\":"									":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"ebooks\":"						"^[[:space:]]*\"apply_move_completed\":"			":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"ebooks\":"						"^[[:space:]]*\"apply_queue\":"								":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"ebooks\":"						"^[[:space:]]*\"auto_add\":"									":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"ebooks\":"						"^[[:space:]]*\"auto_add_trackers\":"					":.*$"		": \[\],";
_replaceConfig "$appConfigPath_label"		"\"ebooks\":"						"^[[:space:]]*\"is_auto_managed\":"						":.*$"		": false,";
_replaceConfig "$appConfigPath_label"		"\"ebooks\":"						"^[[:space:]]*\"max_connections\":"						":.*$"		": 20,";
_replaceConfig "$appConfigPath_label"		"\"ebooks\":"						"^[[:space:]]*\"max_download_speed\":"				":.*$"		": -1,";
_replaceConfig "$appConfigPath_label"		"\"ebooks\":"						"^[[:space:]]*\"max_upload_slots\":"					":.*$"		": 8,";
_replaceConfig "$appConfigPath_label"		"\"ebooks\":"						"^[[:space:]]*\"max_upload_speed\":"					":.*$"		": -1,";
_replaceConfig "$appConfigPath_label"		"\"ebooks\":"						"^[[:space:]]*\"move_completed\":"						":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"ebooks\":"						"^[[:space:]]*\"move_completed_path\":"				":.*$"		": \"$download_incomplete_process_ebooks\",";
_replaceConfig "$appConfigPath_label"		"\"ebooks\":"						"^[[:space:]]*\"remove_at_ratio\":"						":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"ebooks\":"						"^[[:space:]]*\"stop_at_ratio\":"							":.*$"		": true,";
_replaceConfig "$appConfigPath_label"		"\"ebooks\":"						"^[[:space:]]*\"stop_ratio\":"								":.*$"		": 0.01";

if [ ! -f "$appConfigPath_extractor" ]; then cp $(dirname "$0")/$appName/extractor.conf $appConfigPath_extractor; fi
_replaceConfig "$appConfigPath_extractor"			"\"format\":"			"^[[:space:]]*\"extract_path\":"							":.*$"		": \"$download_incomplete_torrent\"";

if [ ! -f "$appConfigPath_execute" ]; then cp $(dirname "$0")/$appName/execute.conf $appConfigPath_execute; fi
_replaceConfig "$appConfigPath_execute"			"\"format\":"				"^[[:space:]]*\"commands\":"									"\[.*$"		"\[ \[ \"1b62ad01d766804e2bf2127f128386c4a5fd2bb7\", \"complete\", \"$jail_nzbtomedia/TorrentToMedia.py\" \] \]";

ln -s /mnt/$zpool_name/apps/$appName/$jail_name.key.pem /mnt/$zpool_name/apps/$appName/ssl/$jail_name.key.pem
ln -s /mnt/$zpool_name/apps/$appName/$jail_name.cert.pem /mnt/$zpool_name/apps/$appName/ssl/$jail_name.cert.pem

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config || true"

_restartService "$appServiceName" "$jail_name"
_restartService "$appWebServiceName" "$jail_name"

_checkSetupResults "" "$appName" ""

_log 8 "+" "Setting up application ($appName) configuration" "COMPLETED" 1;

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2;

# Restore standard redirection
exec &>/dev/tty
