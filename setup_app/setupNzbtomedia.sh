#!/bin/bash
log_enabled="$1"

appName="nzbtomedia"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-setup.log"
log_err_file_path="$HOME/$appName-setup.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="nzbtomedia"
appServiceName="nzbtomedia"
appConfigPath="/mnt/$zpool_name/apps/$appName/autoProcessMedia.cfg"

_log 8 "/" "Setting up application ($appName) configuration" "STARTING" 1;

chown -R $media_user_name:$media_group_name "/mnt/$zpool_name/apps/nzbtomedia"
_replaceConfig "$appConfigPath"			"\[General\]"						"^[[:space:]]*auto_update ="		 						"=.*$"			"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"						"^[[:space:]]*check_media ="		 						"=.*$"			"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"						"^[[:space:]]*ffmpeg_path ="		 						"=.*$"			"= /usr/local/bin";
_replaceConfig "$appConfigPath"			"\[General\]"						"^[[:space:]]*force_clean ="		 						"=.*$"			"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"						"^[[:space:]]*git_path ="										"=.*$"			"= /usr/local/bin/git";
_replaceConfig "$appConfigPath"			"\[General\]"						"^[[:space:]]*git_user ="										"=.*$"			"= $git_user_name";
_replaceConfig "$appConfigPath"			"\[General\]"						"^[[:space:]]*git_branch ="									"=.*$"			"= master";
_replaceConfig "$appConfigPath"			"\[General\]"						"^[[:space:]]*log_debug ="									"=.*$"			"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"						"^[[:space:]]*log_db ="											"=.*$"			"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"						"^[[:space:]]*log_env ="										"=.*$"			"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"						"^[[:space:]]*safe_mode ="									"=.*$"			"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"						"^[[:space:]]*sys_path ="										"=.*$"			"= /usr/local/bin";

if [ "$couchpotato_enabled" == "YES" ]; then _replaceConfig "$appConfigPath" "\[CouchPotato\]" "^[[:space:]]*enabled =" "=.*$" "= 1"; fi
_replaceConfig "$appConfigPath"			"\[CouchPotato\]"				"^[[:space:]]*\[\[movie\]\]"								"\[\[.*$"		"\[\[movies\]\]";
_replaceConfig "$appConfigPath"			"\[CouchPotato\]"				"^[[:space:]]*apikey ="											"=.*$"			"= $couchpotato_api_key";
_replaceConfig "$appConfigPath"			"\[CouchPotato\]"				"^[[:space:]]*chmodDirectory ="							"=.*$"			"= 777";
_replaceConfig "$appConfigPath"			"\[CouchPotato\]"				"^[[:space:]]*host ="												"=.*$"			"= $couchpotato_jail_ip";
_replaceConfig "$appConfigPath"			"\[CouchPotato\]"				"^[[:space:]]*method ="											"=.*$"			"= renamer";
_replaceConfig "$appConfigPath"			"\[CouchPotato\]"				"^[[:space:]]*omdbapikey ="									"=.*$"			"= $omdb_api_key";
_replaceConfig "$appConfigPath"			"\[CouchPotato\]"				"^[[:space:]]*port ="												"=.*$"			"= $couchpotato_jail_port";
_replaceConfig "$appConfigPath"			"\[CouchPotato\]"				"^[[:space:]]*remote_path ="								"=.*$"			"= 0";
_replaceConfig "$appConfigPath"			"\[CouchPotato\]"				"^[[:space:]]*watch_dir ="									"=.*$"			"= $download_incomplete_process_movies";
_replaceConfig "$appConfigPath"			"\[CouchPotato\]"				"^[[:space:]]*web_root ="										"=.*$"			"= $couchpotato_web_root";

if [ "$radarr_enabled" == "YES" ]; then _replaceConfig "$appConfigPath" "\[Radarr\]" "^[[:space:]]*enabled =" "=.*$" "= 1"; fi
_replaceConfig "$appConfigPath"			"\[Radarr\]"						"^[[:space:]]*\[\[movie\]\]"								"\[\[.*$"		"\[\[movies\]\]";
_replaceConfig "$appConfigPath"			"\[Radarr\]"						"^[[:space:]]*apikey ="											"=.*$"			"= $radarr_api_key";
_replaceConfig "$appConfigPath"			"\[Radarr\]"						"^[[:space:]]*delete_failed ="							"=.*$"			"= 1";
_replaceConfig "$appConfigPath"			"\[Radarr\]"						"^[[:space:]]*delete_ignored ="							"=.*$"			"= 1";
_replaceConfig "$appConfigPath"			"\[Radarr\]"						"^[[:space:]]*extract ="										"=.*$"			"= 1";
_replaceConfig "$appConfigPath"			"\[Radarr\]"						"^[[:space:]]*host ="												"=.*$"			"= $radarr_jail_ip";
_replaceConfig "$appConfigPath"			"\[Radarr\]"						"^[[:space:]]*importMode ="									"=.*$"			"= Move";
_replaceConfig "$appConfigPath"			"\[Radarr\]"						"^[[:space:]]*keep_archive ="								"=.*$"			"= 0";
_replaceConfig "$appConfigPath"			"\[Radarr\]"						"^[[:space:]]*omdbapikey ="									"=.*$"			"= $omdb_api_key";
_replaceConfig "$appConfigPath"			"\[Radarr\]"						"^[[:space:]]*port ="												"=.*$"			"= $radarr_jail_port";
_replaceConfig "$appConfigPath"			"\[Radarr\]"						"^[[:space:]]*remote_path ="								"=.*$"			"= 0";
_replaceConfig "$appConfigPath"			"\[Radarr\]"						"^[[:space:]]*watch_dir ="									"=.*$"			"= $download_incomplete_process_movies";
_replaceConfig "$appConfigPath"			"\[Radarr\]"						"^[[:space:]]*web_root ="										"=.*$"			"= $radarr_web_root";

if [ "$medusa_enabled" == "YES" ]; then _replaceConfig "$appConfigPath" "\[SickBeard\]" "^[[:space:]]*enabled = " "=.*$" "= 1"; fi
_replaceConfig "$appConfigPath"			"\[SickBeard\]"					"^[[:space:]]*\[\[tv\]\]"										"\[\[.*$"		"\[\[tv\]\]";
_replaceConfig "$appConfigPath"			"\[SickBeard\]"					"^[[:space:]]*apikey ="											"=.*$"			"= $medusa_api_key";
_replaceConfig "$appConfigPath"			"\[SickBeard\]"					"^[[:space:]]*chmodDirectory ="							"=.*$"			"= 777";
_replaceConfig "$appConfigPath"			"\[SickBeard\]"					"^[[:space:]]*delete_failed ="							"=.*$"			"= 1";
_replaceConfig "$appConfigPath"			"\[SickBeard\]"					"^[[:space:]]*delete_ignored ="							"=.*$"			"= 1";
_replaceConfig "$appConfigPath"			"\[SickBeard\]"					"^[[:space:]]*delete_on ="									"=.*$"			"= 1";
_replaceConfig "$appConfigPath"			"\[SickBeard\]"					"^[[:space:]]*fork = "											"=.*$"			"= auto";
_replaceConfig "$appConfigPath"			"\[SickBeard\]"					"^[[:space:]]*host ="												"=.*$"			"= $medusa_jail_ip";
_replaceConfig "$appConfigPath"			"\[SickBeard\]"					"^[[:space:]]*keep_archive ="								"=.*$"			"= 0";
_replaceConfig "$appConfigPath"			"\[SickBeard\]"					"^[[:space:]]*password ="										"=.*$"			"= \"\"";
_replaceConfig "$appConfigPath"			"\[SickBeard\]"					"^[[:space:]]*port ="												"=.*$"			"= $medusa_jail_port";
_replaceConfig "$appConfigPath"			"\[SickBeard\]"					"^[[:space:]]*process_method ="							"=.*$"			"= move";
_replaceConfig "$appConfigPath"			"\[SickBeard\]"					"^[[:space:]]*remote_path ="								"=.*$"			"= 0";
_replaceConfig "$appConfigPath"			"\[SickBeard\]"					"^[[:space:]]*username ="										"=.*$"			"= \"\"";
_replaceConfig "$appConfigPath"			"\[SickBeard\]"					"^[[:space:]]*watch_dir ="									"=.*$"			"= $download_incomplete_process_tv";
_replaceConfig "$appConfigPath"			"\[SickBeard\]"					"^[[:space:]]*web_root ="										"=.*$"			"= ";

if [ "$headphones_enabled" == "YES" ]; then _replaceConfig "$appConfigPath" "\[HeadPhones\]" "^[[:space:]]*enabled = " "=.*$" "= 1"; fi
_replaceConfig "$appConfigPath"			"\[HeadPhones\]"				"^[[:space:]]*\[\[music\]\]"								"\[\[.*$"		"\[\[music\]\]";
_replaceConfig "$appConfigPath"			"\[HeadPhones\]"				"^[[:space:]]*apikey ="											"=.*$"			"= $headphones_apiKey";
_replaceConfig "$appConfigPath"			"\[HeadPhones\]"				"^[[:space:]]*host ="												"=.*$"			"= $headphones_jail_ip";
_replaceConfig "$appConfigPath"			"\[HeadPhones\]"				"^[[:space:]]*port ="												"=.*$"			"= $headphones_jail_port";
_replaceConfig "$appConfigPath"			"\[HeadPhones\]"				"^[[:space:]]*remote_path ="								"=.*$"			"= 0";
_replaceConfig "$appConfigPath"			"\[HeadPhones\]"				"^[[:space:]]*watch_dir ="									"=.*$"			"= $download_incomplete_process_music";
_replaceConfig "$appConfigPath"			"\[HeadPhones\]"				"^[[:space:]]*web_root ="										"=.*$"			"= $headphones_web_root";

if [ "$lazylibrarian_enabled" == "YES" ]; then _replaceConfig "$appConfigPath" "\[LazyLibrarian\]" "^[[:space:]]*enabled = " "=.*$" "= 1"; fi
hasEBooks=$(cat "$appConfigPath" | grep "\[\[ebooks\]\]")
if [ ${#hasEBooks} -eq 0 ]; then _replaceConfig "$appConfigPath"	"\[LazyLibrarian\]"	"^[[:space:]]*\[\[books\]\]"	"\[\[.*$"	"\[\[ebooks\]\]"; fi
_replaceConfig "$appConfigPath"			"\[LazyLibrarian\]"			"^[[:space:]]*apikey ="											"=.*$"			"= $lazylibrarian_api_key";
_replaceConfig "$appConfigPath"			"\[LazyLibrarian\]"			"^[[:space:]]*host ="												"=.*$"			"= $lazylibrarian_jail_ip";
_replaceConfig "$appConfigPath"			"\[LazyLibrarian\]"			"^[[:space:]]*port ="												"=.*$"			"= $lazylibrarian_jail_port";
_replaceConfig "$appConfigPath"			"\[LazyLibrarian\]"			"^[[:space:]]*remote_path ="								"=.*$"			"= 0";
_replaceConfig "$appConfigPath"			"\[LazyLibrarian\]"			"^[[:space:]]*watch_dir ="									"=.*$"			"= $download_incomplete_process_ebooks";
_replaceConfig "$appConfigPath"			"\[LazyLibrarian\]"			"^[[:space:]]*web_root ="										"=.*$"			"= $lazylibrarian_web_root";

_replaceConfig "$appConfigPath"			"\[Nzb\]"								"^[[:space:]]*clientAgent ="								"=.*$"			"= sabnzbd";
_replaceConfig "$appConfigPath"			"\[Nzb\]"								"^[[:space:]]*default_downloadDirectory ="	"=.*$"			"= $download_incomplete_nzb";
_replaceConfig "$appConfigPath"			"\[Nzb\]"								"^[[:space:]]*sabnzbd_host ="								"=.*$"			"= http://$sabnzbd_jail_ip";
_replaceConfig "$appConfigPath"			"\[Nzb\]"								"^[[:space:]]*sabnzbd_port ="								"=.*$"			"= $sabnzbd_jail_port";
_replaceConfig "$appConfigPath"			"\[Nzb\]"								"^[[:space:]]*sabnzbd_apikey ="							"=.*$"			"= $sabnzbd_api_key";

_replaceConfig "$appConfigPath"			"\[Torrent\]"						"^[[:space:]]*categories ="									"=.*$"			"= tv, movies, ebooks, music, audiobooks";
_replaceConfig "$appConfigPath"			"\[Torrent\]"						"^[[:space:]]*chmodDirectory ="							"=.*$"			"= 0";
_replaceConfig "$appConfigPath"			"\[Torrent\]"						"^[[:space:]]*clientAgent ="								"=.*$"			"= deluge";
_replaceConfig "$appConfigPath"			"\[Torrent\]"						"^[[:space:]]*default_downloadDirectory ="	"=.*$"			"= $download_incomplete_torrent";
_replaceConfig "$appConfigPath"			"\[Torrent\]"						"^[[:space:]]*DelugeHost ="									"=.*$"			"= 127.0.0.1";
_replaceConfig "$appConfigPath"			"\[Torrent\]"						"^[[:space:]]*DelugePort ="									"=.*$"			"= $deluge_jail_port";
_replaceConfig "$appConfigPath"			"\[Torrent\]"						"^[[:space:]]*DelugePWD ="									"=.*$"			"= $media_user_password";
_replaceConfig "$appConfigPath"			"\[Torrent\]"						"^[[:space:]]*DelugeUSR ="									"=.*$"			"= $media_user_name";
_replaceConfig "$appConfigPath"			"\[Torrent\]"						"^[[:space:]]*outputDirectory ="						"=.*$"			"= $download_incomplete_process";
_replaceConfig "$appConfigPath"			"\[Torrent\]"						"^[[:space:]]*useLink ="										"=.*$"			"= no";

#_replaceConfig "$appConfigPath"			"\[Plex\]"							"^[[:space:]]*plex_host ="									"=.*$"			"= $plex_jail_ip";
#_replaceConfig "$appConfigPath"			"\[Plex\]"							"^[[:space:]]*plex_port ="									"=.*$"			"= $plex_jail_port";
#_replaceConfig "$appConfigPath"			"\[Plex\]"							"^[[:space:]]*plex_sections ="							"=.*$"			"= $plex_sections";
#_replaceConfig "$appConfigPath"			"\[Plex\]"							"^[[:space:]]*plex_token ="									"=.*$"			"= $plex_online_auth_token";

_replaceConfig "$appConfigPath"			"\[Transcoder\]"				"^[[:space:]]*transcode ="									"=.*$"			"= 0";

_replaceConfig "$appConfigPath"			"\[Custom\]"						"^[[:space:]]*remove_group ="		 						"=.*$"			"= \[rarbag\],-NZBgeek";
chown -R $media_user_name:$media_group_name "/mnt/$zpool_name/apps/nzbtomedia"

_checkSetupResults "" "$appName" ""

_log 8 "+" "Setting up application ($appName) configuration" "COMPLETED" 1;

# Restore standard redirection
exec &>/dev/tty
