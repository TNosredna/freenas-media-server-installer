#!/bin/bash
log_enabled="$1"

appName="couchpotato"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-setup.log"
log_err_file_path="$HOME/$appName-setup.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="couchpotato"
appServiceName="couchpotato"
applicationPort="$couchpotato_jail_port"
appURI="$couchpotato_web_root"
appConfigPath="/mnt/$zpool_name/apps/$appName/settings.conf"

jailName_var=$appName\_jail_name
jail_name=${!jailName_var}
jail_ip_var=$appName\_jail_ip
jail_ip=${!jail_ip_var}
jail_mac_var=$appName\_jail_mac
jail_mac=${!jail_mac_var}
openvpn_enabled_var=$appName\_openvpn_enabled
openvpn_enabled=${!openvpn_enabled_var}
ipfw_enabled_var=$appName\_ipfw_enabled
ipfw_enabled=${!ipfw_enabled_var}

_log 8 "/" "Setting up application ($appName) configuration" "STARTING" 1;
_stopService "$appServiceName" "$jail_name"
iocage exec $jail_name sed -i '' -e 's?ssl\.PROTOCOL_TLSv3?ssl\.PROTOCOL_TLSv1?g' /usr/local/couchpotato/libs/synchronousdeluge/transfer.py

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config || true"
_replaceConfig "$appConfigPath" "\[core\]"						"^data_dir ="									"=.*$"				"= $jail_config/";
_replaceConfig "$appConfigPath" "\[core\]"						"^api_key_internal_meta ="		"=.*$"				"= rw";	#=ro
_replaceConfig "$appConfigPath" "\[core\]"						"^development ="							"=.*$"				"= 0";	 #=1
_replaceConfig "$appConfigPath" "\[core\]"						"^debug ="										"=.*$"				"= 1";
_replaceConfig "$appConfigPath" "\[core\]"						"^launch_browser ="						"=.*$"				"= 0";	 #=1
_replaceConfig "$appConfigPath" "\[core\]"						"^port ="											"=.*$"				"= $applicationPort";
_replaceConfig "$appConfigPath" "\[core\]"						"^url_base ="									"=.*$"				"= $couchpotato_web_root";
_replaceConfig "$appConfigPath" "\[core\]"						"^show_wizard ="							"=.*$"				"= 0";

_replaceConfig "$appConfigPath" "\[manage\]"					"^library_refresh_interval =" "=.*$"				"= 0";
_replaceConfig "$appConfigPath" "\[manage\]"					"^enabled ="									"=.*$"				"= 1";
_replaceConfig "$appConfigPath" "\[manage\]"					"^library ="									"=.*$"				"= $jail_media_folder_movies/PG-13/::$jail_media_folder_movies/G/::$jail_media_folder_movies/PG/::$jail_media_folder_movies/R/::$jail_media_folder_movies/TV-14/";

_replaceConfig "$appConfigPath" "\[renamer\]"				 	"^file_name ="								"=.*$"				"= <thename> (<year>)<cd>.<ext>";
_replaceConfig "$appConfigPath" "\[renamer\]"				 	"^next_on_failed ="						"=.*$"				"= True";
_replaceConfig "$appConfigPath" "\[renamer\]"				 	"^default_file_action ="			"=.*$"				"= move";		#=move
_replaceConfig "$appConfigPath" "\[renamer\]"				 	"^check_space ="							"=.*$"				"= 0";
_replaceConfig "$appConfigPath" "\[renamer\]"				 	"^from ="											"=.*$"				"= $download_incomplete_process_movies";	#=/mnt/download/incomplete/process/movies
_replaceConfig "$appConfigPath" "\[renamer\]"				 	"^to ="												"=.*$"				"= $jail_media_folder_movies/";
_replaceConfig "$appConfigPath" "\[renamer\]"				 	"^cleanup ="									"=.*$"				"= 1";	 #=1
_replaceConfig "$appConfigPath" "\[renamer\]"				 	"^move_leftover ="						"=.*$"				"= False";	 #=1
_replaceConfig "$appConfigPath" "\[renamer\]"				 	"^folder_name ="							"=.*$"				"= $jail_media_folder_movies/<mpaa_only>/<namethe> (<year>)(<mpaa_only>)";
_replaceConfig "$appConfigPath" "\[renamer\]"				 	"^file_action ="							"=.*$"				"= move"	 #=move
_replaceConfig "$appConfigPath" "\[renamer\]"				 	"^enabled ="									"=.*$"				"= 1"
_replaceConfig "$appConfigPath" "\[renamer\]"				 	"^unrar ="										"=.*$"				"= 1";
_replaceConfig "$appConfigPath" "\[renamer\]"				 	"^force_every ="							"=.*$"				"= 2";	 #=12

_replaceConfig "$appConfigPath" "\[subtitle\]"				"^languages ="								"=.*$"				"= en";
_replaceConfig "$appConfigPath" "\[subtitle\]"				"^enabled ="									"=.*$"				"= 1";

_replaceConfig "$appConfigPath" "\[blackhole\]"			 	"^enabled ="									"=.*$"				"= 0";

_replaceConfig "$appConfigPath" "\[deluge\]"					"^username ="									"=.*$"				"= $media_user_name";
_replaceConfig "$appConfigPath" "\[deluge\]"					"^delete_failed ="						"=.*$"				"= True";	 #=True
_replaceConfig "$appConfigPath" "\[deluge\]"					"^completed_directory ="			"=.*$"				"= $download_incomplete_torrent/";	 #=/mnt/download/incomplete/torrent/
_replaceConfig "$appConfigPath" "\[deluge\]"					"^enabled ="									"=.*$"				"= 1";
_replaceConfig "$appConfigPath" "\[deluge\]"					"^label ="										"=.*$"				"= movies";
_replaceConfig "$appConfigPath" "\[deluge\]"					"^host ="											"=.*$"				"= $deluge_jail_ip:$deluge_jail_port";	 #=10.11.12.15:58846
_replaceConfig "$appConfigPath" "\[deluge\]"					"^delete_files ="							"=.*$"				"= True";	 #=True
_replaceConfig "$appConfigPath" "\[deluge\]"					"^directory ="								"=.*$"				"= $download_incomplete_process_movies/";	#=/mnt/download/incomplete/process/movies/
_replaceConfig "$appConfigPath" "\[deluge\]"					"^remove_complete ="					"=.*$"				"= True";	 #=True
_replaceConfig "$appConfigPath" "\[deluge\]"					"^password ="									"=.*$"				"= $media_user_password";

_replaceConfig "$appConfigPath" "\[sabnzbd\]"				 	"^category ="									"=.*$"				"= movies";
_replaceConfig "$appConfigPath" "\[sabnzbd\]"				 	"^delete_failed ="						"=.*$"				"= True";	 #=True
_replaceConfig "$appConfigPath" "\[sabnzbd\]"				 	"^enabled ="									"=.*$"				"= 1";
_replaceConfig "$appConfigPath" "\[sabnzbd\]"				 	"^priority ="									"=.*$"				"= 1";
_replaceConfig "$appConfigPath" "\[sabnzbd\]"				 	"^host ="											"=.*$"				"= $sabnzbd_jail_ip:$sabnzbd_jail_port";	 #=10.11.12.14:8080
_replaceConfig "$appConfigPath" "\[sabnzbd\]"				 	"^remove_complete ="					"=.*$"				"= 1";	 #=1
_replaceConfig "$appConfigPath" "\[sabnzbd\]"				 	"^api_key ="									"=.*$"				"= $sabnzbd_api_key";		#=0f877b1beef4b412e3ad5f5ce2fce105

_replaceConfig "$appConfigPath" "\[email\]"					 	"^starttls ="									"=.*$"				"= 1";
_replaceConfig "$appConfigPath" "\[email\]"					 	"^smtp_pass ="								"=.*$"				"= $notify_gmail_password";
_replaceConfig "$appConfigPath" "\[email\]"					 	"^from ="											"=.*$"				"= $notify_gmail_address";
_replaceConfig "$appConfigPath" "\[email\]"					 	"^to ="												"=.*$"				"= $notify_gmail_address";
_replaceConfig "$appConfigPath" "\[email\]"					 	"^smtp_port ="								"=.*$"				"= 587";
_replaceConfig "$appConfigPath" "\[email\]"					 	"^enabled ="									"=.*$"				"= 1";
_replaceConfig "$appConfigPath" "\[email\]"					 	"^smtp_server ="							"=.*$"				"= smtp.gmail.com";
_replaceConfig "$appConfigPath" "\[email\]"					 	"^smtp_user ="								"=.*$"				"= $notify_gmail_address";

_replaceConfig "$appConfigPath" "\[plex\]"						"^username ="									"=.*$"				"= $plex_online_user_name";		#=
_replaceConfig "$appConfigPath" "\[plex\]"						"^auth_token ="								"=.*$"				"= $plex_online_auth_token";	 #=JuSR8JhsrrqxzHCwnzkM
_replaceConfig "$appConfigPath" "\[plex\]"						"^enabled ="									"=.*$"				"= 1";
_replaceConfig "$appConfigPath" "\[plex\]"						"^media_server ="							"=.*$"				"= $plex_jail_ip";	 #=10.11.12.13
_replaceConfig "$appConfigPath" "\[plex\]"						"^password ="									"=.*$"				"= $plex_online_user_password";		#=

_replaceConfig "$appConfigPath" "\[binsearch\]"			 	"^enabled ="									"=.*$"				"= 1";
_replaceConfig "$appConfigPath" "\[binsearch\]"			 	"^extra_score ="							"=.*$"				"= 10";

_replaceConfig "$appConfigPath" "\[newznab\]"				 	"^use ="											"=.*$"				"= 1,0,0,1,0,0";
_replaceConfig "$appConfigPath" "\[newznab\]"				 	"^extra_score ="							"=.*$"				"= 300,0,0,600,0,0";
_replaceConfig "$appConfigPath" "\[newznab\]"				 	"^enabled ="									"=.*$"				"= True";	 #=True
_replaceConfig "$appConfigPath" "\[newznab\]"				 	"^custom_categories ="				"=.*$"				"= 2030 2040 2045 2050";
_replaceConfig "$appConfigPath" "\[newznab\]"				 	"^custom_tag ="								"=.*$"				"= movies,,,movies,,";
_replaceConfig "$appConfigPath" "\[newznab\]"				 	"^api_key ="									"=.*$"				"= $newznab_api_key";

_replaceConfig "$appConfigPath" "\[kickasstorrents\]" "^domain ="										"=.*$"				"= https://katcr.co";
_replaceConfig "$appConfigPath" "\[kickasstorrents\]" "^seed_time ="								"=.*$"				"= 0.01";
_replaceConfig "$appConfigPath" "\[kickasstorrents\]" "^extra_score ="							"=.*$"				"= 200";
_replaceConfig "$appConfigPath" "\[kickasstorrents\]" "^only_verified ="						"=.*$"				"= 1";
_replaceConfig "$appConfigPath" "\[kickasstorrents\]" "^enabled ="									"=.*$"				"= True";	 #=True
_replaceConfig "$appConfigPath" "\[kickasstorrents\]" "^seed_ratio ="								"=.*$"				"= 0.01";

_replaceConfig "$appConfigPath" "\[rarbg\]"						"^enabled ="									"=.*$"				"= 1";
_replaceConfig "$appConfigPath" "\[rarbg\]"						"^extra_score ="							"=.*$"				"= 50";
_replaceConfig "$appConfigPath" "\[rarbg\]"						"^min_seeders ="							"=.*$"				"= 10";		#=2

_replaceConfig "$appConfigPath" "\[thepiratebay\]"		"^domain ="										"=.*$"				"= https://thepiratebay.org";
_replaceConfig "$appConfigPath" "\[thepiratebay\]"		"^seed_time ="								"=.*$"				"= 0.01";
_replaceConfig "$appConfigPath" "\[thepiratebay\]"		"^extra_score ="							"=.*$"				"= 200";
_replaceConfig "$appConfigPath" "\[thepiratebay\]"		"^enabled ="									"=.*$"				"= 1";
_replaceConfig "$appConfigPath" "\[thepiratebay\]"		"^trusted_only ="							"=.*$"				"= 1";
_replaceConfig "$appConfigPath" "\[thepiratebay\]"		"^seed_ratio ="								"=.*$"				"= 0.01";

_replaceConfig "$appConfigPath" "\[torrentz\]"				"^seed_time ="								"=.*$"				"= 0.01";
_replaceConfig "$appConfigPath" "\[torrentz\]"				"^enabled ="									"=.*$"				"= True";
_replaceConfig "$appConfigPath" "\[torrentz\]"				"^seed_ratio ="								"=.*$"				"= 0.01";
_replaceConfig "$appConfigPath" "\[torrentz\]"				"^minimal_seeds ="						"=.*$"				"= 5";	 #=1

_replaceConfig "$appConfigPath" "\[yts\]"							"^info ="											"=.*$"				"= 1";
_replaceConfig "$appConfigPath" "\[yts\]"							"^seed_time ="								"=.*$"				"= 0.01";
_replaceConfig "$appConfigPath" "\[yts\]"							"^enabled ="									"=.*$"				"= 1";
_replaceConfig "$appConfigPath" "\[yts\]"							"^seed_ratio ="								"=.*$"				"= 0.01";
_replaceConfig "$appConfigPath" "\[yts\]"							"^extra_score ="							"=.*$"				"= 200";

_replaceConfig "$appConfigPath" "\[searcher\]"				"^ignored_words ="						"=.*$"				"= german, dutch, french, truefrench, danish, swedish, spanish, italian, korean, dubbed, swesub, korsub, dksubs, vain, HC, HDCAM, TS, HQTS";

_replaceConfig "$appConfigPath" "\[nzb\]"						 	"^retention ="								"=.*$"				"= $usenet_provider_retention";

_replaceConfig "$appConfigPath" "\[imdb\]"						"^chart_display_theater ="		"=.*$"				"= 1";

_replaceConfig "$appConfigPath" "\[themoviedb\]"			"^api_key ="									"=.*$"				"= $tmdb_api_key";

_replaceConfig "$appConfigPath" "\[moviesearcher\]"	 	"^run_on_launch ="						"=.*$"				"= 1";
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config || true"

_restartService "$appServiceName" "$jail_name"

# Get new app's api key from it's configuration and add it to common-config.sh
api_key=$(_getProperty "$appConfigPath" "api_key" "\[core\]")
if [ ${#api_key} -gt 0 ]; then
	_replaceConfig "$(dirname "$0")/../common-config.sh"		 "#!\/bin\/bash"		 "^couchpotato_api_key="		 "=\".*\""		 "=\"$api_key\"";
	_updateApiKeys
fi

_checkSetupResults "" "$appName" ""

_log 8 "+" "Setting up application ($appName) configuration" "COMPLETED" 1;

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2;

# Restore standard redirection
exec &>/dev/tty
