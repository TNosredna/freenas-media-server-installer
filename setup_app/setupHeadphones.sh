#!/bin/bash
log_enabled="$1"

appName="headphones"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-setup.log"
log_err_file_path="$HOME/$appName-setup.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="headphones"
appServiceName="headphones"
applicationPort="$headphones_jail_port"
appURI="$headphones_web_root"
appConfigPath="/mnt/$zpool_name/apps/$appName/config.ini"

jailName_var=$appName\_jail_name
jail_name=${!jailName_var}
jail_ip_var=$appName\_jail_ip
jail_ip=${!jail_ip_var}
jail_mac_var=$appName\_jail_mac
jail_mac=${!jail_mac_var}
openvpn_enabled_var=$appName\_openvpn_enabled
openvpn_enabled=${!openvpn_enabled_var}
ipfw_enabled_var=$appName\_ipfw_enabled
ipfw_enabled=${!ipfw_enabled_var}

_log 8 "/" "Setting up application ($appName) configuration" "STARTING" 1;
_stopService "$appServiceName" "$jail_name"
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config || true"

_replaceConfig "$appConfigPath"			"\[Advanced\]"	"verify_ssl_cert ="						"=.*$"		"= 0";

_replaceConfig "$appConfigPath"			"\[Deluge\]"		"deluge_done_directory ="			"=.*$"		"= $download_incomplete_process_music";
_replaceConfig "$appConfigPath"			"\[Deluge\]"		"deluge_host ="								"=.*$"		"= $deluge_jail_ip:$deluge_jail_http_port";
_replaceConfig "$appConfigPath"			"\[Deluge\]"		"deluge_label ="							"=.*$"		"= music";
_replaceConfig "$appConfigPath"			"\[Deluge\]"		"deluge_password ="						"=.*$"		"= $media_user_password";

_replaceConfig "$appConfigPath"			"\[Email\]"			"email_enabled ="							"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[Email\]"			"email_from ="								"=.*$"		"= $notify_gmail_address";
_replaceConfig "$appConfigPath"			"\[Email\]"			"email_smtp_password ="				"=.*$"		"= $notify_gmail_password";
_replaceConfig "$appConfigPath"			"\[Email\]"			"email_smtp_port ="						"=.*$"		"= 587";
_replaceConfig "$appConfigPath"			"\[Email\]"			"email_smtp_server ="					"=.*$"		"= smtp.gmail.com";
_replaceConfig "$appConfigPath"			"\[Email\]"			"email_smtp_user ="						"=.*$"		"= $notify_gmail_address";
_replaceConfig "$appConfigPath"			"\[Email\]"			"email_tls ="									"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[Email\]"			"email_to ="									"=.*$"		"= $notify_gmail_address";

_replaceConfig "$appConfigPath"			"\[General\]"		"api_enabled ="								"=.*$"		"= 1";
api_key=$(_getProperty "$appConfigPath" "api_key" "\[General\]")
if [ ${#api_key} -eq 0 ]; then	_replaceConfig "$appConfigPath"		"\[General\]"		"api_key ="		"=.*$"		"= $headphones_api_key"; fi
_replaceConfig "$appConfigPath"			"\[General\]"		"autowant_upcoming ="					"=.*$"		"= 0";
_replaceConfig "$appConfigPath"			"\[General\]"		"cleanup_files ="							"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"		"correct_metadata ="					"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"		"destination_dir ="						"=.*$"		"= $jail_media_folder_music";
_replaceConfig "$appConfigPath"			"\[General\]"		"download_dir ="							"=.*$"		"= $download_incomplete_process_music";
_replaceConfig "$appConfigPath"			"\[General\]"		"download_torrent_dir ="			"=.*$"		"= $download_incomplete_process_music";
_replaceConfig "$appConfigPath"			"\[General\]"		"embed_lyrics ="							"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"		"file_permissions ="					"=.*$"		"= 0775";
_replaceConfig "$appConfigPath"			"\[General\]"		"folder_permissions ="				"=.*$"		"= 0775";
_replaceConfig "$appConfigPath"			"\[General\]"		"git_user ="									"=.*$"		"= rembo10";
_replaceConfig "$appConfigPath"			"\[General\]"		"git_path ="									"=.*$"		"= /usr/local/bin/git";
_replaceConfig "$appConfigPath"			"\[General\]"		"http_host ="									"=.*$"		"= 0.0.0.0";
_replaceConfig "$appConfigPath"			"\[General\]"		"http_port ="									"=.*$"		"= $headphones_jail_port";
_replaceConfig "$appConfigPath"			"\[General\]"		"http_root ="									"=.*$"		"= $headphones_web_root";
_replaceConfig "$appConfigPath"			"\[General\]"		"launch_browser ="						"=.*$"		"= 0";
_replaceConfig "$appConfigPath"			"\[General\]"		"lossless_destination_dir ="	"=.*$"		"= $jail_media_folder_music";
_replaceConfig "$appConfigPath"			"\[General\]"		"move_files ="								"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"		"music_dir ="									"=.*$"		"= $jail_media_folder_music";
_replaceConfig "$appConfigPath"		 	"\[General\]"		"nzb_downloader ="						"=.*$"		"= 0";
_replaceConfig "$appConfigPath"			"\[General\]"		"prefer_torrents ="						"=.*$"		"= 2";
_replaceConfig "$appConfigPath"			"\[General\]"		"rename_files ="							"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[General\]"		"torrent_downloader ="				"=.*$"		"= 3";
_replaceConfig "$appConfigPath"			"\[General\]"		"usenet_retention ="					"=.*$"		"= 3748";

_replaceConfig "$appConfigPath"			"\[Newznab\]"		"extra_newznabs ="		 				"=.*$"		"= \"\", \"\", 1, https://api.nzb.su, $nzbsu_api_key, 1";
_replaceConfig "$appConfigPath"			"\[Newznab\]"		"newznab ="										"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[Newznab\]"		"newznab_apikey ="						"=.*$"		"= $nzbgeek_api_key";
_replaceConfig "$appConfigPath"			"\[Newznab\]"		"newznab_enabled ="					 	"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[Newznab\]"		"newznab_host ="							"=.*$"		"= https://api.nzbgeek.info";

_replaceConfig "$appConfigPath"			"\[Piratebay\]"	"piratebay ="									"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[Piratebay\]"	"piratebay_proxy_url ="				"=.*$"		"= https://www.thepiratebay.org";
_replaceConfig "$appConfigPath"			"\[Piratebay\]"	"piratebay_ratio ="						"=.*$"		"= 0";

_replaceConfig "$appConfigPath"			"\[Plex\]"			"plex_enabled ="							"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[Plex\]"			"plex_password ="							"=.*$"		"= $plex_online_user_password";
_replaceConfig "$appConfigPath"			"\[Plex\]"			"plex_server_host ="					"=.*$"		"= $plex_jail_ip:$plex_jail_port";
_replaceConfig "$appConfigPath"			"\[Plex\]"			"plex_token ="								"=.*$"		"= $plex_online_auth_token";
_replaceConfig "$appConfigPath"			"\[Plex\]"			"plex_update ="								"=.*$"		"= 1";
_replaceConfig "$appConfigPath"			"\[Plex\]"			"plex_username ="							"=.*$"		"= $plex_online_user_name";

_replaceConfig "$appConfigPath"			"\[SABnzbd\]"		"sab_apikey ="								"=.*$"		"= $sabnzbd_api_key";
_replaceConfig "$appConfigPath"			"\[SABnzbd\]"		"sab_category ="							"=.*$"		"= music";
_replaceConfig "$appConfigPath"			"\[SABnzbd\]"		"sab_host ="									"=.*$"		"= $sabnzbd_jail_ip:$sabnzbd_jail_port";

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config || true"
_restartService "$appServiceName" "$jail_name"

# Get new app's api key from it's configuration and add it to common-config.sh
api_key=$(_getProperty "$appConfigPath" "api_key" "\[General\]")
if [ ${#api_key} -gt 0 ]; then
	_replaceConfig "$(dirname "$0")/../common-config.sh"	"#!\/bin\/bash"		"^[[:space:]]*headphones_api_key="		 "=\".*\""		 "=\"$api_key\"";
	_updateApiKeys
fi

_checkSetupResults "" "$appName" ""

_log 8 "+" "Setting up application ($appName) configuration" "COMPLETED" 1;

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2;

# Restore standard redirection
exec &>/dev/tty
