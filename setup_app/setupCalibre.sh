#!/bin/bash
log_enabled="$1"

appName="calibre"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-setup.log"
log_err_file_path="$HOME/$appName-setup.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="calibre"
appServiceName="calibre"
applicationPort="$calibre_jail_port"
appURI="$calibre_web_root"
appConfigPath=""

jailName_var=$appName\_jail_name
jail_name=${!jailName_var}
jail_ip_var=$appName\_jail_ip
jail_ip=${!jail_ip_var}
jail_mac_var=$appName\_jail_mac
jail_mac=${!jail_mac_var}
openvpn_enabled_var=$appName\_openvpn_enabled
openvpn_enabled=${!openvpn_enabled_var}
ipfw_enabled_var=$appName\_ipfw_enabled
ipfw_enabled=${!ipfw_enabled_var}

_log 8 "/" "Setting up application ($appName) configuration" "STARTING" 1;
_stopService "$appServiceName" "$jail_name"

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config || true"
_log 8 "$" "Application ($appName) has no configuration to setup!" "SKIPPING" 1;
iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config || true"

_restartService "$appServiceName" "$jail_name"

_checkSetupResults "" "$appName" ""

_log 8 "+" "Setting up application ($appName) configuration" "COMPLETED" 1;

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2;

# Restore standard redirection
exec &>/dev/tty
