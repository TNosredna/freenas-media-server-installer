#!/bin/bash
log_enabled="$1"

appName="radarr"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-setup.log"
log_err_file_path="$HOME/$appName-setup.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="Radarr"
appServiceName="radarr"
applicationPort="$radarr_jail_port"
appURI="$radarr_web_root"
appConfigPath="/mnt/$zpool_name/apps/$appName/nzbdrone.db"
appWebConfigPath="/mnt/$zpool_name/apps/$appName/config.xml"

jailName_var=$appName\_jail_name
jail_name=${!jailName_var}
jail_ip_var=$appName\_jail_ip
jail_ip=${!jail_ip_var}
jail_mac_var=$appName\_jail_mac
jail_mac=${!jail_mac_var}
openvpn_enabled_var=$appName\_openvpn_enabled
openvpn_enabled=${!openvpn_enabled_var}
ipfw_enabled_var=$appName\_ipfw_enabled
ipfw_enabled=${!ipfw_enabled_var}

_log 8 "/" "Setting up application ($appName) configuration" "STARTING" 1
_stopService "$appServiceName" "$jail_name"

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config || true"

_setXmlValue "$appWebConfigPath" "/Config/LogLevel" "Debug";

_setSqlProperty "main.Config" "Key" "\"autorenamefolders\"" "Value" "\"true\""
_setSqlProperty "main.Config" "Key" "\"autounmonitorpreviouslydownloadedepisodes\"" "Value" "\"true\""
_setSqlProperty "main.Config" "Key" "\"chowngroup\"" "Value" "\"$media_group_name\""
_setSqlProperty "main.Config" "Key" "\"chownuser\"" "Value" "\"$media_user_name\""
_setSqlProperty "main.Config" "Key" "\"copyusinghardlinks\"" "Value" "\"false\""
_setSqlProperty "main.Config" "Key" "\"extrafileextensions\"" "Value" "\"srt,nfo\""
_setSqlProperty "main.Config" "Key" "\"filechmod\"" "Value" "\"0777\""
_setSqlProperty "main.Config" "Key" "\"filedate\"" "Value" "\"Release\""
_setSqlProperty "main.Config" "Key" "\"folderchmod\"" "Value" "\"0777\""
_setSqlProperty "main.Config" "Key" "\"importextrafiles\"" "Value" "\"true\""
_setSqlProperty "main.Config" "Key" "\"parsingleniency\"" "Value" "\"ParsingLenient\""
_setSqlProperty "main.Config" "Key" "\"preferindexerflags\"" "Value" "\"true\""
_setSqlProperty "main.Config" "Key" "\"removecompleteddownloads\"" "Value" "\"false\""
_setSqlProperty "main.Config" "Key" "\"removefaileddownloads\"" "Value" "\"false\""
_setSqlProperty "main.Config" "Key" "\"retention\"" "Value" "$usenet_provider_retention"
_setSqlProperty "main.Config" "Key" "\"setpermissionslinux\"" "Value" "\"true\""

sqlite3 "$appConfigPath" <<-SQL_STATEMENT
	REPLACE INTO main.Indexers (Name,Implementation,Settings,ConfigContract,EnableRss,EnableSearch) VALUES ("Nzb.su","Newznab",'{"baseUrl": "https://api.nzb.su", "multiLanguages": [1], "apiKey": "$nzbsu_api_key", "categories": [2030,2040,2045,2050], "animeCategories": [], "removeYear": true, "searchByTitle": false}',"NewznabSettings",0,1);
	REPLACE INTO main.Indexers (Name,Implementation,Settings,ConfigContract,EnableRss,EnableSearch) VALUES ("NZBgeek","Newznab",'{"baseUrl": "https://api.nzbgeek.info", "multiLanguages": [1], "apiKey": "$nzbgeek_api_key", "categories": [2030,2040,2045,2050], "animeCategories": [], "removeYear": true, "searchByTitle": false}',"NewznabSettings",0,1);
	REPLACE INTO main.Indexers (Name,Implementation,Settings,ConfigContract,EnableRss,EnableSearch) VALUES ("Rarbg","Newznab",'{"baseUrl": "https://torrentapi.org", "rankedOnly": true, "multiLanguages": [1], "minimumSeeders": 1, "requiredFlags": [], "categories": [14,48,17,44,45,50,51,52,42,46]}',"RarbgSettings",0,1);
SQL_STATEMENT

_replaceSqlJsonPropertyValue "main.Metadata" "Settings" "useMovieNfo" "true" "Implementation" "\"XbmcMetadata\"" "$appConfigPath"
_replaceSqlJsonPropertyValue "main.Metadata" "Enable" "0" "1" "Implementation" "\"XbmcMetadata\"" "$appConfigPath"

sqlite3 "$appConfigPath" <<-SQL_STATEMENT
	REPLACE INTO main.NamingConfig (MultiEpisodeStyle,RenameEpisodes,ReplaceIllegalCharacters,StandardMovieFormat,MovieFolderFormat,ColonReplacementFormat) VALUES (0,1,1,"{Movie Title} ({Release Year}) {Quality Full}","{Movie Title} ({Release Year})",2);
SQL_STATEMENT

sqlite3 "$appConfigPath" <<-SQL_STATEMENT
	REPLACE INTO main.DownloadClients (Enable,Name,Implementation,Settings,ConfigContract) VALUES (1,"Sabnzbd","Sabnzbd",'{"host": "$sabnzbd_jail_ip","port": $sabnzbd_jail_port,"urlBase": "$sabnzbd_web_root","apiKey": "$sabnzbd_api_key","movieCategory": "movies","recentMoviePriority": -100,"olderMoviePriority": -100,"useSsl": false}',"SabnzbdSettings");
	REPLACE INTO main.DownloadClients (Enable,Name,Implementation,Settings,ConfigContract) VALUES (1,"Deluge","Deluge",'{"host": "$deluge_jail_ip","port": $deluge_jail_http_port,"password": "$media_user_password","movieCategory": "movies","recentMoviePriority": 0,"olderMoviePriority": 0,"addPaused": false,"useSsl": false}',"DelugeSettings");
SQL_STATEMENT

sqlite3 "$appConfigPath" <<-SQL_STATEMENT
	REPLACE INTO main.Notifications (Name,OnGrab,OnDownload,Settings,Implementation,ConfigContract,OnUpgrade,Tags,OnRename) VALUES ("$notify_gmail_address",0,1,'{"server": "smtp.gmail.com","port": 587,"ssl": true,"username": "$notify_gmail_address","password": "$notify_gmail_password","from": "$notify_gmail_address","to": "$notify_gmail_address"}',"Email","EmailSettings",0,"[]",0);
	REPLACE INTO main.Notifications (Name,OnGrab,OnDownload,Settings,Implementation,ConfigContract,OnUpgrade,Tags,OnRename) VALUES ("$plex_server_friendly_name",0,1,'{"host": "$plex_jail_ip","port": $plex_jail_port,"username": "$plex_online_user_name","password": "$plex_online_user_password","updateLibrary": true,"useSsl": false,"isValid": true}',"PlexServer","PlexServerSettings",0,"[]",1);
SQL_STATEMENT

sqlite3 "$appConfigPath" <<-SQL_STATEMENT
	REPLACE INTO main.RootFolders (Path) VALUES ("$jail_media_folder_movies");
SQL_STATEMENT

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config || true"

_restartService "$appServiceName" "$jail_name"

# Get new app's api key from it's configuration and add it to common-config.sh
api_key=$(_getXmlValue "$appWebConfigPath" "/Config/ApiKey")
if [ ${#api_key} -gt 0 ]; then
	_replaceConfig "$(dirname "$0")/../common-config.sh" "#!\/bin\/bash" "^radarr_api_key=" "=\".*\"" "=\"$api_key\""
	_updateApiKeys
fi

_checkSetupResults "" "$appName" ""

_log 8 "+" "Setting up application ($appName) configuration" "COMPLETED" 1

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2

# Restore standard redirection
exec &>/dev/tty
