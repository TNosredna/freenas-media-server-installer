#!/bin/bash
log_enabled="$1"

appName="plex"

source $(dirname "$0")/../common-config.sh
source $(dirname "$0")/../util.sh
log_file_path="$HOME/$appName-setup.log"
log_err_file_path="$HOME/$appName-setup.err.log"

rm -f "$log_file_path"
rm -f "$log_err_file_path"
# Change redirection to file and screen
exec > >(tee -a "$log_file_path") 2>&1

appPackageName="plexmediaserver-plexpass"
appServiceName="plexmediaserver_plexpass"
applicationPort="$plex_jail_port"
appURI="$plex_web_root"
appConfigPath="/mnt/$zpool_name/apps/$appName/Plex Media Server/Preferences.xml"

jailName_var=$appName\_jail_name
jail_name=${!jailName_var}
jail_ip_var=$appName\_jail_ip
jail_ip=${!jail_ip_var}
jail_mac_var=$appName\_jail_mac
jail_mac=${!jail_mac_var}
openvpn_enabled_var=$appName\_openvpn_enabled
openvpn_enabled=${!openvpn_enabled_var}
ipfw_enabled_var=$appName\_ipfw_enabled
ipfw_enabled=${!ipfw_enabled_var}

_log 8 "/" "Setting up application ($appName) configuration" "STARTING" 1;
_stopService "$appServiceName" "$jail_name"

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config || true"

_replaceXmlAttribute "$appConfigPath" "Preferences" "AcceptedEULA" "1";
_replaceXmlAttribute "$appConfigPath" "Preferences" "AlbumSort" "artist:desc";
_replaceXmlAttribute "$appConfigPath" "Preferences" "allowMediaDeletion" "1";
_replaceXmlAttribute "$appConfigPath" "Preferences" "allowedNetworks" "$subnet/24";
_replaceXmlAttribute "$appConfigPath" "Preferences" "ArticleStrings" "the, a, in, that, to";
_replaceXmlAttribute "$appConfigPath" "Preferences" "autoScanMusicSections" "1";
_replaceXmlAttribute "$appConfigPath" "Preferences" "BackgroundTranscodeLowPriority" "0";
_replaceXmlAttribute "$appConfigPath" "Preferences" "BonjourEnabled" "1";
_replaceXmlAttribute "$appConfigPath" "Preferences" "ButlerTaskRefreshLibraries" "1";
_replaceXmlAttribute "$appConfigPath" "Preferences" "DlnaEnabled" "1";
_replaceXmlAttribute "$appConfigPath" "Preferences" "EnableIPv6" "0";
_replaceXmlAttribute "$appConfigPath" "Preferences" "FriendlyName" "$plex_server_friendly_name";
_replaceXmlAttribute "$appConfigPath" "Preferences" "FSEventLibraryPartialScanEnabled" "1";
_replaceXmlAttribute "$appConfigPath" "Preferences" "FSEventLibraryUpdatesEnabled" "1";
_replaceXmlAttribute "$appConfigPath" "Preferences" "GenerateBIFBehavior" "asap";
_replaceXmlAttribute "$appConfigPath" "Preferences" "GenerateChapterThumbBehavior" "asap";
_replaceXmlAttribute "$appConfigPath" "Preferences" "HardwareAcceleratedCodecs" "1";
_replaceXmlAttribute "$appConfigPath" "Preferences" "LanguageInCloud" "1";
_replaceXmlAttribute "$appConfigPath" "Preferences" "LanNetworksBandwidth" "$subnet/24";
_replaceXmlAttribute "$appConfigPath" "Preferences" "LocationVisibility" "2";
_replaceXmlAttribute "$appConfigPath" "Preferences" "logDebug" "1";
_replaceXmlAttribute "$appConfigPath" "Preferences" "LogNumFiles" "10";
_replaceXmlAttribute "$appConfigPath" "Preferences" "logTokens" "1";
_replaceXmlAttribute "$appConfigPath" "Preferences" "LogVerbose" "1";
_replaceXmlAttribute "$appConfigPath" "Preferences" "LoudnessAnalysisBehavior" "asap";
_replaceXmlAttribute "$appConfigPath" "Preferences" "PlexOnlineHome" "1";
_replaceXmlAttribute "$appConfigPath" "Preferences" "PlexOnlineMail" "$plex_online_email";
_replaceXmlAttribute "$appConfigPath" "Preferences" "PlexOnlineToken" "$plex_online_auth_token";
_replaceXmlAttribute "$appConfigPath" "Preferences" "PlexOnlineUsername" "$plex_online_user_name";
_replaceXmlAttribute "$appConfigPath" "Preferences" "PublishServerOnPlexOnlineKey" "1";
_replaceXmlAttribute "$appConfigPath" "Preferences" "PushNotificationsEnabled" "1";
_replaceXmlAttribute "$appConfigPath" "Preferences" "ScannerLowPriority" "1";
_replaceXmlAttribute "$appConfigPath" "Preferences" "ScheduledLibraryUpdatesEnabled" "1";
_replaceXmlAttribute "$appConfigPath" "Preferences" "ScheduledLibraryUpdateInterval" "3600";

iocage exec $jail_name "chown -R $media_user_name:$media_group_name $jail_config || true"

# Get new app's api key from online server and add it to common-config.sh
api_key=$(_getPlexAuthToken "$plex_online_user_name" "$plex_online_user_password")
api_key=$(echo "$api_key" | tr -d '\040\011\012\015')
if [ ${#api_key} -gt 0 ]; then
	_replaceConfig "$(dirname "$0")/../common-config.sh"		 "#!\/bin\/bash"		 "plex_online_auth_token="		 "=\".*\""		 "=\"$api_key\"";
	_replaceXmlAttribute "$appConfigPath" "Preferences" "PlexOnlineToken" "$api_key";
	_updateApiKeys
fi
_restartService "$appServiceName" "$jail_name"

_checkSetupResults "" "$appName" ""

_log 8 "+" "Setting up application ($appName) configuration" "COMPLETED" 1;

iocage restart $jail_name

realJailIP=$(iocage exec $jail_name ifconfig epair0b | grep inet | cut -w -f3)
_log 0 "@" "$appName is available at \"http://$realJailIP:$applicationPort$appURI\"" "" 2;

# Restore standard redirection
exec &>/dev/tty
